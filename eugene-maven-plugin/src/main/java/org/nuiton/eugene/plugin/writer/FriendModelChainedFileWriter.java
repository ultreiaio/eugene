package org.nuiton.eugene.plugin.writer;

/*-
 * #%L
 * EUGene :: Maven plugin
 * %%
 * Copyright (C) 2006 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.writer.ChainedFileWriterConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created by tchemit on 24/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FriendModelChainedFileWriter extends BaseChainedFileWriterToMemoryModel {

    @Override
    protected String getInputType() {
        return "friend";
    }

    @Override
    public String getInputProtocol() {
        return "friend";
    }

    @Override
    public boolean acceptInclude(String include) {
        return include.startsWith("friend:") ||
                include.endsWith(".friendmodel");
    }

    @Override
    public void generate(ChainedFileWriterConfiguration configuration, File outputDir, Map<File, List<File>> filesByRoot, Map<File, List<File>> resourcesByFile) throws IOException {
        super.generate(configuration, outputDir, filesByRoot, resourcesByFile);
    }

    @Override
    public String getDefaultIncludes() {
        return "**/*.friendmodel";
    }

    @Override
    public String getDefaultInputDirectory() {
        return "src/main/models";
    }

    @Override
    public String getDefaultOutputDirectory() {
        return "java";
    }

    @Override
    public String getDefaultTestInputDirectory() {
        return "src/test/models";
    }

    @Override
    public String getDefaultTestOutputDirectory() {
        return "test-java";
    }
}
