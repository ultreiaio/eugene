<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: EUGene Core
  %%
  Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.

  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->

<!--
	
 stateModel.dtd
 
 author : chatellier
 version : $Revision$
 Last update : $Date$
 By : $Author$
-->
<!ELEMENT objectModel ( stateChart )* >
<!ATTLIST objectModel name NMTOKEN #REQUIRED >
<!ATTLIST objectModel xmlns CDATA #REQUIRED >
<!ATTLIST objectModel version CDATA #IMPLIED >

<!ELEMENT stateChart ( state | complexeState )* >
<!ATTLIST stateChart name NMTOKEN #REQUIRED >
<!ATTLIST stateChart package CDATA #REQUIRED >

<!ELEMENT complexeState ( state )+ >
<!ATTLIST state name NMTOKEN #REQUIRED >

<!ELEMENT state ( transition )* >
<!ATTLIST state initial (true | false) "false" >
<!ATTLIST state final (true | false) "false" >
<!ATTLIST state name NMTOKEN #REQUIRED >

<!ELEMENT transition EMPTY >
<!ATTLIST interface event NMTOKEN #REQUIRED >
<!ATTLIST interface toState NMTOKEN #REQUIRED >
