<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: EUGene Core
  %%
  Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
  
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" targetNamespace="http://nuiton.org/eugene/objectModel/v1">

    <xsd:complexType name="objectModel" mixed="true">
        <xsd:choice minOccurs="0" maxOccurs="unbounded">
            <xsd:element name="tagValue" type="tagValue" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="package" type="package"/>
            <xsd:element name="class" type="class"/>
            <xsd:element name="associationClass" type="associationClass"/>
            <xsd:element name="interface" type="interface"/>
            <xsd:element name="enumeration" type="enumeration"/>
        </xsd:choice>
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="version" type="xsd:string" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="package">
        <xsd:sequence>
            <xsd:element name="stereotype" type="stereotype" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="tagValue" type="tagValue" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="extern" type="xsd:boolean" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="classifier">
        <xsd:sequence>
            <xsd:element name="stereotype" type="stereotype" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="tagValue" type="tagValue" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="superclass" type="superclass" minOccurs="0" maxOccurs="1"/>
            <xsd:element name="interface" type="superinterface" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="operation" type="operation" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="dependency" type="dependency" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="package" type="xsd:string" use="required"/>
        <xsd:attribute name="extern" type="xsd:boolean" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="class">
        <xsd:complexContent>
            <xsd:extension base="classifier">
                <xsd:sequence>
                    <xsd:element name="attribute" type="attribute" minOccurs="0" maxOccurs="unbounded"/>
                </xsd:sequence>
                <xsd:attribute name="extern" type="xsd:boolean" use="optional"/>
                <xsd:attribute name="abstract" type="xsd:boolean" use="optional"/>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>

    <xsd:complexType name="associationClass">
        <xsd:complexContent>
            <xsd:extension base="classifier">
                <xsd:sequence>
                    <xsd:element name="attribute" type="attribute" minOccurs="0" maxOccurs="unbounded"/>
                    <xsd:element name="participant" type="participant" minOccurs="0" maxOccurs="unbounded"/>
                </xsd:sequence>
                <xsd:attribute name="abstract" type="xsd:boolean" use="optional"/>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>

    <xsd:complexType name="interface">
        <xsd:complexContent>
            <xsd:extension base="classifier"/>
        </xsd:complexContent>
    </xsd:complexType>

    <xsd:complexType name="superclass">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="discriminator" type="xsd:string" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="superinterface">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="attribute">
        <xsd:sequence>
            <xsd:element name="tagValue" type="tagValue" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="stereotype" type="stereotype" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="name" type="xsd:string" use="optional"/>
        <xsd:attribute name="visibility" type="xsd:string" use="optional"/>
        <xsd:attribute name="type" type="xsd:string" use="required"/>
        <xsd:attribute name="reverseAttributeName" type="xsd:string" use="optional"/>
        <xsd:attribute name="reverseMaxMultiplicity" type="xsd:integer" use="optional"/>
        <xsd:attribute name="associationType" type="xsd:string" use="optional"/>
        <xsd:attribute name="minMultiplicity" type="xsd:nonNegativeInteger" use="optional"/>
        <xsd:attribute name="maxMultiplicity" type="xsd:integer" use="optional"/>
        <xsd:attribute name="navigable" type="xsd:boolean" use="optional"/>
        <xsd:attribute name="ordering" type="xsd:string" use="optional"/>
        <xsd:attribute name="defaultValue" type="xsd:string" use="optional"/>
        <xsd:attribute name="associationClassName" type="xsd:string" use="optional"/>

    </xsd:complexType>

    <xsd:complexType name="operation">
        <xsd:sequence>
            <xsd:element name="stereotype" type="stereotype" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="parameter" type="parameter" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="returnParameter" type="returnParameter" minOccurs="1" maxOccurs="1"/>
            <xsd:element name="exceptionParameter" type="exceptionParameter" minOccurs="0" maxOccurs="unbounded"/>
            <xsd:element name="tagValue" type="tagValue" minOccurs="0" maxOccurs="unbounded"/>
        </xsd:sequence>
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="visibility" type="xsd:string" use="optional"/>
    </xsd:complexType>

    <xsd:complexType name="parameter">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="type" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="returnParameter">
        <xsd:attribute name="type" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="exceptionParameter">
        <xsd:attribute name="name" type="xsd:string" use="optional"/>
        <xsd:attribute name="type" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="participant">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="attribute" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="enumeration">
        <xsd:complexContent>
            <xsd:extension base="classifier">
                <xsd:sequence>
                    <xsd:element name="literal" type="literal" minOccurs="0" maxOccurs="unbounded"/>
                </xsd:sequence>
            </xsd:extension>
        </xsd:complexContent>
    </xsd:complexType>

    <xsd:complexType name="literal">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="dependency">
        <xsd:attribute name="name" type="xsd:string" use="optional"/>
        <xsd:attribute name="supplierName" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="stereotype">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:complexType name="tagValue">
        <xsd:attribute name="name" type="xsd:string" use="required"/>
        <xsd:attribute name="value" type="xsd:string" use="required"/>
    </xsd:complexType>

    <xsd:element name="objectModel" type="objectModel" />

</xsd:schema>
