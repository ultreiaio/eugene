package org.nuiton.eugene;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Moved from java4all.java-lang.
 *
 * Created on 13/07/2022.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0.0
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by tchemit on 30/12/2017.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ClassLoaders {

    /**
     * Retourner la liste des fichiers du classLoader. Ces fichiers doivent
     * correspondre au pattern donne.
     * <p>
     * Utile par defaut {@link ClassLoader#getSystemClassLoader()}.
     *
     * @param pattern le nom du fichier a extraire du fichier compressé ou
     *                du repertoire doit correspondre au pattern (repertoire + nom
     *                compris).
     * @return la liste des urls correspondant au pattern
     */
    public static List<URL> getURLs(String pattern) {
        return getURLs(pattern, (URLClassLoader) null);
    }

    /**
     * Retourner la liste des fichiers du classLoader. Ces fichiers doivent
     * correspondre au pattern donne.
     *
     * @param classLoader classloader to use (if null, use {@link ClassLoader#getSystemClassLoader()}
     * @param pattern     le nom du fichier a extraire du fichier compressé ou
     *                    du repertoire doit correspondre au pattern (repertoire + nom
     *                    compris).
     * @return la liste des urls correspondant au pattern
     */
    public static List<URL> getURLs(String pattern, URLClassLoader classLoader) {
        if (classLoader == null) {
            classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        }
        URL[] arrayURL = getURLs(classLoader);
        return getURLs(pattern, arrayURL);
    }


    /**
     * Recupere la liste des urls d'un {@link URLClassLoader}.
     * <p>
     * Note : Un cas particulier est positionné pour JBoss qui utilise
     * la method getAllURLs.
     *
     * @param classLoader le class loader a scanner
     * @return les urls du classloader.
     */
    public static URL[] getURLs(URLClassLoader classLoader) {
        if (classLoader == null) {
            classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        }
        Method m;
        try {
            // Essai de récupération de la méthode getAllURLs() de
            // RepositoryClassLoader (JBoss)
            m = classLoader.getClass().getMethod("getAllURLs");
        } catch (Exception e) {
            m = null;
        }
        URL[] result;
        if (m == null) {
            result = classLoader.getURLs();
        } else {
            try {
                result = (URL[]) m.invoke(classLoader);
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }
        }
        return result;
    }

    /**
     * Retourner la liste des fichiers du classLoader. Ces fichiers doivent
     * correspondre au pattern donne.
     *
     * @param arrayURL les urls ou chercher
     * @param pattern  le nom du fichier a extraire du fichier compressé ou
     *                 dur epertoire doit correspondre au pattern (repertoire + nom
     *                 compris).
     * @return la liste des urls correspondant au pattern
     */
    public static List<URL> getURLs(String pattern, URL... arrayURL) {
        long t0 = System.nanoTime();

        Set<URL> urlList = new LinkedHashSet<>();

        if (arrayURL.length == 1) {
            URL jarURL = arrayURL[0];
            if (isJar(jarURL.toString())) {
                // jar invocation
                try {
                    arrayURL = getClassPathURLsFromJarManifest(jarURL);
                } catch (Exception e) {
//                    log.warn(e);
                    arrayURL = new URL[]{jarURL};
                }
            }
        }
//        for (URL url : arrayURL) {
//            log.debug("found url " + url);
//        }

        for (URL urlFile : arrayURL) {
            // EC-20100510 this cause wrong accent encoding
            //String fileName = urlFile.getFile();
//            String fileName;
//            try {
//                fileName = urlFile.toURI().getPath();
//            } catch (Exception e) {
//                if (log.isWarnEnabled()) {
//                    log.warn(e);
//                }
//                // warning, this can cause wrong encoding !!!
//                fileName = urlFile.getFile();
//            }

//            // TODO deal with encoding in windows, this is very durty, but it
//            // works...
            File file = toFile(urlFile);
            String fileName = file.getAbsolutePath();
//            File file = new File(fileName.replaceAll("%20", " "));
            if (!file.exists()) {
                // this case should not appear
//                log.debug("Can't find file " + file + " (" + fileName + ")");
                continue;
            }
            if (isJar(fileName)) {
                // cas ou le ichier du classLoader est un fichier jar
//                log.debug("jar to search " + file);
                urlList.addAll(getURLsFromJar(file, pattern));
                continue;
            }
            if (file.isDirectory()) {
                // cas ou le fichier du classLoader est un repertoire
//                log.debug("directory to search " + file);
                // on traite le cas ou il peut y avoir des repertoire dans ce
                // repertoire
                urlList.addAll(getURLsFromDirectory(file, pattern));
                continue;
            }

            if (isZip(fileName)) {
                // cas ou le ichier du classLoader est un fichier zip
//                log.debug("zip to search " + file);
                urlList.addAll(getURLsFromZip(file, pattern));
            }

        }
//        log.info(String.format("search URLs pattern: %s in %d urls in %s", pattern, arrayURL.length, Strings.convertTime(System.nanoTime() - t0)));
        return new LinkedList<>(urlList);
    }

    public static List<URL> getURLsFromZip(File zipFile, String pattern) {
        try {
//            log.trace("search '" + pattern + "' in " + zipFile);

            Set<URL> result = new LinkedHashSet<>();
//            InputStream in = new FileInputStream(zipFile);
            try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile))) {
                while (zis.available() != 0) {
                    ZipEntry entry = zis.getNextEntry();

                    if (entry == null) {
                        break;
                    }

                    String name = entry.getName();
//                    log.trace("zipFile: " + zipFile + " name: " + name);
                    if (pattern == null || name.matches(pattern)) {
                        // on recupere le fichier correspondant au pattern dans
                        // le classloader
                        URL url = getURL(name);
                        // on ajoute le fichier correspondant au pattern dans la liste
//                        log.trace("zipFile: " + zipFile + " url: " + url);
                        result.add(url);
                    }
                }
            }
//            log.trace("found with pattern '" + pattern + "' : " + result);
            return new LinkedList<>(result);
        } catch (IOException eee) {
            throw new RuntimeException("Can't get url from zip: " + zipFile.getAbsolutePath(), eee);
        }
    }

    /**
     * Recherche la ressource nom.
     *
     * @param name nom de la ressource
     * @return l'url de la ressource
     */
    public static URL getURL(String name) {
        URL url = getURLOrNull(name);
        if (url != null) {
            return url;
        }

        throw new RuntimeException(String.format("Can't find resource: %s", name));
    }


    /**
     * Recherche la ressource nom.
     *
     * @param name le nom de la ressource
     * @return l'url de la ressource ou null
     */
    public static URL getURLOrNull(String name) {
        // on recherche d'abord sur le filesystem
        File file = new File(name);
        if (file.exists()) {
            try {
                return file.toURI().toURL();
            } catch (MalformedURLException eee) {
                //log.warn(t("nuitonutil.error.convert.file.to.url", file, eee.getMessage()));
            }
        }

        // on ne l'a pas trouve on recherche dans le classpath

        // on supprime le / devant le nom de la ressource, sinon elle
        // n'est pas trouve (pas de recherche dans les differents
        // element du classpath.
        if (name.length() > 1 && name.startsWith("/")) {
            name = name.substring(1);
        }
        URL url = ClassLoader.getSystemClassLoader().getResource(name);
        if (url != null) {
            return url;
        }

        ClassLoader cl = ClassLoaders.class.getClassLoader();
        url = cl.getResource(name);
        return url;
    }


    /**
     * Verifie si le fichier est un fichier jar.
     *
     * @param name nom du fichier a tester
     * @return vrai si le fichier se termine par .jar faux sinon
     */
    public static boolean isJar(String name) {
        if (name != null && name.length() > 4) {
            String ext = name.substring(name.length() - 4, name.length());
            return ".jar".equalsIgnoreCase(ext);
        }
        return false;
    }

    /**
     * Verifie si le fichier est un fichier zip
     *
     * @param name nom du fichier a tester
     * @return vrai si le fichier se termine par .zip faux sinon
     */
    public static boolean isZip(String name) {
        if (name != null && name.length() > 4) {
            String ext = name.substring(name.length() - 4, name.length());
            return ".zip".equalsIgnoreCase(ext);
        }
        return false;
    }


    public static List<URL> getURLsFromJar(File jarfile, String pattern) {
        try {
//            log.trace("search '" + pattern + "' in " + jarfile);
            Set<URL> result = new LinkedHashSet<>();
            try (ZipInputStream zis = new ZipInputStream(new FileInputStream(jarfile))) {
                while (zis.available() != 0) {
                    ZipEntry entry = zis.getNextEntry();
                    if (entry == null) {
                        break;
                    }
                    String name = entry.getName();
//                    log.trace("jarfile: " + jarfile + " name: " + name);
                    if (pattern == null || name.matches(pattern)) {
                        // on recupere le fichier correspondant au pattern dans le classloader
                        URL url = getURL(name);
                        // on ajoute le fichier correspondant au pattern dans la liste
//                        log.trace("jarfile: " + jarfile + " url: " + url);
                        result.add(url);
                    }
                }
            }
//            log.trace("found with pattern '" + pattern + "' : " + result);
            return new LinkedList<>(result);
        } catch (IOException eee) {
            throw new RuntimeException(String.format("Can't get url from zip: %s", jarfile.getAbsolutePath()), eee);
        }
    }


    public static URL[] getClassPathURLsFromJarManifest(URL jarURL)
            throws IOException {
        URL[] result;
        File jarFile = toFile(jarURL);
//        log.debug("class-path jar to scan " + jarFile);
        try (JarFile jar = new JarFile(jarFile)) {
            //            String jarPath = jarURL.toURI().getPath();
            //            File jarFile = new File(jarPath);
            File container = jarFile.getParentFile();
            Manifest mf = jar.getManifest();
            String classPath = null;
            if (mf != null && mf.getMainAttributes() != null) {
                classPath = mf.getMainAttributes().getValue(Attributes.Name.CLASS_PATH);
            }
            String[] paths;
            if (classPath != null) {
                paths = classPath.split(" ");
            } else {
                paths = new String[0];
            }
            result = new URL[paths.length + 1];
            result[0] = jarURL;
            File path;
            for (int i = 0; i < paths.length; i++) {
                String s = paths[i];
                // test de l'existence d'un protocole dans le path (genre file:...)
                if (s.indexOf(':') != -1) {
                    result[i + 1] = new URL(s);
                    continue;
                }

                if (s.startsWith(".") || !s.startsWith("/")) {
                    // relative url
                    path = new File(container, s);
                } else {
                    path = new File(s);
                }
                //                log.debug(path);
                result[i + 1] = path.toURI().toURL();
            }
        }
        return result;
    }

    /**
     * Retourne la liste des fichiers correspondant au pattern donne, aucun
     * ordre ne doit être supposé sur les fichiers.
     *
     * @param repository repertoire dans lequel on recherche les fichiers
     * @param pattern    le nom du fichier a extraire du fichier du repertoire doit
     *                   correspondre au pattern (repertoire + nom compris). si le
     *                   pattern est null, tous les fichiers trouvé sont retourné.
     * @return la liste des urls correspondant au pattern
     */
    public static List<URL> getURLsFromDirectory(File repository, String pattern) {
        try {
//            log.trace("search '" + pattern + "' in " + repository);

            List<URL> urlList = new LinkedList<>();
            File[] filesList = repository.listFiles();

            if (filesList != null) {

                for (File file : filesList) {

                    String name = file.getAbsolutePath();

//                    log.trace("directory: " + repository + " name: " + name);

                    // cas de recursivite : repertoire dans un repertoire
                    if (file.exists() && file.isDirectory()) {
                        urlList.addAll(getURLsFromDirectory(file, pattern));
                        // si le fichier du repertoire n'est pas un repertoire
                        // on verifie s'il correspond au pattern
                    } else if (pattern == null || name.matches(pattern)) {
                        URL url = file.toURI().toURL();
//                        log.trace("directory: " + repository + " url: " + url);
                        urlList.add(url);
                    }
                }
            }
//            log.trace("found with pattern '" + pattern + "' : " + urlList);
            return urlList;
        } catch (MalformedURLException eee) {
            throw new RuntimeException(String.format("Can't convert url to file %s", repository + " (pattern " + pattern + ") "), eee);
            //throw new ResourceException("Le fichier n'a pu être converti en URL", eee);
        }
    }

    //-----------------------------------------------------------------------

    /**
     * Convert from a <code>URL</code> to a <code>File</code>.
     * <p>
     * From version 1.1 this method will decode the URL.
     * Syntax such as <code>file:///my%20docs/file.txt</code> will be
     * correctly decoded to <code>/my docs/file.txt</code>. Starting with version
     * 1.5, this method uses UTF-8 to decode percent-encoded octets to characters.
     * Additionally, malformed percent-encoded octets are handled leniently by
     * passing them through literally.
     *
     * @param url the file URL to convert, {@code null} returns {@code null}
     * @return the equivalent <code>File</code> object, or {@code null}
     * if the URL's protocol is not <code>file</code>
     */
    public static File toFile(final URL url) {
        if (url == null || !"file".equalsIgnoreCase(url.getProtocol())) {
            return null;
        } else {
            String filename = url.getFile().replace('/', File.separatorChar);
            filename = decodeUrl(filename);
            return new File(filename);
        }
    }

    /**
     * Decodes the specified URL as per RFC 3986, i.e. transforms
     * percent-encoded octets to characters by decoding with the UTF-8 character
     * set. This function is primarily intended for usage with
     * {@link java.net.URL} which unfortunately does not enforce proper URLs. As
     * such, this method will leniently accept invalid characters or malformed
     * percent-encoded octets and simply pass them literally through to the
     * result string. Except for rare edge cases, this will make unencoded URLs
     * pass through unaltered.
     *
     * @param url The URL to decode, may be {@code null}.
     * @return The decoded URL or {@code null} if the input was
     * {@code null}.
     */
    static String decodeUrl(final String url) {
        String decoded = url;
        if (url != null && url.indexOf('%') >= 0) {
            final int n = url.length();
            final StringBuilder buffer = new StringBuilder();
            final ByteBuffer bytes = ByteBuffer.allocate(n);
            for (int i = 0; i < n; ) {
                if (url.charAt(i) == '%') {
                    try {
                        do {
                            final byte octet = (byte) Integer.parseInt(url.substring(i + 1, i + 3), 16);
                            bytes.put(octet);
                            i += 3;
                        } while (i < n && url.charAt(i) == '%');
                        continue;
                    } catch (final RuntimeException e) {
                        // malformed percent-encoded octet, fall through and
                        // append characters literally
                    } finally {
                        if (bytes.position() > 0) {
                            bytes.flip();
                            buffer.append(StandardCharsets.UTF_8.decode(bytes).toString());
                            bytes.clear();
                        }
                    }
                }
                buffer.append(url.charAt(i++));
            }
            decoded = buffer.toString();
        }
        return decoded;
    }


    /**
     * Permet d'ajouter dans le classloader par defaut une nouvelle URL dans
     * lequel il faut rechercher les fichiers.
     *
     * @param url l'url a ajouter
     */
    public static void addDefaultClassLoader(URL url) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        addClassLoader(classLoader, url);
    }

    /**
     * Permet d'ajouter dans un classloader une nouvelle URL dans
     * lequel il faut rechercher les fichiers.
     *
     * @param classLoader le classloader a modifier
     * @param url         l'url a ajouter
     */
    public static void addClassLoader(ClassLoader classLoader, URL url) {
        try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            method.setAccessible(true);
            method.invoke(classLoader, url);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Can't add url in class loader %s: %s", classLoader, e), e);
        }
    }
}
