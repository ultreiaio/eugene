/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.java;

import io.ultreia.java4all.i18n.I18n;
import io.ultreia.java4all.lang.Strings;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.Template;
import org.nuiton.eugene.java.extension.ImportsManager;
import org.nuiton.eugene.java.extension.ObjectModelAnnotation;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.ObjectModelTransformer;
import org.nuiton.eugene.models.object.ObjectModelType;

import javax.annotation.Generated;
import java.beans.Introspector;
import java.io.File;
import java.net.URL;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

/**
 * Created: 28 oct. 2009
 *
 * @author Florian Desbois - desbois@codelutin.com
 */
public abstract class ObjectModelTransformerToJava extends ObjectModelTransformer<ObjectModel> {

    private static final Logger log = LogManager.getLogger(ObjectModelTransformerToJava.class);

    /**
     * internal builder
     */
    protected JavaBuilder builder;

    private String constantPrefix;

    protected final EugeneCoreTagValues eugeneTagValues;

    public ObjectModelTransformerToJava() {
        eugeneTagValues = new EugeneCoreTagValues();
    }

    @Override
    public Template<ObjectModel> initOutputTemplate() {
        // by default, use a stupid java generator
        return new JavaGenerator();
    }

    @Override
    public ObjectModel initOutputModel() {
        if (log.isDebugEnabled()) {
            log.debug("inputModelName = " + getModel().getName());
        }
        builder = new JavaBuilder(getModel().getName());
        setConstantPrefix("");
        return builder.getModel();
    }

    public ImportsManager getImportManager(ObjectModelClass clazz) {
        return builder.getImportManager(clazz);
    }

    @Override
    protected void debugOutputModel() {
        if (log.isDebugEnabled()) {
            log.debug("classes : " + getOutputModel().getClasses().size());
            for (ObjectModelClass clazz : getOutputModel().getClasses()) {
                log.debug("  class : " + clazz.getQualifiedName());
                ImportsManager manager = getImportManager(clazz);
                for (String imports :
                        manager.getImports(clazz.getPackageName())) {
                    log.debug("  import : " + imports);
                }
                for (ObjectModelAttribute attribute : clazz.getAttributes()) {
                    log.debug("  attribute : " + attribute.getType() +
                                      " " + attribute.getName());
                }
                for (ObjectModelOperation operation : clazz.getOperations()) {
                    log.debug("  operation : " + operation.getReturnType() +
                                      " " + operation.getName());
                }
            }
        }
    }

    /**
     * Add the {@link Generated} annotation to the given {@link ObjectModelClassifier}
     *
     * @param element the element to be generated on which the annotation will be added
     * @since 2.7
     */
    // TODO AThimel 06/07/13 Maybe be skipped by configuration ?
    public void addGeneratedAnnotation(ObjectModelClassifier element) {

        String generatorName = getClass().getName();
        Date now = new Date();
        ObjectModelAnnotation annotation = addAnnotation(element, element, Generated.class);
        addAnnotationParameter(element, annotation, "value", generatorName);
        addAnnotationParameter(element, annotation, "date", now.toString());
    }

    public ObjectModelClass createClass(String name, String packageName) {
        ObjectModelClass result = builder.createClass(name, packageName);
        addGeneratedAnnotation(result);
        return result;
    }

    public ObjectModelPackage getPackage(ObjectModelClassifier input) {
        return model.getPackage(input);
    }

    public ObjectModelPackage getPackage(String packageName) {
        return model.getPackage(packageName);
    }

    public ObjectModelEnumeration createEnumeration(String name,
                                                    String packageName) {
        ObjectModelEnumeration result = builder.createEnumeration(name, packageName);
        addGeneratedAnnotation(result);
        return result;
    }

    public ObjectModelClass createAbstractClass(String name,
                                                String packageName) {
        ObjectModelClass result = builder.createAbstractClass(name, packageName);
        addGeneratedAnnotation(result);
        return result;
    }

    public ObjectModelInterface createInterface(String name,
                                                String packageName) {
        ObjectModelInterface result = builder.createInterface(name, packageName);
        addGeneratedAnnotation(result);
        return result;
    }

    public void setSuperClass(ObjectModelClass classifier,
                              String superclassQualifiedName) {
        builder.setSuperClass(classifier, superclassQualifiedName);
    }

    public void setSuperClass(ObjectModelClass classifier,
                              Class<?> superclassQualifiedName) {
        setSuperClass(classifier, superclassQualifiedName.getName());
    }

    public void addInterface(ObjectModelClassifier classifier,
                             String interfaceQualifiedName) {
        builder.addInterface(classifier, interfaceQualifiedName);
    }

    public void addInterface(ObjectModelClassifier classifier,
                             Class<?> interfaceQualifiedName) {
        addInterface(classifier, interfaceQualifiedName.getName());
    }

    public void addImport(ObjectModelClassifier classifier,
                          String imports) {
        builder.addImport(classifier, imports);
    }

    public void addImport(ObjectModelClassifier classifier,
                          ObjectModelClass imports) {
        addImport(classifier, imports.getPackageName() + "." + imports.getName());
    }

    public void addImport(ObjectModelClassifier classifier,
                          Class<?> imports) {
        addImport(classifier, imports.getName());
    }

    public String importAndSimplify(ObjectModelClassifier classifier, String imports) {
        return builder.importAndSimplify(classifier, imports);
    }

    /**
     * Adds a tag value to the model.
     *
     * @param name  name of the tag value
     * @param value value of the tag value
     */
    public void addTagValue(String name, String value) {
        builder.addTagValue(name, value);
    }

    /**
     * Adds a tag value to the given {@code element}.
     *
     * @param element the element on which the tag value is attached.
     * @param name    the name of the tag value
     * @param value   the value of the tag value
     */
    public void addTagValue(ObjectModelElement element, String name, String value) {
        builder.addTagValue(element, name, value);
    }

    public String getConstantName(String propertyName) {
        return getConstantPrefix() + builder.getConstantName(propertyName);
    }

    public String getConstantPrefix(ObjectModelClassifier input) {

        // look if there is a constant_prefix tag value
        String prefix = eugeneTagValues.getConstantPrefixTagValue(
                input,
                input == null ? null : getPackage(input),
                getModel());

        return prefix.trim();
    }

    public ObjectModelAttribute addConstant(
            ObjectModelClassifier classifier,
            String name,
            String type,
            String value,
            ObjectModelModifier visibility) throws IllegalArgumentException {
        return builder.addConstant(classifier, name, type, value, visibility);
    }

    public ObjectModelAttribute addConstant(
            ObjectModelClassifier classifier,
            String name,
            Class<?> type,
            String value,
            ObjectModelModifier visibility) throws IllegalArgumentException {
        return addConstant(classifier, name, type.getName(), value, visibility);
    }

    public ObjectModelAttribute addAttribute(
            ObjectModelClassifier classifier,
            String name,
            String type,
            String value,
            ObjectModelModifier... modifiers) {
        return builder.addAttribute(classifier, name, type, value, modifiers);
    }

    public ObjectModelAttribute addAttribute(
            ObjectModelClassifier classifier,
            String name,
            Class<?> type,
            String value,
            ObjectModelModifier... modifiers) {
        return addAttribute(classifier, name, type.getName(), value, modifiers);
    }

    public ObjectModelAttribute addAttribute(
            ObjectModelClassifier classifier,
            ObjectModelAttribute attribute) {
        return addAttribute(classifier, attribute.getName(), attribute.getType());
    }

    public ObjectModelAttribute addAttribute(
            ObjectModelClassifier classifier,
            String name,
            String type) {
        return builder.addAttribute(classifier, name, type);
    }

    public ObjectModelOperation addConstructor(
            ObjectModelClass clazz, ObjectModelModifier visibility)
            throws IllegalArgumentException {
        return builder.addConstructor(clazz, visibility);
    }

    public ObjectModelOperation addConstructor(
            ObjectModelEnumeration clazz, ObjectModelModifier visibility)
            throws IllegalArgumentException {
        return builder.addConstructor(clazz, visibility);
    }

    public ObjectModelOperation addOperation(
            ObjectModelClassifier classifier,
            String name,
            String type,
            ObjectModelModifier... modifiers) {
        return builder.addOperation(classifier, name, type, modifiers);
    }

    public ObjectModelOperation addOperation(
            ObjectModelClassifier classifier,
            String name,
            Class<?> type,
            ObjectModelModifier... modifiers) {
        return addOperation(classifier, name, type == null ? null : type.getName(), modifiers);
    }

    /**
     * Adds an operation to the given classifier.
     *
     * @param classifier the classifier on which the operation will be added
     * @param operation  the operation to add
     * @return the added operation
     * @since 2.1.2
     */
    public ObjectModelOperation addOperation(
            ObjectModelClassifier classifier,
            ObjectModelOperation operation) {
        return builder.addOperation(classifier, operation);
    }

    public void addLiteral(ObjectModelEnumeration classifier, String name) {
        builder.addLiteral(classifier, name);
    }

    public ObjectModelParameter addParameter(
            ObjectModelOperation operation,
            String type,
            String name) {
        return builder.addParameter(operation, type, name);
    }

    public ObjectModelParameter addParameter(
            ObjectModelOperation operation,
            Class<?> type,
            String name) {
        return addParameter(operation, type.getName(), name);
    }

    public void addException(
            ObjectModelOperation operation,
            String exception) {
        builder.addException(operation, exception);
    }

    public void addException(
            ObjectModelOperation operation,
            Class<?> exception) {
        addException(operation, exception.getName());
    }

    public void setOperationBody(ObjectModelOperation operation,
                                 String body)
            throws IllegalArgumentException {
        builder.setOperationBody(operation, body);
    }

    public ObjectModelClassifier addInnerClassifier(
            ObjectModelClass clazz,
            ObjectModelType type,
            String name,
            ObjectModelModifier... modifiers) throws IllegalArgumentException {
        return builder.addInnerClassifier(clazz, type, name, modifiers);
    }

    public void setDocumentation(ObjectModelElement element,
                                 String documentation) {
        builder.setDocumentation(element, documentation);
    }

    /**
     * Adds a min multiplicity to the given {@code attribute}.
     *
     * @param attribute    the attribute on which the min multiplicty will be setted
     * @param multiplicity the multiplicity to set
     * @since 2.1.2
     */
    public void setMinMultiplicity(ObjectModelAttribute attribute, int multiplicity) {
        builder.setMinMultiplicity(attribute, multiplicity);
    }

    /**
     * Adds a max multiplicity to the given {@code attribute}.
     *
     * @param attribute    the attribute on which the min multiplicty will be setted
     * @param multiplicity the multiplicity to set
     * @since 2.1.2
     */
    public void setMaxMultiplicity(ObjectModelAttribute attribute, int multiplicity) {
        builder.setMaxMultiplicity(attribute, multiplicity);
    }

    /**
     * Adds a navigable flag to the given {@code attribute}.
     *
     * @param attribute the attribute on which the navigable flag will be setted
     * @param navigable the navigable flag to set
     * @since 2.3
     */
    public void setNavigable(ObjectModelAttribute attribute, boolean navigable) {
        builder.setNavigable(attribute, navigable);
    }

    /**
     * Adds a comment to the given {@code element}.
     *
     * @param element the element on which the comment will be added.
     * @param comment the comment to add
     * @since 2.1.2
     */
    public void addComment(ObjectModelElement element, String comment) {
        builder.addComment(element, comment);
    }

    /**
     * Add an annotation of the given {@code element} of the given {@code classifier}.
     * <p>
     * <strong>Important note:</strong>
     * <pre>
     * Since 2.6.4, the behaviour of this method has changed, we only allowed
     * now to pass here a annotation type and no longer a annotation
     * string representation with some parameters...
     * </pre>
     *
     * @param classifier     holder of element to treat
     * @param element        where to add the annotation
     * @param annotationType type of annotation to create
     * @return the instanciated annotation
     */
    public ObjectModelAnnotation addAnnotation(ObjectModelClassifier classifier,
                                               ObjectModelElement element,
                                               String annotationType) {
        return builder.addAnnotation(classifier, element, annotationType);
    }

    /**
     * Add an annotation of the given {@code element} of the given {@code classifier}.
     *
     * @param classifier     holder of element to treat
     * @param element        where to add the annotation
     * @param annotationType type of annotation to create
     * @return the instanciated annotation
     * @since 2.6.4
     */
    public ObjectModelAnnotation addAnnotation(ObjectModelClassifier classifier,
                                               ObjectModelElement element,
                                               Class<?> annotationType) {
        addImport(classifier, annotationType);
        return builder.addAnnotation(classifier,
                                     element,
                                     annotationType.getSimpleName());
    }

    /**
     * Add a parameter to an annotation.
     *
     * @param classifier      holder of element to treat
     * @param annotation      where to add the parameter
     * @param annotationName  parameter name
     * @param annotationValue paramter value
     * @return the annotation
     * @since 2.6.4
     */
    public ObjectModelAnnotation addAnnotationParameter(ObjectModelClassifier classifier,
                                                        ObjectModelAnnotation annotation,
                                                        String annotationName,
                                                        Object annotationValue) {
        if (annotationValue instanceof Enum) {
            Enum value = (Enum) annotationValue;
            addImport(classifier, value.getClass());
        }
        return builder.addAnnotationParameter(annotation,
                                              annotationName,
                                              annotationValue);
    }

    public void addAnnotationClassParameter(ImportsManager manager, ObjectModelClassifier output, ObjectModelAnnotation annotation, String name, Class<?> value) {
        String importAndSimplify = manager.importAndSimplify(value.getName());
        addAnnotationParameter(output, annotation, name, importAndSimplify + ".class");
    }

    public void addAnnotationClassParameter(ImportsManager manager, ObjectModelClassifier output, ObjectModelAnnotation annotation, String name, String value) {
        String importAndSimplify = manager.importAndSimplify(value);
        addAnnotationParameter(output, annotation, name, importAndSimplify + ".class");
    }

    public void addAnnotationClassParameter(ImportsManager manager, ObjectModelClassifier output, ObjectModelAnnotation annotation, String name, Class<?>... values) {
        StringBuilder content = new StringBuilder("{");
        int max = values.length;
        for (int i = 0; i < max; i++) {
            Class<?> value = values[i];

            addImport(output, value.getName());
            String importAndSimplify = manager.importAndSimplify(value.getName());
            content.append(importAndSimplify).append(".class");
            if (i + 1 < max) {
                content.append(", ");
            }
        }
        content.append("}");
        addAnnotationParameter(output, annotation, name, content.toString());
    }

    public void addAnnotationClassParameter(ObjectModelClassifier output, ObjectModelAnnotation annotation, String name, Iterable<String> values) {
        StringBuilder content = new StringBuilder("{");
        Iterator<String> iterator = values.iterator();
        while (iterator.hasNext()) {
            String value = iterator.next();
            addImport(output, value);
            content.append(value).append(".class");
            if (iterator.hasNext()) {
                content.append(", ");
            }
        }
        content.append("}");
        addAnnotationParameter(output, annotation, name, content.toString());
    }

    /**
     * add an operation to the classifier with the form of a simple block
     * of code.
     * <p>
     * use it to add a "static {}" block into an classifier, then use
     * setOperationBody with the returned value of this method to complete
     * the creation of the static block
     *
     * @param classifier the classifier where the block will be added
     * @param modifiers  modifiers for the block
     * @return the block added
     */
    public ObjectModelOperation addBlock(ObjectModelClassifier classifier,
                                         ObjectModelModifier... modifiers) {
        return builder.addBlock(classifier, modifiers);
    }

    /**
     * Add a stereotype to the given {@code element}.
     *
     * @param element    the element on which the stereotype will be added
     * @param stereotype the stereotype to add
     * @since 2.1.2
     */
    public void addStereotype(ObjectModelElement element, String stereotype) {
        builder.addStereotype(element, stereotype);
    }

    /**
     * Clone the {@code source} operation into the {@code destination} classifier.
     * name, returnType, parameters, exceptions and tagValues will be cloned.
     * You can specify {@code modifiers} for the result operation.
     *
     * @param source             operation to clone
     * @param destination        classifier where result operation will be added
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers          extra modifiers
     * @return the new operation created in destination classifier
     * @since 2.1.2
     */
    public ObjectModelOperation cloneOperationSignature(ObjectModelOperation source,
                                                        ObjectModelClassifier destination,
                                                        boolean cloneDocumentation,
                                                        ObjectModelModifier... modifiers) {

        ObjectModelOperation outputOperation =
                addOperation(destination, source.getName(), source.getReturnType(), modifiers);

        if (cloneDocumentation && GeneratorUtil.hasDocumentation(source)) {
            setDocumentation(outputOperation, source.getDocumentation());
        }

        for (ObjectModelParameter parameter : source.getParameters()) {

            ObjectModelParameter outputParam =
                    addParameter(outputOperation, parameter.getType(), parameter.getName());

            if (cloneDocumentation && GeneratorUtil.hasDocumentation(parameter)) {
                setDocumentation(outputParam, parameter.getDocumentation());
            }
        }

        for (String exception : source.getExceptions()) {
            addException(outputOperation, exception);
        }

        cloneTagValues(source, outputOperation);

        return outputOperation;
    }

    /**
     * Clone the {@code source} operation into the {@code destination} classifier.
     * whole signature, tagValues and body code will be cloned. You can specify
     * {@code modifiers} for the result operation.
     *
     * @param source             operation to clone
     * @param destination        classifier where result operation will be added
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers          extra modifiers
     * @return the new operation created in destination classifier
     * @since 2.1.2
     */
    public ObjectModelOperation cloneOperation(ObjectModelOperation source,
                                               ObjectModelClassifier destination,
                                               boolean cloneDocumentation,
                                               ObjectModelJavaModifier... modifiers) {
        ObjectModelOperation outputOperation = cloneOperationSignature(
                source,
                destination,
                cloneDocumentation,
                modifiers
        );

        // add body only if operation is not abstract
        boolean opAbstract = false;
        for (ObjectModelModifier modifier : modifiers) {
            if (modifier == ObjectModelJavaModifier.ABSTRACT) {
                opAbstract = true;
                break;
            }
        }
        if (!opAbstract) {
            setOperationBody(outputOperation, source.getBodyCode());
        }
        return outputOperation;
    }

    /**
     * clone a given attribute into a classifier of the output model
     *
     * @param source             the original attribute
     * @param destination        classifier where the clone will be added
     * @param cloneDocumentation flag to add documentation if some found in model
     * @param modifiers          extra modifiers
     * @return the clone attribute
     * @since 2.1.2
     */
    public ObjectModelAttribute cloneAttribute(ObjectModelAttribute source,
                                               ObjectModelClassifier destination,
                                               boolean cloneDocumentation,
                                               ObjectModelModifier... modifiers) {

        ObjectModelAttribute outputAttribute = addAttribute(destination,
                                                            source.getName(), source.getType(),
                                                            source.getDefaultValue(), modifiers);

        cloneTagValues(source, outputAttribute);

        if (cloneDocumentation) {
            setDocumentation(outputAttribute, source.getDocumentation());
        }

        for (String comment : source.getComments()) {
            addComment(outputAttribute, comment);
        }

        setMinMultiplicity(outputAttribute, source.getMinMultiplicity());
        setMaxMultiplicity(outputAttribute, source.getMaxMultiplicity());
        setNavigable(outputAttribute, source.isNavigable());

        return outputAttribute;
    }

    /**
     * Copy all tag values fro the given {@code source} to the given
     * {@code destination}.
     *
     * @param source      the source element
     * @param destination the destination element
     * @since 2.3
     */
    public void cloneTagValues(ObjectModelElement source,
                               ObjectModelElement destination) {
        Map<String, String> tags = source.getTagValues();
        for (Map.Entry<String, String> entry : tags.entrySet()) {
            addTagValue(destination, entry.getKey(), entry.getValue());
        }
    }

    /**
     * @param source      the source element
     * @param destination the source element
     * @since 2.3
     */
    public void cloneStereotypes(ObjectModelClassifier source,
                                 ObjectModelClassifier destination) {

        Set<String> stereotypes = source.getStereotypes();
        for (String stereotype : stereotypes) {
            addStereotype(destination, stereotype);
        }
    }

    /**
     * copy attributes, interfaces declartion and operation of a given classifier
     * into another classifier.
     * <p>
     * class-specific, enumeration-specific and interface-specific features
     * of the given classifier <strong>will not</strong> be present in the clone.
     * To copy those specific elements, use
     *
     * @param source            the classifier to clone from the source model
     * @param destination       where to clone the given source one
     * @param copyDocumentation flag to add documentation if some found in model {@link #cloneClassifier(ObjectModelClassifier, boolean)}
     * @since 2.1.2
     * @deprecated since 2.3, prefer use the {@link #cloneClass(ObjectModelClass, boolean)} method
     */
    @Deprecated
    public void copyClassifier(ObjectModelClassifier source,
                               ObjectModelClassifier destination,
                               boolean copyDocumentation) {

        cloneClassifier(source, destination, copyDocumentation);
    }

    /**
     * copy attributes, interfaces declartion and operation of a given classifier
     * into another classifier.
     * <p>
     * class-specific, enumeration-specific and interface-specific features
     * of the given classifier <strong>will not</strong> be present in the clone.
     * To copy those specific elements, use
     *
     * @param source            the classifier to clone from the source model
     * @param destination       where to clone the given source one
     * @param copyDocumentation flag to add documentation if some found in model {@link #cloneClassifier(ObjectModelClassifier, boolean)}
     * @since 2.3
     */
    public void cloneClassifier(ObjectModelClassifier source,
                                ObjectModelClassifier destination,
                                boolean copyDocumentation) {

        cloneTagValues(source, destination);

        cloneStereotypes(source, destination);

        for (ObjectModelAttribute attribute : source.getAttributes()) {
            cloneAttribute(attribute, destination, copyDocumentation);
        }

        for (ObjectModelInterface interfacez : source.getInterfaces()) {
            addInterface(destination, interfacez.getQualifiedName());
        }

        for (ObjectModelOperation operation : source.getOperations()) {
            cloneOperation(operation, destination, copyDocumentation);
        }
    }


    /**
     * creates a clone of the given {@code source} class in the output model
     * and clones attributes, inheritence declarations and operations into the
     * clone
     *
     * @param source             the class to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given class
     * @since 2.1.2
     */
    public ObjectModelClass cloneClass(ObjectModelClass source,
                                       boolean cloneDocumentation) {
        ObjectModelClass outputClass =
                createClass(source.getName(), source.getPackageName());

        cloneClassifier(source, outputClass, cloneDocumentation);

        if (source.getSuperclasses().size() > 1) {
            log.warn(source.getQualifiedName() + " has multiple inheritence, some of them will be ignored");
        }

        for (ObjectModelClass superClass : source.getSuperclasses()) {
            setSuperClass(outputClass, superClass.getQualifiedName());

            // TODO 20100812 bleny is there something to do with discriminator for superClass ? 
        }

        if (!CollectionUtils.isEmpty(source.getInnerClassifiers())) {
            for (ObjectModelClassifier classifier : source.getInnerClassifiers()) {
                ObjectModelClassifier innerClassifierClone =
                        cloneClassifier(classifier, cloneDocumentation);
                addInnerClassifier(outputClass,
                                   ObjectModelType.OBJECT_MODEL_CLASSIFIER,
                                   innerClassifierClone.getName());
            }
        }
        return outputClass;
    }

    /**
     * creates a clone of the given {@code source} interface in the output model
     * and clones attributes, inheritence declaration and operations into the
     * clone
     *
     * @param source             the interface to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given interface
     * @since 2.1.2
     */
    public ObjectModelInterface cloneInterface(ObjectModelInterface source,
                                               boolean cloneDocumentation) {
        ObjectModelInterface outputInterface =
                createInterface(source.getName(), source.getPackageName());

        cloneClassifier(source, outputInterface, cloneDocumentation);
        // nothing more to do. copyClassifier already done the job

        return outputInterface;
    }

    /**
     * creates a clone of the given {@code source} enumeration in the output
     * model and clones attributes, inheritence declaration, operations and
     * literals into the clone
     *
     * @param source             the enumeration to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given enumeration
     * @since 2.1.2
     */
    public ObjectModelEnumeration cloneEnumeration(ObjectModelEnumeration source,
                                                   boolean cloneDocumentation) {
        ObjectModelEnumeration outputEnumeration =
                createEnumeration(source.getName(), source.getPackageName());

        cloneClassifier(source, outputEnumeration, cloneDocumentation);

        for (String literal : source.getLiterals()) {
            addLiteral(outputEnumeration, literal);
        }

        return outputEnumeration;
    }

    /**
     * creates a clone of the given {@code source} classifier in the output
     * model and clones attributes, inheritence declaration and operations
     * <p>
     * class-specific, enumeration-specific and interface-specific features
     * of the given classifier <strong>will</strong> be present in the clone
     *
     * @param source             the classifier to clone from the source model
     * @param cloneDocumentation flag to add documentation if some found in model
     * @return the clone of the given classifier
     * @since 2.1.2
     */
    public ObjectModelClassifier cloneClassifier(ObjectModelClassifier source,
                                                 boolean cloneDocumentation) {
        ObjectModelClassifier clone = null;
        if (source.isInterface()) {
            clone = cloneInterface((ObjectModelInterface) source, cloneDocumentation);
        } else if (source.isClass()) {
            clone = cloneClass((ObjectModelClass) source, cloneDocumentation);
        } else if (source.isEnum()) {
            clone = cloneEnumeration((ObjectModelEnumeration) source, cloneDocumentation);
        } else {
            // should never occur
            log.error("strange classifier " + source);
        }
        return clone;
    }

    /**
     * Search in {@code input} model, for a enumeration dependency named
     * {@link JavaGeneratorUtil#DEPENDENCIES_CONSTANTS}.
     * <p>
     * If found, add all literals on {@code output} model as constants.
     * <p>
     * You can add a {@link EugeneCoreTagValues.Store#constantPrefix} to your
     * enumeration to prefix constant names.
     *
     * @param input  the input model
     * @param output the ouput model
     * @return the set of constants names generated
     */
    public Set<String> addConstantsFromDependency(ObjectModelClassifier input,
                                                  ObjectModelClassifier output) {
        Set<String> literals = eugeneTagValues.getConstants(input);
        if (literals == null) {
            // constant attributes via 'constants' enumeration dependency
            // literals of enumeration will be values and constant name will be generated
            ObjectModelDependency constantsDependencies = input.getDependency(JavaGeneratorUtil.DEPENDENCIES_CONSTANTS);
            if (constantsDependencies == null) {
                // no dependency found
                return Set.of();
            }
            ObjectModelClassifier classifier = constantsDependencies.getSupplier();
            if (!(classifier instanceof ObjectModelEnumeration)) {
                // do not have an enumeration
                return Set.of();
            }
            ObjectModelEnumeration constants = (ObjectModelEnumeration) classifier;
            // add all literal of dependency as constants in interface
            literals = new HashSet<>(constants.getLiterals());
            if (!literals.isEmpty()) {
                log.warn("");
                log.warn("Using constants dependencies is deprecated, use now tagValue *constants* instead.");
                log.warn("");
            }
        }
        Set<String> constantNames = new LinkedHashSet<>();
        for (String literal : literals) {
            // always use builder method to obtain constant name to not use a
            // possible altered by transformer
            String constantName = getConstantName(literal);
            addConstant(output, constantName, String.class, "\"" + literal + "\"", ObjectModelJavaModifier.PUBLIC);
            constantNames.add(constantName);
        }
        return constantNames;
    }

    public String getConstantPrefix() {
        return constantPrefix;
    }

    public void setConstantPrefix(String constantPrefix) {
        this.constantPrefix = constantPrefix;
    }

    public String getJavaBeanMethodName(String methodPrefix, String propertyName) {
        return methodPrefix + JavaGeneratorUtil.capitalizeJavaBeanPropertyName(propertyName);
    }

    public String getJavaBeanMethodName(String methodPrefix, String propertyName, String methodSuffix) {
        return getJavaBeanMethodName(methodPrefix, propertyName) + methodSuffix;
    }

    /**
     * Used to initialize constant prefix depends on model tagvalue {@link
     * EugeneCoreTagValues.Store#constantPrefix}. If no tagvalue found, the default
     * value will be an empty prefix.
     */
    public void initConstantPrefixFromModel() {
        constantPrefix = getConstantPrefix(null);
    }

    public URL getFileInClassPath(String fqn) {
        String resourceName = fqn.replaceAll("\\.", "/") + ".java";
        URL fileLocation = getClassLoader().getResource(resourceName);
        if (log.isDebugEnabled()) {
            log.debug("Look for resource : " + resourceName + " = " + fileLocation);
        }
        return fileLocation;
    }

    /**
     * Generates a static block with I18n keys for each navigable attributes
     * prefixed by the given {@code i18nPrefix}.
     *
     * @param input      the input classifier
     * @param output     the output classifier (to generate)
     * @param i18nPrefix the i81n prefix to use
     */
    public void generateI18nBlock(ObjectModelClassifier input,
                                  ObjectModelClassifier output,
                                  String i18nPrefix) {

        ObjectModelOperation block =
                addBlock(output, ObjectModelJavaModifier.STATIC);

        StringBuilder buffer = new StringBuilder(300);
        addImport(output, I18n.class);
        addI18n(buffer, i18nPrefix, Introspector.decapitalize(input.getName()));
        for (ObjectModelAttribute attr : input.getAttributes()) {
            //tchemit-20100225 only treate navigable relations
            if (attr.isNavigable()) {
                addI18n(buffer,
                        i18nPrefix,
                        Introspector.decapitalize(attr.getName())
                );
            }
        }
        setOperationBody(block, buffer.toString());
    }

    public void addI18n(StringBuilder buffer, String i18nPrefix,
                        String suffix) {
        buffer.append("\n        I18n.n(\"");
        buffer.append(i18nPrefix);
        buffer.append(suffix);
        buffer.append("\");");
    }

    /**
     * Checks if the classifier fully qualified name is in class-path and log a
     * message that fqn will not generated if found
     *
     * @param classifier classifier to test
     * @return {@code true} if fqn of classifier was found in class-path,
     * {@code false} otherwise.
     * @since 2.5
     * @deprecated since 3.0, use the {@link #getResourcesHelper()} object directly.
     */
    @Deprecated
    public boolean isInClassPath(ObjectModelClassifier classifier) {
        return isInClassPath(classifier.getQualifiedName());
    }

    /**
     * Checks if the given fully qualified name is in class-path and log a
     * message that fqn will not generated if found.
     *
     * @param fqn fully qualified name to test
     * @return {@code true} if fqn was found in class-path,
     * {@code false} otherwise.
     * @since 2.5
     * @deprecated since 3.0, use the {@link #getResourcesHelper()} object directly.
     */
    @Deprecated
    public boolean isInClassPath(String fqn) {
        return getResourcesHelper().isJavaFileInClassPath(fqn);
    }

    /**
     * Checks if the given class (by his package and simple class name)
     * is in class-path and log a message that fqn will not generated if found.
     *
     * @param packageName package name of the class to test
     * @param className   class simple name of the class to test
     * @return {@code true} if fqn was found in class-path,
     * {@code false} otherwise.
     * @since 2.7.1
     * @deprecated since 3.0, use the {@link #getResourcesHelper()} object directly.
     */
    @Deprecated
    public boolean isInClassPath(String packageName, String className) {

        return isInClassPath(packageName + "." + className);
    }

    public void storeServiceLoaderFile(Class<?> type, List<? extends ObjectModelClassifier> classes, Function<? super ObjectModelClassifier, String> function) {
        Path outputDirectory = getConfiguration().getProperty(Template.PROP_RESOURCE_DIRECTORY, File.class).toPath();

        boolean write = ServiceLoaderUtil.store(outputDirectory, type, toQualifiedNames(classes, function::apply));
        if (write) {
            getLog().info(String.format("Update service loader file (type %s) with %d classes.", type.getName(), classes.size()));
        } else {
            if (isVerbose()) {
                getLog().info(String.format("Service loader file up to date (type %s) with %d classes.", type.getName(), classes.size()));
            }
        }
    }

    public List<String> toQualifiedNames(List<? extends ObjectModelClassifier> classes, Function<? super ObjectModelClassifier, String> function) {
        classes.sort(Comparator.comparing(ObjectModelClassifier::getQualifiedName));
        return classes.stream().map(function).collect(Collectors.toList());
    }

    public String getMethodName(boolean useRelativeName, String[] relativeNameExcludes, String methodPrefix, String beanPackageName, String beanName) {
        String methodName;
        if (beanPackageName != null) {
            beanName = beanPackageName + "." + beanName;
        }
        if (useRelativeName) {
            methodName = methodPrefix + Strings.getRelativeCamelCaseName(getDefaultPackageName(), beanName, relativeNameExcludes);
        } else {
            methodName = methodPrefix + GeneratorUtil.getSimpleName(beanName);
        }
        return methodName;
    }
}
