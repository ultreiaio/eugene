package org.nuiton.eugene.java;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by tchemit on 05/12/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ServiceLoaderUtil {

    public static List<String> load(Path classPathDirectory, Class<?> type) {
        Path filePath = getServiceLoaderFile(classPathDirectory, type);
        try {
            return Files.exists(filePath) ? Files.readAllLines(filePath) : List.of();
        } catch (IOException e) {
            throw new IllegalStateException("Can't load service loader file at: " + filePath, e);
        }
    }

    public static boolean store(Path classPathDirectory, Class<?> type, List<String> implementations) {

        List<String> load = load(classPathDirectory, type);
        if (load.equals(implementations)) {
            // file is up to date
            return false;
        }
        Path filePath = getServiceLoaderFile(classPathDirectory, type);
        try {
            if (!Files.exists(filePath.getParent())) {
                Files.createDirectories(filePath.getParent());
            }
            Files.write(filePath, implementations);
            return true;
        } catch (IOException e) {
            throw new IllegalStateException("Can't store service loader file at: " + filePath, e);
        }
    }

    public static Path getServiceLoaderFile(Path classPathDirectory, Class<?> type) {
        return classPathDirectory.resolve("META-INF").resolve("services").resolve(type.getName());
    }
}
