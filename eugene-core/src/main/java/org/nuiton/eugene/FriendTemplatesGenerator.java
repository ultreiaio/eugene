package org.nuiton.eugene;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.friend.ModelDef;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelGenerator;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by tchemit on 24/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class FriendTemplatesGenerator extends ObjectModelGenerator {

    //TODO: tenir compte des valeurs par defaut : afficher seulement les valeurs différentes des valeurs par défaut

    @Override
    public void generateFromModel(Writer output, ObjectModel input) throws IOException {

        ModelDef modelDef = ModelDef.of(input);
        try (BufferedWriter writer = new BufferedWriter(output)) {
            modelDef.write(writer);
        }
    }

    @Override
    public String getFilenameForModel(ObjectModel model) {
        return model.getName() + ".friendmodel";
    }

}
