package org.nuiton.eugene;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelElement;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Created on 24/09/16.
 * <p>
 * Defines all tag values managed by Eugene.
 * <p>
 * In another library using eugene, please extends this contract to put your
 * own tag values, to get a unique place where to find tag values.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@AutoService(TagValueMetadatasProvider.class)
public class EugeneCoreTagValues extends DefaultTagValueMetadatasProvider {

    @Override
    public String getDescription() {
        return "EUGeNe Core tag values";
    }

    public enum Store implements TagValueMetadata {

        /**
         * Tag value to add the version of the model from outside (says in the
         * properties file associated to the model)..
         * <p>
         * Actually, the eugene api does not use to modify the model. ItaTa is only
         * used while reading the properties associated with a model and if found is
         * directly set to the {@code version} field of the model.
         *
         * @since 2.3
         */
        version("Version of model", String.class, null, ObjectModel.class),

        /**
         * Tag value to add on constants enumeration (or other incoming dev)
         * a prefix to constant to generate.
         * <p>
         * You can globaly use it on the complete model or to a specific classifier.
         *
         * @since 2.5
         */
        documentation("Documentation", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelElement.class),

        /**
         * Tag value to add on constants enumeration (or other incoming dev)
         * a prefix to constant to generate.
         * <p>
         * You can globaly use it on the complete model or to a specific classifier.
         *
         * @since 2.3
         */
        constantPrefix("Constant field prefix", String.class, "PROPERTY_", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to specify the i18n prefix to use whene generating i18n keys.
         * <p>
         * You can globaly use it on the complete model or to a specific classifier.
         *
         * @since 2.3
         */
        i18n("I18n prefix", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to generate also {@code getXXX} methods for a boolean property.
         *
         * @since 2.12
         */
        generateBooleanGetMethods("Generate getter on Boolean", boolean.class, "false", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a generic to an attribute.
         *
         * @since 3.0
         */
        attributeGeneric("Generic type of attribute", String.class, null, ObjectModelAttribute.class),

        /**
         * Stereotype to mark an attribute with multiplicity as ordered.
         * <p>
         * It means that order of insertion is maintained: in Java, it will lead
         * to a collection typed with {@link java.util.List} or {@link java.util.LinkedHashSet}
         * or {@link java.util.LinkedHashMap}.
         *
         * @since 2.8
         */
        ordered("Is attribute ordered ?", boolean.class, null, ObjectModelAttribute.class),
        /**
         * Stereotype to mark an attribute with multiplicity as unique.
         * <p>
         * It means that uniqueness of elements is maintained in the collection: in Java, it
         * will lead to a collection typed with {@link java.util.Set}.
         */
        unique("Is attribute unique ?", boolean.class, null, ObjectModelAttribute.class),
        /**
         * Stereotype to set default value of an attribute.
         */
        defaultValue("Default value", String.class, null, ObjectModelAttribute.class),
        /**
         * Stereotype to skip generation for some templates.
         */
        skip("Skip generation", boolean.class, null, ObjectModelClassifier.class, ObjectModelPackage.class),

        /**
         * Tag value to specify the default package used to generate.
         *
         * @since 3.0
         */
        defaultPackage("Default package", String.class, null, ObjectModel.class),
        /**
         * To add some constants to a class (will create PROPERTY_XXX constants).
         * <p>
         * You can globally use it on the complete model.
         *
         * @see #getConstants(ObjectModelClassifier)
         * @since 3.0
         */
        constants("Add constants from properties names separate by comma", String[].class, null, ObjectModelClassifier.class),
        /**
         * To use relative name in some generated classes as model (to be able to use same object name in different packages).
         * <p>
         * You can globally use it on the complete model.
         *
         * @see #isUseRelativeName(ObjectModel)
         * @since 3.0
         */
        useRelativeName("Pour utiliser des noms relatifs au paquetage par défaut pour autoriser les homonymes", boolean.class, "false", ObjectModel.class),
        /**
         * When using relative names, you can define some package starter to exclude.
         * <p>
         * You can globally use it on the complete model.
         *
         * @see #getRelativeNameExcludes(ObjectModel)
         * @since 3.0
         */
        relativeNameExcludes("Les paquetages à exclure lors de la résolution d'un nom relatif", String[].class, null, ObjectModel.class);


        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String _defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this._defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return _defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public EugeneCoreTagValues() {
        super((TagValueMetadata[]) Store.values());
    }

    /**
     * Obtain the value of the {@link Store#documentation} tag value on the given model.
     *
     * @param model model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#documentation
     * @since 2.3
     */
    public String getDocumentationTagValue(ObjectModel model) {
        return TagValueUtil.findTagValue(Store.documentation, model);
    }

    /**
     * Obtain the value of the {@link Store#documentation} tag value on the given element.
     *
     * @param element element to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#documentation
     * @since 2.3
     */
    public String getDocumentationTagValue(ObjectModelElement element) {
        return TagValueUtil.findTagValue(Store.documentation, element);
    }

    /**
     * Obtain the value of the {@link Store#documentation} tag value on the given package.
     *
     * @param aPackage package to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#documentation
     * @since 2.12
     */
    public String getDocumentationTagValue(ObjectModelPackage aPackage) {
        return TagValueUtil.findTagValue(Store.documentation, aPackage);
    }

    /**
     * Cherche et renvoie le préfixe i18n à utiliser sur cet element, sinon sur
     * le model.
     *
     * @param element  element to seek
     * @param aPackage package to seek
     * @param model    model to seek
     * @return le préfixe i18n ou <code>null</code> si non spécifié
     * @since 2.3
     */
    public String getI18nPrefixTagValue(ObjectModelElement element, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.i18n, element, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateBooleanGetMethods}
     * tag value on the given model, package or classifier.
     * <p>
     * It will first look on the model, then on package and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateBooleanGetMethods
     * @since 2.12
     */
    public boolean isGenerateBooleanGetMethods(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateBooleanGetMethods, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#constantPrefix} tag value on the given model or classifier.
     * <p>
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#constantPrefix
     * @since 2.3
     */
    public String getConstantPrefixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.constantPrefix, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#attributeGeneric} tag value on the given attribute.
     *
     * @param attribute attribute to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#attributeGeneric
     * @since 3.0
     */
    public String getAttributeGenericTagValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findTagValue(Store.attributeGeneric, attribute);
    }

    /**
     * Check if the given attribute has the {@link Store#ordered} boolean tag value.
     *
     * @param attribute attribute to test
     * @return {@code true} if boolean tag value was found, {@code false otherwise}
     * @see Store#ordered
     * @since 2.9
     */
    public static boolean isOrdered(ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.ordered, attribute);
    }

    /**
     * Check if the given attribute has the {@link Store#unique} boolean tag value.
     *
     * @param attribute attribute to test
     * @return {@code true} if boolean tag value was found, {@code false otherwise}
     * @see Store#unique
     * @since 2.9
     */
    public static boolean isUnique(ObjectModelAttribute attribute) {
        return TagValueUtil.findBooleanTagValue(Store.unique, attribute);
    }

    /**
     * Check if the given attribute has the {@link Store#defaultValue} boolean tag value.
     *
     * @param attribute attribute to test
     * @return default value or {@code null} if not found.
     * @see Store#defaultValue
     * @since 3.0
     */
    public static String getDefaultValue(ObjectModelAttribute attribute) {
        return TagValueUtil.findDirectTagValue(Store.defaultValue, attribute);
    }

    /**
     * Check if the given classifier has the {@link Store#skip} boolean tag value.
     *
     * @param classifier classifier to test
     * @param aPackage   package to test
     * @return {@code true} if boolean tag value was found, {@code false otherwise}
     * @see Store#skip
     * @since 2.9
     */
    public static boolean isSkip(ObjectModelClassifier classifier, ObjectModelPackage aPackage) {
        return TagValueUtil.findBooleanTagValue(Store.skip, classifier, aPackage);
    }

    /**
     * Check if the given package has the {@link Store#skip} boolean tag value.
     *
     * @param aPackage package to test
     * @return {@code true} if boolean tag value was found, {@code false otherwise}
     * @see Store#skip
     */
    public static boolean isSkip(ObjectModelPackage aPackage) {
        return TagValueUtil.findBooleanTagValue(Store.skip, aPackage);
    }


    /**
     * Obtain the value of the {@link Store#defaultPackage} tag value on the given element.
     *
     * @param element element to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#defaultPackage
     * @since 2.3
     */
    public String getDefaultPackage(ObjectModelElement element) {
        return TagValueUtil.findTagValue(Store.defaultPackage, element);
    }

    /**
     * Get tag value {@link Store#constants} on given {@code classifier}.
     *
     * @param classifier classifier to test
     * @return {@code null} if tag value was not found, set of constants to create.
     * @see Store#constants
     */
    public Set<String> getConstants(ObjectModelClassifier classifier) {
        String notEmptyTagValue = TagValueUtil.findNotEmptyTagValue(Store.constants, classifier);
        return notEmptyTagValue == null ? Set.of() : new LinkedHashSet<>(Arrays.asList(notEmptyTagValue.split("\\s*,\\s*")));
    }


    /**
     * Obtain the value of the {@link Store#useRelativeName} tag value on the given model.
     *
     * @param model model to seek
     * @return {@code true} the none empty value of the found tag value or {@code false} if not found nor empty.
     * @see Store#useRelativeName
     * @since 3.0
     */
    public boolean isUseRelativeName(ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.useRelativeName, model);
    }

    /**
     * Get tag value {@link Store#relativeNameExcludes} on given {@code model}.
     *
     * @param model model to seek
     * @return {@code null} if tag value was not found, set of relative name exclude.
     * @see Store#relativeNameExcludes
     */
    public Set<String> getRelativeNameExcludes(ObjectModel model) {
        String notEmptyTagValue = TagValueUtil.findNotEmptyTagValue(Store.relativeNameExcludes, model);
        return notEmptyTagValue == null ? Set.of() : new LinkedHashSet<>(Arrays.asList(notEmptyTagValue.split("\\s*,\\s*")));
    }

}
