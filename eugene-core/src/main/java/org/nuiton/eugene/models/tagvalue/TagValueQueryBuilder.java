package org.nuiton.eugene.models.tagvalue;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.io.ModelExtensionFileParser;

import java.util.Map;

/**
 * Created on 14/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
@SuppressWarnings("unused")
public final class TagValueQueryBuilder {

    private final TagValuesStore store;

    public static TagValueQueryBuilder create(TagValuesStore store) {
        return new TagValueQueryBuilder(store);
    }

    public TagValueQueryBuilder(TagValuesStore store) {
        this.store = store;
    }

    public OnModel onModel(String modelName) {
        return new Builder().onModel(modelName);
    }

    public OnPackage onPackage(String packageName) {
        return new Builder().onPackage(packageName);
    }

    public OnClassifier onClassifier(String classifierName) {
        return new Builder().onClassifier(classifierName);
    }

    public interface OnTagValue {
        StepResult onTagValue(String tagValue);
    }

    public interface StepResult {
        String single();
    }

    public interface StepMultipleResult {

        Map<String, String> multiple();
    }

    public interface StepWith {
        OnModel onModel(String modelName);

        OnPackage onPackage(String packageName);

        OnClassifier onClassifier(String classifierName);
    }

    public interface OnModel extends OnTagValue, StepMultipleResult {
    }

    public interface OnPackage extends OnTagValue, StepMultipleResult {

    }

    public interface OnClassifier extends OnTagValue, StepMultipleResult {
        OnAttribute onAttribute(String attributeName);
    }

    public interface OnAttribute extends OnTagValue, StepMultipleResult {
    }

    private class Builder implements StepWith, OnModel, OnPackage, OnClassifier, OnAttribute, OnTagValue, StepResult {

        private String key;

        @Override
        public String single() {
            return store.getStore().getProperty(key);
        }

        @Override
        public Map<String, String> multiple() {
            return store.getTagValues(key);
        }

        @Override
        public StepResult onTagValue(String tagValue) {
            key += "." + ModelExtensionFileParser.TAG_VALUE + "." + tagValue;
            return this;
        }

        @Override
        public OnModel onModel(String modelName) {
            key = ModelExtensionFileParser.MODEL + "." + modelName;
            return this;
        }

        @Override
        public OnPackage onPackage(String packageName) {
            key = ModelExtensionFileParser.PACKAGE + "." + packageName;
            return this;
        }

        @Override
        public OnClassifier onClassifier(String classifierName) {
            key = classifierName + "." + ModelExtensionFileParser.CLASS;
            return this;
        }

        @Override
        public OnAttribute onAttribute(String attributeName) {
            key = key.replace("." + ModelExtensionFileParser.CLASS, "");
            key += "." + ModelExtensionFileParser.ATTRIBUTE + "." + attributeName;
            return this;
        }
    }

}
