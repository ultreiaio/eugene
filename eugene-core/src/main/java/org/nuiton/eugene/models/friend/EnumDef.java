package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.xml.ObjectModelEnumerationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImplRef;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class EnumDef extends ClassifierDef<EnumDef> {

    static final String PREFIX = "enum ";

    public static EnumDef of(ObjectModelEnumeration anEnumeration, String defaultPackage) {

        EnumDef result = new EnumDef(getRelativeType(anEnumeration.getQualifiedName(), defaultPackage), defaultPackage);
        result.loadStereotypesAndTagValues(anEnumeration);
        for (String literal : anEnumeration.getLiterals()) {
            result.add(literal);
        }
        return result;
    }

    public static EnumDef of(String line, List<String> groupLines, String defaultPackage) {

        String firstLine = StringUtils.removeStart(line, PREFIX).trim();
        Pair<String, String> pair = splitTagValues(firstLine);
        firstLine = pair.getLeft();
        String tagValues = pair.getRight();

        EnumDef result = new EnumDef(firstLine.trim(), defaultPackage);
        if (tagValues != null) {
            result.loadStereotypesAndTagValues(tagValues);
        }
        for (String groupLine : groupLines) {
            result.add(groupLine.trim());
        }

        return result;
    }

    /** Logger. */
    private static final Logger log = LogManager.getLogger(EnumDef.class);

    private final List<String> literals = new LinkedList<>();

    private EnumDef(String name, String defaultPackage) {
        super(name, defaultPackage);
    }

    @Override
    public void write(BufferedWriter writer) throws IOException {
        writer.newLine();
        writer.append(PREFIX).append(getName());
        writeStereotypesAndTagValues(writer);
        for (String literal : literals) {
            writer.newLine();
            writer.append(literal);
        }
        writer.newLine();
    }

    public void add(String literal) {
        log.debug(String.format("Add literal: %s on %s", literal, getName()));
        literals.add(literal);
    }

    public List<String> getLiterals() {
        return literals;
    }

    @Override
    public String toString() {
        return getName() + (literals.isEmpty() ? "" : "(" + Joiner.on(",").join(literals) + ")");
    }

    public ObjectModelEnumerationImpl toObjectModel() {
        ObjectModelEnumerationImpl result = new ObjectModelEnumerationImpl();
        toObjectModel(result);
        for (String literal : literals) {
            ObjectModelImplRef ref = new ObjectModelImplRef();
            ref.setName(literal);
            result.addLiteral(ref);
        }
        return result;
    }
}
