package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.xml.ObjectModelAttributeImpl;

import java.beans.Introspector;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Map;
import java.util.Objects;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class AttributeDef extends ElementDef {
    private final String type;
    private String defaultValue;
    private String fullyQualifiedType;
    private String reverseName;
    private String cardinality;
    private String relationType;

    public AttributeDef(String name, String type, String reverseName, String cardinality, String relationType) {
        super(name);
        this.type = type;
        this.reverseName = reverseName;
        this.cardinality = cardinality;
        this.relationType = relationType;
    }

    public static AttributeDef of(ObjectModelAttribute anAttribute, String defaultPackage, String defaultReverseName) {
        String relationType = null;
        if (anAttribute.isComposite()) {
            relationType = "+";
        }
        int minMultiplicity = anAttribute.getMinMultiplicity();
        int maxMultiplicity = anAttribute.getMaxMultiplicity();
        String cardinality = minMultiplicity == maxMultiplicity ? minMultiplicity + "" : minMultiplicity + ".." + maxMultiplicity;
        if (cardinality.equals("0..-1")) {
            cardinality = "*";
        } else if (cardinality.equals("1..-1")) {
            cardinality = "+";
        }
        int reverseMaxMultiplicity = anAttribute.getReverseMaxMultiplicity();
        if (reverseMaxMultiplicity == -1) {
            cardinality = "*:" + cardinality;
        }
        if ("1".equals(cardinality)) {
            cardinality = null;
        }
        String reverseName = anAttribute.getReverseAttributeName();
        if (reverseName != null) {
            reverseName = reverseName.trim();
            if (defaultReverseName.equals(reverseName)) {
                reverseName = null;
            }
        }
        AttributeDef result = new AttributeDef(anAttribute.getName(),
                                               getRelativeType(anAttribute.getType(), defaultPackage),
                                               reverseName,
                                               cardinality,
                                               relationType);
        result.loadStereotypesAndTagValues(anAttribute);
        return result;
    }

    public static AttributeDef of(String firstLine) {
        Pair<String, String> pair = splitTagValues(firstLine);
        firstLine = pair.getLeft();
        String tagValues = pair.getRight();

        int typeIndexOf = firstLine.lastIndexOf(' ');
        String type = firstLine.substring(typeIndexOf + 1).trim();
        firstLine = firstLine.substring(0, typeIndexOf).trim();

        String cardinality = null;
        if (firstLine.contains("{")) {
            int parameterFirstIndex = firstLine.indexOf('{');
            int parameterLastIndex = firstLine.indexOf('}');
            cardinality = firstLine.substring(parameterFirstIndex, parameterLastIndex + 1).replace("{", "").replace("}", "");
            firstLine = firstLine.substring(0, parameterFirstIndex).trim();
        }

        String[] parts = firstLine.split(" ");
        String name = parts[0].trim();
        String relationType = null;
        if (parts.length == 2) {
            relationType = parts[1].trim();
        }
        String[] nameParts = name.trim().split(":");
        AttributeDef attributeDef;
        if (nameParts.length == 1) {

            attributeDef = new AttributeDef(name, type, null, cardinality, relationType);
        } else {
            attributeDef = new AttributeDef(nameParts[0].trim(), type, nameParts[1].trim(), cardinality, relationType);
        }

        if (tagValues != null) {
            attributeDef.loadStereotypesAndTagValues(tagValues);
        }
        return attributeDef;
    }

    public String getType() {
        return type;
    }

    public String getReverseName() {
        return reverseName;
    }

    public String getCardinality() {
        return cardinality;
    }

    public String getRelationType() {
        return relationType;
    }

    public String getFullyQualifiedType() {
        return fullyQualifiedType;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public void addTagValue(String key, String value) {
        if ("defaultValue".equals(key)) {
            setDefaultValue(value);
        }
        super.addTagValue(key, value);
    }

    @Override
    public String toString() {
        return getName()
                + (reverseName == null ? "" : ":" + reverseName)
                + (relationType == null ? "" : " " + relationType)
                + (cardinality == null ? "" : " {" + cardinality + "}");
    }

    @Override
    public void write(BufferedWriter writer) throws IOException {
        writer.newLine();
        writer.append(getName());
        if (reverseName != null) {
            writer.append(":").append(reverseName);
        }
        writer.append(" ");
        if (relationType != null) {
            writer.append(relationType).append(" ");
        }
        if (cardinality != null) {
            writer.append("{").append(cardinality).append("} ");
        }
        writer.append(type);
        writeStereotypesAndTagValues(writer);
    }

    boolean isBidirectional() {
        return Objects.equals("-", relationType);
    }

    void resolveType(Map<String, String> classesMapping) {
        fullyQualifiedType = resolveType(type, classesMapping);
    }

    ObjectModelAttributeImpl toObjectModel(ClassifierDef container) {
        ObjectModelAttributeImpl result = new ObjectModelAttributeImpl();
        result.setName(getName());
        result.setType(getFullyQualifiedType());
        result.setReverseAttributeName(reverseName == null ? Introspector.decapitalize(container.getSimpleName()) : reverseName);
        flushStereotypesAndTagValues(result);
        result.setUnique(result.getStereotypes().contains("unique"));
        result.setDefaultValue(getDefaultValue());
        if (relationType != null) {
            if (relationType.contains("+")) {
                result.setAssociationType("composite");
            }
        }
        if (cardinality == null) {
            result.setMinMultiplicity(1);
            result.setMaxMultiplicity(1);
            result.setReverseMaxMultiplicity(1);
        } else {
            Integer min;
            Integer max;
            Integer reverseMax;

            String[] split = cardinality.split(":");
            String toCard;
            String fromCard;
            if (split.length == 1) {
                fromCard = "1";
                toCard = split[0];
            } else {
                fromCard = split[0];
                toCard = split[1];
            }

            switch (toCard) {
                case "*":
                    min = 0;
                    max = -1;
                    break;
                case "+":
                    min = 1;
                    max = -1;
                    break;
                case "0..1":
                    min = 0;
                    max = 1;
                    break;
                default:
                case "1":
                    min = 1;
                    max = 1;
                    break;
            }

            switch (fromCard) {
                case "*":
                case "+":
                    reverseMax = -1;
                    break;
                case "0..1":
                    reverseMax = 1;
                    break;
                default:
                case "1":
                    reverseMax = 1;
                    break;
            }
            result.setMinMultiplicity(min);
            result.setMaxMultiplicity(max);
            result.setReverseMaxMultiplicity(reverseMax);
        }
        result.setNavigable(true);
        return result;
    }

    public String getReverseName(ClassifierDef container) {
        return reverseName == null ? Introspector.decapitalize(container.getSimpleName()) : reverseName;

    }

    ObjectModelAttributeImpl toReverseModel(ClassifierDef container) {
        ObjectModelAttributeImpl result = new ObjectModelAttributeImpl();
        result.setName(getReverseName());
        result.setReverseAttributeName(getName());
        result.setType(container.getFullyQualifiedName());
        result.setUnique(false);

        if (cardinality == null) {
            result.setMinMultiplicity(1);
            result.setMaxMultiplicity(1);
            result.setReverseMaxMultiplicity(1);
        } else {
            Integer max;
            Integer reverseMax;

            String[] split = cardinality.split(":");
            String toCard;
            String fromCard;
            if (split.length == 1) {
                fromCard = "1";
                toCard = split[0];
            } else {
                fromCard = split[0];
                toCard = split[1];
            }

            switch (toCard) {
                case "*":
                    max = -1;
                    break;
                case "+":
                    max = -1;
                    break;
                case "0..1":
                    max = 1;
                    break;
                default:
                case "1":
                    max = 1;
                    break;
            }

            switch (fromCard) {
                case "*":
                case "+":
                    reverseMax = -1;
                    break;
                case "0..1":
                    reverseMax = 1;
                    break;
                default:
                case "1":
                    reverseMax = 1;
                    break;
            }
            result.setMinMultiplicity(0);
            result.setMaxMultiplicity(reverseMax);
            result.setReverseMaxMultiplicity(max);
        }
        result.setNavigable(relationType != null && relationType.contains("-"));
        return result;
    }
}
