package org.nuiton.eugene.models.tagvalue;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableMap;
import io.ultreia.java4all.util.SortedProperties;
import org.nuiton.eugene.models.extension.io.ModelExtensionFileParser;

import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * A tag value store with low level access.
 *
 * <p>
 * Created on 14/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0-alpha-39
 */
public class TagValuesStore {

    private final SortedProperties store = new SortedProperties();

    public static String getModelTagValueKey(String modelName, String name) {
        return String.format("%s.%s.%s.%s", ModelExtensionFileParser.MODEL, modelName, ModelExtensionFileParser.TAG_VALUE, name);
    }

    public static String getPackageTagValueKey(String packageName, String name) {
        return String.format("%s.%s.%s.%s", ModelExtensionFileParser.PACKAGE, packageName, ModelExtensionFileParser.TAG_VALUE, name);
    }

    public static String getClassifierTagValueKey(String classifierName, String name) {
        return String.format("%s.%s.%s.%s", classifierName, ModelExtensionFileParser.CLASS, ModelExtensionFileParser.TAG_VALUE, name);
    }

    public static String getAttributeTagValueKey(String classifierName, String attributeName, String name) {
        return String.format("%s.%s.%s.%s.%s", classifierName, ModelExtensionFileParser.ATTRIBUTE, attributeName, ModelExtensionFileParser.TAG_VALUE, name);
    }

    public String getModelTagValue(String modelName, String name) {
        return store.getProperty(getModelTagValueKey(modelName, name));
    }

    public String getPackageTagValue(String packageName, String name) {
        return store.getProperty(getPackageTagValueKey(packageName, name));
    }

    public String getClassifierTagValue(String classifierName, String name) {
        return store.getProperty(getClassifierTagValueKey(classifierName, name));
    }

    public String getAttributeTagValue(String classifierName, String attributeName, String name) {
        return store.getProperty(getAttributeTagValueKey(classifierName, attributeName, name));
    }

    public boolean hasModelTagValue(String modelName, String name) {
        return null != store.getProperty(getModelTagValueKey(modelName, name));
    }

    public void setModelTagValue(String modelName, String name, String value) {
        store.setProperty(getModelTagValueKey(modelName, name), value);
    }

    public void removeModelTagValue(String modelName, String name) {
        store.setProperty(getModelTagValueKey(modelName, name), null);
    }

    public void setPackageTagValue(String packageName, String name, String value) {
        store.setProperty(getPackageTagValueKey(packageName, name), value);
    }

    public void setClassifierTagValue(String classifierName, String name, String value) {
        store.setProperty(getClassifierTagValueKey(classifierName, name), value);
    }

    public void setAttributeTagValue(String classifierName, String attributeName, String name, String value) {
        store.setProperty(getAttributeTagValueKey(classifierName, attributeName, name), value);
    }

    public void removePackageTagValue(String packageName, String name) {
        store.setProperty(getPackageTagValueKey(packageName, name), null);
    }

    public void removeClassifierTagValue(String classifierName, String name) {
        store.setProperty(getClassifierTagValueKey(classifierName, name), null);
    }

    public void removeAttributeTagValue(String classifierName, String attributeName, String name) {
        store.setProperty(getAttributeTagValueKey(classifierName, attributeName, name), null);
    }

    public boolean hasPackageTagValue(String packageName, String name) {
        return null != store.getProperty(getPackageTagValueKey(packageName, name), null);
    }

    public boolean hasClassifierTagValue(String classifierName, String name) {
        return null != store.getProperty(getClassifierTagValueKey(classifierName, name), null);
    }

    public boolean hasAttributeTagValue(String classifierName, String attributeName, String name) {
        return null != store.getProperty(getAttributeTagValueKey(classifierName, attributeName, name), null);
    }

    public Map<String, String> getPackageTagValues(String packageName) {
        String prefix = String.format("%s.%s.%s.", ModelExtensionFileParser.PACKAGE, packageName, ModelExtensionFileParser.TAG_VALUE);
        return getTagValues(prefix);
    }

    public Map<String, String> getClassifierTagValues(String classifierName) {
        String prefix = String.format("%s.%s.%s.", classifierName, ModelExtensionFileParser.CLASS, ModelExtensionFileParser.TAG_VALUE);
        return getTagValues(prefix);
    }

    public Map<String, String> getAttributeTagValues(String classifierName, String attributeName) {
        String prefix = String.format("%s.%s.%s.%s.", classifierName, ModelExtensionFileParser.ATTRIBUTE, attributeName, ModelExtensionFileParser.TAG_VALUE);
        return getTagValues(prefix);
    }

    public Properties getStore() {
        return store;
    }

    public Map<String, String> getModelTagValues(String modelName) {
        String prefix = String.format("%s.%s.%s.", ModelExtensionFileParser.MODEL, modelName, ModelExtensionFileParser.TAG_VALUE);
        return getTagValues(prefix);
    }

    public Map<String, String> getTagValues(String prefix) {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        int length = prefix.length();
        for (String propertyName : store.stringPropertyNames()) {
            if (propertyName.startsWith(prefix)) {
                builder.put(propertyName.substring(length), store.getProperty(propertyName));
            }
        }
        return builder.build();
    }

    public List<String> getPropertyList(String prefix) {
        return null;
    }
}
