/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * ObjectModelClassImpl.
 * <p>
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 * @author Florian Desbois - desbois@codelutin.com
 */
@SuppressWarnings("UnusedReturnValue")
public class ObjectModelClassImpl extends ObjectModelClassifierImpl implements ObjectModelClass {

    private static final Logger log = LogManager.getLogger(ObjectModelClassImpl.class);
    private static Set<ObjectModelModifier> authorizedModifiers;
    protected List<ObjectModelClass> superclasses;
    protected Map<ObjectModelClass, String> superclassesDiscriminators = new HashMap<>();
    protected List<ObjectModelImplRef> superclassesRefs = new ArrayList<>();
    protected List<ObjectModelClass> specialisations;
    protected List<ObjectModelClassifier> innerClasses;

    public ObjectModelClassImpl() {
    }

    @Override
    protected Set<ObjectModelModifier> getAuthorizedModifiers() {
        if (authorizedModifiers == null) {
            // http://docs.oracle.com/javase/specs/jls/se7/html/jls-8.html#jls-8.1.1
            // public protected private abstract static final strictfp
            Set<ObjectModelModifier> modifiers = Sets.newHashSet(
                    ObjectModelJavaModifier.ABSTRACT,  // Force cast because of generics limitation
                    ObjectModelJavaModifier.STATIC,
                    ObjectModelJavaModifier.FINAL,
                    ObjectModelJavaModifier.STRICTFP);
            modifiers.addAll(ObjectModelJavaModifier.visibilityModifiers);
            authorizedModifiers = ImmutableSet.copyOf(modifiers);
        }
        return authorizedModifiers;
    }

    public void clearSuperclasses() {
        superclasses = null;
        superclassesRefs.clear();
    }

    public void addSuperclass(ObjectModelImplRef ref) {
        superclassesRefs.add(ref);
        // superclassesRefs is modified, superclasses must be reset
        superclasses = null;
        superclassesDiscriminators = null;
    }

    /**
     * Digester method to add innerClass to this ObjectModelClass.
     *
     * @param innerClass the ObjectModelClass to add
     */
    public void addInnerClassifier(ObjectModelClassifierImpl innerClass) {
        innerClass.setDeclaringElement(this);
        innerClass.setInner(true);
        if (innerClasses == null) {
            innerClasses = new ArrayList<>();
        }
        innerClasses.add(innerClass);
    }


    public void setAbstract(boolean abstractz) {
        addOrRemoveModifier(ObjectModelJavaModifier.ABSTRACT, abstractz);
    }

    @Override
    public Collection<ObjectModelClass> getSuperclasses() {
        if (superclasses == null) {
            superclasses = new ArrayList<>();
            superclassesDiscriminators = new LinkedHashMap<>();
            for (ObjectModelImplRef superclassesRef : superclassesRefs) {
                ObjectModelImplSuperClassRef ref = (ObjectModelImplSuperClassRef) superclassesRef;

                if (log.isDebugEnabled()) {
                    log.debug("Superclass ref for " + getQualifiedName() + " : " + ref.getName());
                }

                ObjectModelClass superclass = objectModelImpl.getClass(ref.getName());
                if (superclass == null) {
                    ExternalCacheExtension cache = objectModelImpl.getExtension(
                            ExternalCacheExtension.OBJECTMODEL_EXTENSION, ExternalCacheExtension.class);

                    superclass = cache.getCache(ref, ObjectModelClassImpl.class);
                }

                if (log.isDebugEnabled()) {
                    log.debug("Superclass for " + getQualifiedName() + " : " + superclass.getQualifiedName());
                }

                superclasses.add(superclass);
                superclassesDiscriminators.put(superclass, ref.getDiscriminator());
            }
        }
        return superclasses;
    }

    @Override
    public Collection<ObjectModelClassifier> getInnerClassifiers() {
        return innerClasses;
    }

    /**
     * Returns the discriminator for the given superclass.
     *
     * @return the discriminator for the given superclass as a String if it exists, null otherwise.
     */
    @Override
    public String getDiscriminator(ObjectModelClass superclass) {
        if (superclassesDiscriminators == null) {
            getSuperclasses();
        }
        return superclassesDiscriminators.get(superclass);
    }

    /**
     * Returns all known direct specialized classes for this class.
     *
     * @return a Collection containing all known direct specialized ObjectModelClass for this class.
     * @see ObjectModelClass
     */
    @Override
    public Collection<ObjectModelClass> getSpecialisations() {
        if (specialisations == null) {
            specialisations = new ArrayList<>();
            for (ObjectModelClass o : objectModelImpl.getClasses()) {
                if (o.getSuperclasses().contains(this)) {
                    specialisations.add(o);
                }
            }
        }
        return specialisations;
    }

    /**
     * Returns all known specialized classes for this class for the specified discriminator.
     *
     * @return a Collection containing all known specialized ObjectModelClass
     * for this class for the specified discriminator.
     * @see ObjectModelClass
     */
    @Override
    public Collection<ObjectModelClass> getSpecialisations(String discriminator) {
        List<ObjectModelClass> discriminatedSpecialisations = new ArrayList<>();
        for (ObjectModelClass candidateClass : getSpecialisations()) {
            if (discriminator.equals(candidateClass.getDiscriminator(this))) {
                discriminatedSpecialisations.add(candidateClass);
            }
        }
        return discriminatedSpecialisations;
    }

    /**
     * Returns whether this class is abstract or not.
     *
     * @return a boolean indicating whether this class is abstract or not.
     */
    @Override
    public boolean isAbstract() {
        return modifiers.contains(ObjectModelJavaModifier.ABSTRACT);
    }

    @Override
    public Collection<ObjectModelOperation> getAllOtherOperations(
            boolean distinct) {
        Collection<ObjectModelOperation> result = getAllInterfaceOperations(distinct);
        getAllSuperclassOperations(result);
        return result;
    }

    @Override
    public Collection<ObjectModelOperation> getAllSuperclassOperations(
            boolean distinct) {
        Collection<ObjectModelOperation> result;
        if (distinct) {
            result = new HashSet<>();
        } else {
            result = new LinkedList<>();
        }
        getAllSuperclassOperations(result);
        return result;
    }

    protected Collection<ObjectModelOperation> getAllSuperclassOperations(Collection<ObjectModelOperation> result) {
        for (Object o : getSuperclasses()) {
            ObjectModelClassImpl clazz = (ObjectModelClassImpl) o;
            result.addAll(clazz.getOperations());
            clazz.getAllSuperclassOperations(result);
            clazz.getAllInterfaceOperations(result);
        }
        return result;
    }

    @Override
    public Collection<ObjectModelAttribute> getAllOtherAttributes() {
        Collection<ObjectModelAttribute> result = getAllInterfaceAttributes();
        getAllOtherAttributes(result);
        return result;
    }

    protected Collection<ObjectModelAttribute> getAllOtherAttributes(Collection<ObjectModelAttribute> result) {
        for (Object o : getSuperclasses()) {
            ObjectModelClassImpl clazz = (ObjectModelClassImpl) o;
            result.addAll(clazz.getAttributes());
            clazz.getAllOtherAttributes(result);
        }
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("class ").append(getQualifiedName()).append("<<").append(getStereotypes()).append(">> tagvalue: ").append(getTagValues()).append(" ");
        result.append("extends ");
        for (Iterator<?> i = getSuperclasses().iterator(); i.hasNext(); ) {
            result.append(((ObjectModelClassifier) i.next()).getName());
            if (i.hasNext()) {
                result.append(", ");
            }
        }
        result.append("implements ");
        for (Iterator<?> i = getInterfaces().iterator(); i.hasNext(); ) {
            result.append(((ObjectModelClassifier) i.next()).getName());
            if (i.hasNext()) {
                result.append(", ");
            }
        }
        return result.toString();
    }


    /**
     * This method takes two ObjectModelClassImpl and merges both of them in the
     * first one
     *
     * @param source the instance of ObjectModelClassImpl tu be used for merging
     */
    protected void mergeFrom(ObjectModelClassImpl source) {
        super.mergeFrom(source);

        source.getAttributes().stream().map(objectModelAttribute -> (ObjectModelAttributeImpl) objectModelAttribute).forEach(this::addAttribute);
        source.superclassesRefs.forEach(superclassRef -> {
            if (!contains(superclassesRefs, superclassRef)) {
                superclassesRefs.add(superclassRef);
            }
            superclasses = null; // On force ainsi à regénérer l'objet
        });
    }
}
