/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.tagvalue.ObjectModelTagValuesStore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementation class for the root node abstraction of object model trees.
 * This an entry point for browsing a model tree. This object offers as well
 * several facilities for a direct access to some of the object model elements.
 * In this concrete class, the tree is build by parsing an object model xml
 * description using lutinxml XMLObjectParser.
 * <p>
 * Created: 14 janv. 2004
 *
 * @author Cédric Pineau - pineau@codelutin.com
 */
public class ObjectModelImpl implements ObjectModel {

    /**
     * logger
     */
    private static final Logger log = LogManager.getLogger(ObjectModelImpl.class);
    private final ObjectModelTagValuesStore tagValuesStore;
    /**
     * Used to add others specific object to the model
     * The key defined must be unique to get the significative extension associated to
     */
    private final Map<String, Object> extensions = new HashMap<>();
    protected String name;
    protected String version;
    protected Map<String, ObjectModelClass> classes = new HashMap<>();
    protected Map<String, ObjectModelPackage> packages = new HashMap<>();
    protected Map<String, ObjectModelInterface> interfaces = new HashMap<>();
    protected Map<String, ObjectModelClassifier> classifiers = new HashMap<>();
    protected Map<String, ObjectModelEnumeration> enumerations = new HashMap<>();
    protected List<String> comments = new ArrayList<>();
    protected Map<String, String> tagValues = new HashMap<>();

    public ObjectModelImpl() {
        tagValuesStore = new ObjectModelTagValuesStore(this);
    }

    @Override
    public ObjectModelTagValuesStore getTagValuesStore() {
        return tagValuesStore;
    }

    @Override
    public String getModelType() {
        return ModelHelper.ModelType.OBJECT.getAlias();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String getVersion() {
        return version;
    }

    public void addPackage(ObjectModelPackageImpl aPackage) {
        aPackage.postInit();
        aPackage.setObjectModelImpl(this);

        if (!aPackage.isExtern()) {
            ObjectModelPackageImpl initialElement = (ObjectModelPackageImpl)
                    packages.get(aPackage.getName());
            if (initialElement == null) {
                packages.put(aPackage.getName(), aPackage);
            } else {

                // La package existe déjà. On va prendre tout ce que contient la
                // nouvelle package et l'ajouter à la précédente si nécessaire.
                initialElement.mergeFrom(aPackage);
            }
        }
    }

    public void addClass(ObjectModelClassImpl clazz) {
        clazz.postInit();
        clazz.setObjectModelImpl(this);

        if (!clazz.isExtern()) {
            ObjectModelElementImpl initialElement = (ObjectModelElementImpl)
                    classes.get(clazz.getQualifiedName());
            if (initialElement == null) {
                classes.put(clazz.getQualifiedName(), clazz);
                classifiers.put(clazz.getQualifiedName(), clazz);
            } else {
                if (!(initialElement instanceof ObjectModelClassImpl)) {
                    throw new IllegalArgumentException(
                            String.format("\"%s\" is incompatible with already defined element \"%s\"", clazz, initialElement));
                }
                ObjectModelClassImpl initialClazz =
                        (ObjectModelClassImpl) initialElement;

                // La classe existe déjà. On va prendre tout ce que contient la
                // nouvelle classe et l'ajouter à la précédente si nécessaire.
                initialClazz.mergeFrom(clazz);
            }
        }
    }

    public void addAssociationClass(ObjectModelAssociationClassImpl clazz) {
        clazz.postInit();
        clazz.setObjectModelImpl(this);

        if (!clazz.isExtern()) {
            ObjectModelElementImpl initialElement = (ObjectModelElementImpl) classes.get(clazz.getQualifiedName());
            if (initialElement == null) {
                classes.put(clazz.getQualifiedName(), clazz);
                classifiers.put(clazz.getQualifiedName(), clazz);
            } else {
                if (!(initialElement instanceof ObjectModelAssociationClassImpl)) {
                    throw new IllegalArgumentException(
                            String.format("\"%s\" is incompatible with already defined element \"%s\"", clazz, initialElement)
                    );
                }
                ((ObjectModelAssociationClassImpl) initialElement).mergeFrom(clazz);
            }
        }
    }

    public void addComment(String comment) {
        comments.add(comment);
    }

    /**
     * Returns the name of this model.
     *
     * @return the name of this model.
     */
    @Override
    public String getName() {
        return name;
    }


    @Override
    public Collection<ObjectModelPackage> getPackages() {
        return packages.values();
    }

    @Override
    public ObjectModelPackage getPackage(String packageName) {
        return packages.get(packageName);
    }

    @Override
    public ObjectModelPackage getPackage(ObjectModelClassifier classifier) {
        return getPackage(classifier.getPackageName());
    }

    @Override
    public boolean hasPackage(String packageName) {
        return packages.containsKey(packageName);
    }

    /**
     * Returns all classifiers defined in this model.
     *
     * @return a Collection containing all ObjectModelClassifier for this model.
     * @see ObjectModelClassifier
     */
    @Override
    public Collection<ObjectModelClassifier> getClassifiers() {
        return classifiers.values();
    }

    /**
     * Returns the classifier corresponding to the given qualified name, or null
     * if the model contains no classifier for this qualified name.
     *
     * @param qualifiedClassifierName -
     *                                the qualified name of the classifier to retrieve.
     * @return the ObjectModelClassifier of the found classifier, or null if the
     * model contains no classifier for this qualified name.
     */
    @Override
    public ObjectModelClassifier getClassifier(String qualifiedClassifierName) {
        return qualifiedClassifierName == null ? null
                : classifiers.get(qualifiedClassifierName);
    }

    /**
     * Returns all classes defined in this model.
     *
     * @return a Collection containing all ObjectModelClass for this model.
     * @see ObjectModelClass
     */
    @Override
    public Collection<ObjectModelClass> getClasses() {
        return classes.values();
    }

    /**
     * Returns the class corresponding to the given qualified name, or null if
     * the model contains no class for this qualified name.
     *
     * @param qualifiedClassName -
     *                           the qualified name of the class to retrieve.
     * @return the ObjectModelClass of the found class, or null if the model
     * contains no class for this qualified name.
     */
    @Override
    public ObjectModelClass getClass(String qualifiedClassName) {
        if (qualifiedClassName == null) {
            return null;
        }
        if (!hasClass(qualifiedClassName)) {
            if (log.isDebugEnabled()) {
                log.debug("Class " + qualifiedClassName + " not found in model");
            }
            return null;
        }
        return classes.get(qualifiedClassName);
    }

    @Override
    public boolean hasClass(String qualifiedClassName) {
        return classes.containsKey(qualifiedClassName);
    }

    public void addInterface(ObjectModelInterfaceImpl interfacez) {
        interfacez.postInit();
        interfacez.setObjectModelImpl(this);

        if (!interfacez.isExtern()) {
            ObjectModelElementImpl initialElement = (ObjectModelElementImpl)
                    interfaces.get(interfacez.getQualifiedName());
            if (initialElement == null) {
                interfaces.put(interfacez.getQualifiedName(), interfacez);
                classifiers.put(interfacez.getQualifiedName(), interfacez);
            } else {
                if (!(initialElement instanceof ObjectModelInterfaceImpl)) {
                    throw new IllegalArgumentException(
                            "\"" + interfacez + "\" is incompatible with " +
                                    "already defined element \"" + initialElement +
                                    "\"");
                }
                ((ObjectModelInterfaceImpl) initialElement).mergeFrom(interfacez);
            }
        }
    }

    /**
     * Returns the interface corresponding to the given qualified name, or null
     * if the model contains no interface for this qualified name.
     *
     * @param qualifiedInterfaceName -
     *                               the qualified name of the interface to retrieve.
     * @return the ObjectModelInterface of the found interface, or null if the
     * model contains no interface for this qualified name.
     */
    @Override
    public ObjectModelInterface getInterface(String qualifiedInterfaceName) {
        ObjectModelInterface result = interfaces.get(qualifiedInterfaceName);
        if (result == null) {
            if (log.isDebugEnabled()) {
                log.debug("Interface " + qualifiedInterfaceName +
                                  " not found in model");
            }
        }
        return result;
    }

    /**
     * Returns all interfaces defined in this model.
     *
     * @return a Collection containing all ObjectModelInterface for this model.
     * @see ObjectModelInterface
     */
    @Override
    public Collection<ObjectModelInterface> getInterfaces() {
        return interfaces.values();
    }

    public void addEnumeration(ObjectModelEnumerationImpl enumeration) {
        enumeration.postInit();
        enumeration.setObjectModelImpl(this);
        classifiers.put(enumeration.getQualifiedName(), enumeration);
        enumerations.put(enumeration.getQualifiedName(), enumeration);
    }

    @Override
    public Collection<ObjectModelEnumeration> getEnumerations() {
        return enumerations.values();
    }

    @Override
    public ObjectModelEnumeration getEnumeration(String qualifiedEnumerationName) {
        ObjectModelEnumeration result =
                enumerations.get(qualifiedEnumerationName);
        if (result == null) {
            if (log.isDebugEnabled()) {
                log.debug("Enumeration " + qualifiedEnumerationName +
                                  " not found in model");
            }
        }
        return result;
    }

    /**
     * Returns all comments not lied to a particular model element
     *
     * @return a List containing all comments for this model as Strings.
     */
    @Override
    public List<String> getComments() {
        return comments;
    }

    @Override
    public Set<String> getStereotypes() {
        Set<String> result = new LinkedHashSet<>();
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            if ("true".equals(entry.getValue())) {
                result.add(entry.getKey());
            }
        }
        return result;
    }

    @Override
    public boolean hasStereotype(String stereotypeName) {
        return tagValues.containsKey(stereotypeName);
    }

    @Override
    public void addStereotype(String stereotype) {
        tagValues.put(stereotype, "true");
    }

    @Override
    public void removeStereotype(String stereotype) {
        tagValues.remove(stereotype);
    }

    public void removePackage(String packageName) {

        for (ObjectModelClass objectModelClass : new LinkedHashSet<>(classes.values())) {
            if (objectModelClass.getQualifiedName().startsWith(packageName + ".")) {
                classes.remove(objectModelClass.getQualifiedName());
            }
        }
        for (ObjectModelInterface objectModelInterface : new LinkedHashSet<>(interfaces.values())) {
            if (objectModelInterface.getQualifiedName().startsWith(packageName + ".")) {
                classes.remove(objectModelInterface.getQualifiedName());
            }
        }
        for (ObjectModelEnumeration objectModelEnumeration : new LinkedHashSet<>(enumerations.values())) {
            if (objectModelEnumeration.getQualifiedName().startsWith(packageName + ".")) {
                classes.remove(objectModelEnumeration.getQualifiedName());
            }
        }
        packages.remove(packageName);

    }

    public void removeClass(String className) {
        classes.remove(className);
    }

    public void removeInterface(String interfaceName) {
        interfaces.remove(interfaceName);
    }

    public void removeEnumeration(String enumerationName) {
        enumerations.remove(enumerationName);
    }


    public ObjectModelImplTagValue addTagValue(ObjectModelImplTagValue tagValue) {
        if (tagValue == null) {
            return new ObjectModelImplTagValue();
        }
        tagValues.put(tagValue.getName(), tagValue.getValue());
        return tagValue;
    }

    @Override
    public Map<String, String> getTagValues() {
        return tagValues;
    }

    @Override
    public String getTagValue(String tagValue) {
        return tagValue == null ? null : tagValues.get(tagValue);
    }

    @Override
    public void addTagValue(String tagValue, String value) {
        String oldValue = getTagValue(tagValue);
        if (StringUtils.isNotEmpty(oldValue) && !oldValue.equals(value)) {
            if (log.isWarnEnabled()) {
                log.warn("Replace tagValue '" + tagValue + "' (old:" +
                                 oldValue + ", new: " + value + ")");
            }
        }
        tagValues.put(tagValue, value);
    }

    @Override
    public boolean hasTagValue(String tagValue) {
        return tagValues.containsKey(tagValue);
    }

    @Override
    public void removeTagValue(String tagvalue) {
        tagValues.remove(tagvalue);
    }

    /**
     * Get the extension associated to the reference (unique). Create it if not exist.
     *
     * @param <O>            object type returned
     * @param reference      unique corresponding to the extension to get
     * @param extensionClass class of the extension
     * @return the object value for the extension
     * @throws ClassCastException when extensionClass is not valid
     * @throws RuntimeException   when instantiation problem to create new extension
     */
    @Override
    @SuppressWarnings("unchecked")
    public <O> O getExtension(String reference, Class<O> extensionClass) throws RuntimeException {
        if (reference == null) {
            throw new NullPointerException("reference parameter can not be null in method ObjectModelImpl#getExtension");
        }
        if (extensionClass == null) {
            throw new NullPointerException("extensionClass parameter can not be null in method ObjectModelImpl#getExtension.");
        }
        Object object = extensions.get(reference);
        O result;
        if (object != null && !extensionClass.isAssignableFrom(object.getClass())) {
            throw new ClassCastException("Invalid cast for " + extensionClass.getName());
        }
        if (object == null) {
            try {
                result = extensionClass.getConstructor().newInstance();
            } catch (Exception eee) { // IllegalAccessException and InstantiationException
                throw new RuntimeException("Unable to create new extension '" + extensionClass.getName() +
                                                   "' for '" + reference + "'", eee);
            }
            extensions.put(reference, result);
        } else {
            result = (O) object;
        }
        return result;
    }
}
