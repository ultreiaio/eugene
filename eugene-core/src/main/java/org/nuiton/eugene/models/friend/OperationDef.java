package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.xml.ObjectModelOperationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelParameterImpl;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class OperationDef extends ElementDef {
    /** Logger. */
    private static final Logger log = LogManager.getLogger(OperationDef.class);

    private final String returnType;
    private String fullyQualifiedReturnType;
    private final List<ParameterDef> parameters = new LinkedList<>();

    public static OperationDef of(ObjectModelOperation anOperation, String defaultPackage) {
        OperationDef result = new OperationDef(anOperation.getName(), getRelativeType(anOperation.getReturnType(), defaultPackage));
        result.loadStereotypesAndTagValues(anOperation);
        for (ObjectModelParameter objectModelParameter : anOperation.getParameters()) {
            result.add(ParameterDef.of(objectModelParameter, defaultPackage));
        }
        return result;
    }

    public static OperationDef of(String lineDef) {
        Pair<String, String> pair = splitTagValues(lineDef, lineDef.indexOf(')'));
        lineDef = pair.getLeft();
        String tagValues = pair.getRight();

        int parameterFirstIndex = lineDef.indexOf('(');
        int parameterLastIndex = lineDef.indexOf(')');
        String type = lineDef.substring(parameterLastIndex + 1).trim();
        String parameters = lineDef.substring(parameterFirstIndex, parameterLastIndex + 1).replace("(", "").replace(")", "");
        lineDef = lineDef.substring(0, parameterFirstIndex);

        OperationDef result = new OperationDef(lineDef.trim(), type);

        if (tagValues != null) {
            result.loadStereotypesAndTagValues(tagValues);
        }

        if (!parameters.isEmpty()) {
            for (String parameter : parameters.split(",")) {
                ParameterDef parameterDef = ParameterDef.of(parameter.trim());
                result.add(parameterDef);
            }
        }
        return result;
    }

    private OperationDef(String name, String returnType) {
        super(name);
        this.returnType = returnType;
    }

    public String getReturnType() {
        return returnType;
    }

    public String getFullyQualifiedReturnType() {
        return fullyQualifiedReturnType;
    }

    public List<ParameterDef> getParameters() {
        return parameters;
    }

    public void add(ParameterDef parameterDef) {
        log.debug(String.format("Add parameter: %s on %s", parameterDef, getName()));
        parameters.add(parameterDef);
    }

    @Override
    public String toString() {
        return getName()
                + (parameters.isEmpty() ? "()" : "(" + Joiner.on(",").join(parameters) + ")")
                + ":" + returnType;
    }

    @Override
    public void write(BufferedWriter writer) throws IOException {
        writer.newLine();
        writer.append(getName());
        if (parameters.isEmpty()) {
            writer.append("()");
        } else {
            writer.append("(");
            write(writer, parameters, ", ");
            writer.append(")");
        }
        if (!returnType.isEmpty()) {
            writer.append(" ").append(returnType);
        }
        writeStereotypesAndTagValues(writer);
    }

    void resolveTypes(Map<String, String> classesMapping) {
        fullyQualifiedReturnType = resolveType(getReturnType(), classesMapping);
        for (ParameterDef parameterDef : getParameters()) {
            parameterDef.resolveType(classesMapping);
        }
    }

    ObjectModelOperationImpl toObjectModel() {
        ObjectModelOperationImpl result = new ObjectModelOperationImpl();
        result.setName(getName());
        flushStereotypesAndTagValues(result);
        ObjectModelParameterImpl returnParameter = new ObjectModelParameterImpl();
        returnParameter.setType(getFullyQualifiedReturnType());
        result.setReturnParameter(returnParameter);
        for (ParameterDef parameter : parameters) {
            result.addParameter(parameter.toObjectModel());
        }
        return result;
    }
}
