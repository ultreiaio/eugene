/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.xml;

import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelDependency;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * ObjectModelClassifierImpl.
 *
 * @author chatellier
 * @author cedric
 */
public abstract class ObjectModelClassifierImpl extends ObjectModelElementImpl implements ObjectModelClassifier {

    protected boolean extern;

    protected String qualifiedName;

    protected String packageName;

    protected List<ObjectModelInterface> interfaces;

    protected List<ObjectModelImplRef> interfacesRefs = new ArrayList<>();

    protected List<ObjectModelOperation> operations = new ArrayList<>();

    protected Map<String, ObjectModelAttribute> attributes = new LinkedHashMap<>();

    protected List<ObjectModelDependency> dependencies = new ArrayList<>();

    protected String type;

    protected boolean inner;

    @Override
    public String toString() {
        return "" + getQualifiedName() + " implements " + getInterfaces();
    }

    @Override
    public void postInit() {
        super.postInit();
        qualifiedName = packageName + "." + name;
    }

    public void setExtern(boolean extern) {
        this.extern = extern;
    }

    public void setPackage(String packageName) {
        this.packageName = packageName;
    }

    public void setInner(boolean inner) {
        this.inner = inner;
    }

    @Override
    public boolean isInner() {
        return inner;
    }

    public void addInterface(ObjectModelImplRef ref) {
        interfacesRefs.add(ref);
        interfaces = null;
    }

    public void addOperation(ObjectModelOperationImpl operation) {
        operation.postInit();
        operation.setDeclaringElement(this);
        operations.add(operation);
    }

    public void addAttribute(ObjectModelAttributeImpl attribute) {
        attribute.postInit();
        attribute.setDeclaringElement(this);

        String attributeName = attribute.getName();
        ObjectModelAttributeImpl existingAttribute = (ObjectModelAttributeImpl) attributes.get(attributeName);
        if (existingAttribute == null) {
            attributes.put(attributeName, attribute);
        } else {
            // merge it, instead of adding it (it will replace the
            existingAttribute.mergeFrom(attribute);
        }
    }

    public void addDependency(ObjectModelDependencyImpl dependency) {
        dependency.postInit();
        dependency.setClient(this);
        dependencies.add(dependency);
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isExtern() {
        return extern;
    }

    @Override
    public String getPackageName() {
        return packageName;
    }

    @Override
    public String getQualifiedName() {
        return qualifiedName;
    }


    //FIXME: If interface reference was not a FQN, then we only got his simple name
    //FIXME: then in extensionCache we will compute a bad FQN : simpleName.simpleName
    @Override
    public Collection<ObjectModelInterface> getInterfaces() {
        if (interfaces == null) {
            interfaces = new ArrayList<>();
            for (ObjectModelImplRef ref : interfacesRefs) {

                ObjectModelInterfaceImpl interfacez = (ObjectModelInterfaceImpl) objectModelImpl.getInterface(ref.getName());

                if (interfacez == null) { // Interface not exist in model

                    ExternalCacheExtension cache = objectModelImpl.getExtension(
                            ExternalCacheExtension.OBJECTMODEL_EXTENSION,
                            ExternalCacheExtension.class);

                    // get external interface from cache (or create it)
                    interfacez = cache.getCache(ref, ObjectModelInterfaceImpl.class);
                }
                interfaces.add(interfacez);
            }
        }
        return interfaces;
    }

    @Override
    public Collection<ObjectModelOperation> getOperations(String name) {
        List<ObjectModelOperation> result = new ArrayList<>();
        for (ObjectModelOperation op : getOperations()) {
            if (name.equals(op.getName())) {
                result.add(op);
            }
        }
        return result;
    }

    @Override
    public Collection<ObjectModelOperation> getOperations() {
        return operations;
    }

    @Override
    public Collection<ObjectModelOperation> getAllOtherOperations(boolean distinct) {
        return getAllInterfaceOperations(distinct);
    }

    @Override
    public Collection<ObjectModelOperation> getAllInterfaceOperations(boolean distinct) {
        Collection<ObjectModelOperation> result;
        if (distinct) {
            result = new HashSet<>();
        } else {
            result = new LinkedList<>();
        }
        getAllInterfaceOperations(result);
        return result;
    }

    protected Collection<ObjectModelOperation> getAllInterfaceOperations(Collection<ObjectModelOperation> result) {
        for (ObjectModelClassifier interfacez : getInterfaces()) {
            result.addAll(interfacez.getOperations());
            ((ObjectModelClassifierImpl) interfacez).getAllInterfaceOperations(result);
        }
        return result;
    }

    @Override
    public Collection<ObjectModelAttribute> getAttributes() {
        return attributes.values();
    }

    @Override
    public ObjectModelAttribute getAttribute(String attributeName) {
        return attributeName == null ? null : attributes.get(attributeName);
    }

    @Override
    public Collection<ObjectModelAttribute> getAllInterfaceAttributes() {
        Collection<ObjectModelAttribute> result = new LinkedList<>();
        getAllInterfaceAttributes(result);
        return result;
    }

    @Override
    public Collection<ObjectModelAttribute> getAllOtherAttributes() {
        return getAllInterfaceAttributes();
    }

    @Override
    public Collection<ObjectModelDependency> getDependencies() {
        return dependencies;
    }

    @Override
    public ObjectModelDependency getDependency(String name) {
        if (name.isEmpty()) {
            return null;
        }
        for (ObjectModelDependency dependency : dependencies) {
            if (dependency.getName().equalsIgnoreCase(name)) {
                return dependency;
            }
        }
        return null;
    }

    @Override
    public final boolean isClass() {
        return this instanceof ObjectModelClass;
    }

    @Override
    public final boolean isInterface() {
        return this instanceof ObjectModelInterface;
    }

    @Override
    public final boolean isEnum() {
        return this instanceof ObjectModelEnumeration;
    }

    protected Collection<ObjectModelAttribute> getAllInterfaceAttributes(Collection<ObjectModelAttribute> result) {
        for (Object o : getInterfaces()) {
            ObjectModelClassifierImpl clazz = (ObjectModelClassifierImpl) o;
            result.addAll(clazz.getAttributes());
            clazz.getAllInterfaceAttributes(result);
        }
        return result;
    }

    protected void mergeFrom(ObjectModelClassifierImpl source) {
        Iterator<?> it;

        // On n'utilise pas les setXXX puisque les post-init sont censés être déjà faits...

        String description = "";
        String sourceDoc = "";
        if (documentation != null) {
            description += getDescription();
            if (documentation.contains("--")) {
                sourceDoc += getSourceDocumentation();
            }
        }
        if (source.documentation != null) {
            if (!description.equals("")) {
                description += " - ";
            }
            description += source.getDescription();
            if (source.documentation.contains("--")) {
                if (!sourceDoc.equals("")) {
                    sourceDoc += " - ";
                }
                sourceDoc += source.getSourceDocumentation();
            }
        }
        if (!description.equals("") || !sourceDoc.equals("")) {
            documentation = description + "--" + sourceDoc;
        }
        // System.out.println("Doc after : " + documentation);

        for (it = source.interfacesRefs.iterator(); it.hasNext(); ) {
            ObjectModelImplRef interfaceRef = (ObjectModelImplRef) it.next();
            if (!contains(interfacesRefs, interfaceRef)) {
                interfacesRefs.add(interfaceRef);
            }
            interfaces = null; // On force ainsi à
            // regénérer l'objet
        }
        for (it = source.getOperations().iterator(); it.hasNext(); ) {
            ObjectModelOperationImpl operation = (ObjectModelOperationImpl) it.next();
            if (!contains(getOperations(), operation)) {
                operations.add(operation);
            }
        }
        for (it = source.getDependencies().iterator(); it.hasNext(); ) {
            ObjectModelDependencyImpl dependency = (ObjectModelDependencyImpl) it.next();
            if (!contains(getDependencies(), dependency)) {
                dependencies.add(dependency);
            }
        }
        for (it = source.getComments().iterator(); it.hasNext(); ) {
            String comment = (String) it.next();
            if (!comments.contains(comment)) {
                comments.add(comment);
            }
        }
        for (it = source.getStereotypes().iterator(); it.hasNext(); ) {
            String stereotype = (String) it.next();
            if (!getStereotypes().contains(stereotype)) {
                addStereotype(stereotype);
            }
        }

        for (it = source.getTagValues().keySet().iterator(); it.hasNext(); ) {
            String tagName = (String) it.next();
            if (!getTagValues().containsKey(tagName)) {
                tagValues.put(
                        tagName,
                        source.getTagValue(tagName)
                );
            }
        }
    }

    protected boolean contains(Collection<?> coll, ObjectModelOperationImpl toFind) {
        // Le equals(...) de ObjectModelOperationImpl convient
        return coll.contains(toFind);
    }

    protected boolean contains(Collection<ObjectModelAttribute> coll, ObjectModelAttributeImpl toFind) {
        for (ObjectModelAttribute attribute : coll) {
            if (attribute.getName().equals(toFind.getName())) {
                // Seul le nom de l'attribut compte
                return true;
            }
        }
        return false;
    }

    protected boolean contains(Collection<?> coll, ObjectModelImplRef toFind) {
        // Le equals(...) de ObjectModelImplRef convient
        return coll.contains(toFind);
    }

}
