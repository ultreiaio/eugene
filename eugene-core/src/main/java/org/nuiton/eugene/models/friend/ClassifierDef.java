package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.object.xml.ObjectModelClassifierImpl;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ClassifierDef<D extends ClassifierDef<?>> extends ElementDef implements Comparable<D> {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(ClassifierDef.class);

    private final String fullyQualifiedName;
    private final List<OperationDef> operations = new LinkedList<>();

    ClassifierDef(String name, String defaultPackage) {
        super(name);
        if (name.startsWith("!")) {
            fullyQualifiedName = name.substring(1);
        } else
            fullyQualifiedName = defaultPackage + name;
    }

    String getSimpleName() {
        if (getName().contains(".")) {
            return getName().substring(getName().lastIndexOf(".") + 1);
        } else {
            return getName();
        }
    }

    public List<OperationDef> getOperations() {
        return operations;
    }

    @Override
    public int compareTo(D o) {
        return getFullyQualifiedName().compareTo(o.getFullyQualifiedName());
    }

    String getFullyQualifiedName() {
        return fullyQualifiedName;
    }

    void add(OperationDef operationDef) {
        log.debug(String.format("Add operation: %s on %s", operationDef, getName()));
        operations.add(operationDef);
    }

    void resolveTypes(Map<String, String> classesMapping) {
        for (OperationDef operation : operations) {
            operation.resolveTypes(classesMapping);
        }
    }

    void toObjectModel(ObjectModelClassifierImpl classifier) {
        String fullyQualifiedName = getFullyQualifiedName();
        int lastIndexOf = fullyQualifiedName.lastIndexOf('.');
        classifier.setName(fullyQualifiedName.substring(lastIndexOf + 1));
        classifier.setPackage(fullyQualifiedName.substring(0, lastIndexOf));
        flushStereotypesAndTagValues(classifier);
        for (OperationDef operationDef : getOperations()) {
            classifier.addOperation(operationDef.toObjectModel());
        }
    }
}
