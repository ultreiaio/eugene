package org.nuiton.eugene.models.extension.io;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.models.extension.tagvalue.InvalidStereotypeSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.InvalidTagValueSyntaxException;
import org.nuiton.eugene.models.extension.tagvalue.MismatchTagValueTargetException;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueNotFoundException;
import org.nuiton.eugene.models.extension.tagvalue.provider.AggregateTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAssociationClass;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelAssociationClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelClassifierImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.tagvalue.TagValuesStore;

import java.io.File;
import java.io.IOException;


/**
 * Created on 09/09/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ModelExtensionReader implements ModelExtensionFileParserCallback {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(ModelExtensionReader.class);

    protected final boolean verbose;
    protected final boolean strictLoading;
    protected final ObjectModel model;
    protected final TagValueMetadatasProvider tagValueMetadatasProvider;

    protected final TagValuesStore tagValuesStore;

    public ModelExtensionReader(boolean verbose, boolean strictLoading, ObjectModel model) {
        this.verbose = verbose;
        this.strictLoading = strictLoading;
        this.model = model;
        this.tagValueMetadatasProvider = new AggregateTagValueMetadatasProvider((ClassLoader) null);
        this.tagValuesStore = model.getTagValuesStore().getStore();
    }

    public void read(File modelExtension) throws IOException, InvalidTagValueSyntaxException, InvalidStereotypeSyntaxException {

        ModelExtensionFileParser parser = ModelExtensionFileParser.newParser(strictLoading, modelExtension);
        parser.parse(modelExtension, this);

    }

    @Override
    public boolean onModelTagValueFound(String tag, String value) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(tag, ObjectModel.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid model tag value: the tag-value '%s' is unknown.", tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid model tag value: this tag-value '%s' can not be apply on the model.", tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if tagValue is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(tag).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated tagValue usage: %s", value));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe tags
                return false;
            }
        }
        ObjectModelImpl modelImpl = (ObjectModelImpl) model;
        if (tag.equals(EugeneCoreTagValues.Store.version.getName())) {
            // push directly the version in the model version property
            modelImpl.setVersion(value);
        }
        if (verbose) {
            log.info(String.format("model tag value imported %s → %s", tag, value));
        }
        modelImpl.addTagValue(tag, value);
        tagValuesStore.setModelTagValue(model.getName(), tag, value);
        return safe;
    }

    @Override
    public boolean onModelStereotypeFound(String stereotype) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(stereotype, ObjectModel.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid model stereotype [%s] : the stereotype '%s' is unknown.", stereotype, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid model stereotype [%s] : this stereotype '%s' can not be apply on model.", stereotype, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if stereotype is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(stereotype).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated model stereotype usage: %s", stereotype));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe stereotypes
                return false;
            }
        }
        model.addTagValue(stereotype, "true");
        tagValuesStore.setModelTagValue(model.getName(), stereotype, "true");
        if (verbose) {
            log.info(String.format("stereotype imported %s → model.", stereotype));
        }
        return safe;
    }

    @Override
    public boolean onPackageTagValueFound(String packageName, String tag, String value) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(tag, ObjectModelPackage.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid tag value on package [%s]: the tag-value '%s' is unknown.", packageName, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid tag value on package [%s]: this tag-value '%s' can not be apply on the model.", packageName, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if tagValue is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(tag).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated tagValue usage on package [%s]: %s", packageName, value));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe tags
                return false;
            }
        }
        ObjectModelPackage omc = model.getPackage(packageName);
        if (verbose) {
            log.info(String.format("imported tag value on package [%s] → %s = %s", packageName, tag, value));
        }
        omc.addTagValue(tag, value);
        tagValuesStore.setPackageTagValue(packageName, tag, value);
        return true;
    }

    @Override
    public boolean onPackageStereotypeFound(String packageName, String stereotype) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(stereotype, ObjectModelPackage.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid model stereotype on package [%s]: the stereotype '%s' is unknown.", packageName, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid model stereotype on package [%s]: this stereotype '%s' can not be apply on package.", packageName, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if tagValue is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(stereotype).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated stereotype usage on package [%s]: %s", packageName, stereotype));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe stereotypes
                return false;
            }
        }
        ObjectModelPackage omc = model.getPackage(packageName);
        if (omc == null) {
            // package not found
            return false;
        }
        omc.addTagValue(stereotype, "true");
        tagValuesStore.setPackageTagValue(packageName, stereotype, "true");
        if (verbose) {
            log.info(String.format("imported stereotype on package [%s] → %s", packageName, stereotype));
        }
        return true;
    }

    @Override
    public boolean onClassTagValueFound(String className, String tag, String value) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(tag, ObjectModelClass.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid tag value on class [%s]: this tag-value '%s' is unknown.", className, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid tag value on class [%s]: this tag-value '%s' can not be apply on class scope.", className, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if tagValue is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(tag).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated tagValue usage on class [%s] : %s", className, value));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe tags
                return false;
            }
        }
        ObjectModelClassifier omc = getClassifier(className);
        if (omc == null) {
            // classifier not found
            return false;
        }
        omc.addTagValue(tag, value);
        tagValuesStore.setClassifierTagValue(className, tag, value);
        if (verbose) {
            log.info(String.format("tag value imported on class [%s] → %s = %s", className, tag, value));
        }
        return true;
    }

    @Override
    public boolean onClassStereotypeFound(String className, String stereotype) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(stereotype, ObjectModelClassifier.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid model stereotype on class [%s]: the stereotype '%s' is unknown.", className, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid model stereotype on class [%s]: this stereotype '%s' can not be apply on class scope.", className, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if stereotype is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(stereotype).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated stereotype usage on class [%s]: %s", className, stereotype));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe stereotypes
                return false;
            }
        }
        ObjectModelClassifier omc = getClassifier(className);
        if (omc == null) {
            // classifier not found
            return false;
        }
        omc.addTagValue(stereotype, "true");
        tagValuesStore.setClassifierTagValue(className, stereotype, "true");
        if (verbose) {
            log.info(String.format("stereotype imported on class [%s] → %s", className, stereotype));
        }
        return true;
    }

    @Override
    public boolean onAttributeTagValueFound(String className, String attributeName, String tag, String value) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(tag, ObjectModelAttribute.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid tag value on attribute [%s#%s]: this tag-value '%s' is unknown.", className, attributeName, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid tag value on attribute [%s#%s]: this tag-value '%s' can not be apply on attribute scope.", className, attributeName, tag);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if tagValue is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(tag).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated tagValue usage on attribute [%s#%s] : %s", className, attributeName, value));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe tags
                return false;
            }
        }
        ObjectModelClassifier omc = getClassifier(className);
        if (omc == null) {
            // classifier not found
            return false;
        }
        ObjectModelAttribute attribute = getAttribute(omc, attributeName);
        if (attribute == null) {
            attribute = getInheritedAttribute(omc, attributeName);
            if (attribute == null) {
                // attribute not found
                return false;
            }
        } else {
            // on direct attribute, can affect value, otherwise do not do it!
            attribute.addTagValue(tag, value);
        }
        tagValuesStore.setAttributeTagValue(className, attributeName, tag, value);
        if (verbose) {
            log.info(String.format("tag value imported on attribute [%s#%s] → %s = %s", className, attributeName, tag, value));
        }
        return true;
    }

    @Override
    public boolean onAttributeStereotypeFound(String className, String attributeName, String stereotype) {
        boolean safe = false;
        try {
            tagValueMetadatasProvider.validate(stereotype, ObjectModelAttribute.class);
            safe = true;
        } catch (TagValueNotFoundException e) {
            String message = String.format("Invalid model stereotype on attribute [%s#%s]: the stereotype '%s' is unknown.", className, attributeName, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        } catch (MismatchTagValueTargetException e) {
            String message = String.format("Invalid model stereotype on attribute [%s#%s]: this stereotype '%s' can not be apply on attribute scope.", className, attributeName, stereotype);
            if (strictLoading) {
                log.error(message);
            } else {
                log.warn(message);
            }
        }
        if (safe) {
            // test if stereotype is deprecated
            TagValueMetadata tagValueMetadata = tagValueMetadatasProvider.getTagValue(stereotype).toJavaUtil().orElseThrow();
            boolean deprecated = tagValueMetadata.isDeprecated();
            if (deprecated) {
                log.warn(String.format("Deprecated stereotype usage on attribute [%s]: %s", className, stereotype));
            }
        } else {
            if (strictLoading) {
                // in strict loading do not accept unsafe stereotypes
                return false;
            }
        }
        ObjectModelClassifier omc = getClassifier(className);
        if (omc == null) {
            // classifier not found
            return false;
        }
        ObjectModelAttribute attribute = getAttribute(omc, attributeName);
        if (attribute == null) {
            // attribute not found
            return false;
        }
        attribute.addStereotype(stereotype);
        tagValuesStore.setAttributeTagValue(className, attributeName, stereotype, "true");
        if (verbose) {
            log.info(String.format("stereotype imported on attribute [%s#%s] → %s", className, attributeName, stereotype));
        }
        return true;
    }

    protected ObjectModelClassifier getClassifier(String fqn) {
        ObjectModelClassifier omc = model.getClassifier(fqn);
        if (omc == null) {
            if (log.isWarnEnabled()) {
                log.warn("Could not find classifier for " + fqn);
            }
            return null;
        }

        //todo tchemit 2010-11-25 : what does it mean ? every thing extends ObjectModelClassifierImpl
        if (!(omc instanceof ObjectModelClassifierImpl)) {
            // TODO il faudra avoir des methodes d'acces en Set sur l'interface pour eviter ce message
            if (log.isWarnEnabled()) {
                log.warn("Can't add properties to model, " +
                                 "it's not an " +
                                 "ObjectModelClassifierImpl : " +
                                 omc.getQualifiedName());
            }
            return null;
        }
        return omc;
    }

    /**
     * Retrieve an attribute from a {@code clazz} with its {@code name}.
     * This method manage the association class case to explore participants
     * attributes if needed.
     *
     * @param clazz where the attribute need to be find
     * @param name  attribute name to find
     * @return the attribute found or null
     */
    protected ObjectModelAttribute getAttribute(ObjectModelClassifier clazz, String name) {
        ObjectModelAttribute result = clazz.getAttribute(name);

        // Ano #619 : FD-2010-05-17 : Specific case for Association class :
        // check on participant attributes
        if (result == null && clazz instanceof ObjectModelAssociationClassImpl) {
            if (log.isDebugEnabled()) {
                log.debug("Attribute " + name + " not found from " + clazz.getQualifiedName() + " association class. Will check participants...");
            }
            ObjectModelAssociationClass assoc = (ObjectModelAssociationClass) clazz;
            for (ObjectModelAttribute participant : assoc.getParticipantsAttributes()) {
                if (participant.getName().equals(name)) {
                    result = participant;
                    break;
                }
            }
        }
        return result;
    }

    protected ObjectModelAttribute getInheritedAttribute(ObjectModelClassifier clazz, String name) {
        return clazz.getAllOtherAttributes().stream().filter(a->a.getName().equals(name)).findFirst().orElse(null);
    }
}
