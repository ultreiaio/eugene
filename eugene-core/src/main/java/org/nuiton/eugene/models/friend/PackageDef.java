package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelPackageImpl;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class PackageDef extends ElementDef implements Comparable<PackageDef> {

    static final String PREFIX = "package ";

    public static PackageDef of(ObjectModelPackage aPackage) {
        PackageDef result = new PackageDef(aPackage.getName());
        result.loadStereotypesAndTagValues(aPackage);
        return result;
    }

    public static PackageDef of(String lineDef) {

        String firstLine = StringUtils.removeStart(lineDef, PREFIX).trim();
        Pair<String, String> pair = splitTagValues(firstLine);
        firstLine = pair.getLeft();
        String tagValues = pair.getRight();
        PackageDef result = new PackageDef(firstLine);
        if (tagValues != null) {
            result.loadStereotypesAndTagValues(tagValues);
        }
        return result;
    }

    private final String fullyQualifiedName;

    @Override
    public void write(BufferedWriter writer) throws IOException {
        if (getStereotypes().isEmpty() && getTagValues().isEmpty()) {
            return;
        }
        writer.newLine();
        writer.append(PREFIX).append(getName());
        writeStereotypesAndTagValues(writer);
    }

    @Override
    public String toString() {
        return getFullyQualifiedName();
    }

    String getFullyQualifiedName() {
        return fullyQualifiedName;
    }

    @Override
    public int compareTo(PackageDef o) {
        return getFullyQualifiedName().compareTo(o.getFullyQualifiedName());
    }

    ObjectModelPackageImpl toObjectModel() {
        ObjectModelPackageImpl result = new ObjectModelPackageImpl();
        result.setName(getFullyQualifiedName());
        flushStereotypesAndTagValues(result);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackageDef that = (PackageDef) o;
        return Objects.equals(fullyQualifiedName, that.fullyQualifiedName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullyQualifiedName);
    }

    private PackageDef(String name) {
        super(name);
        fullyQualifiedName = name;
    }

}
