package org.nuiton.eugene.models.tagvalue;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;

import java.util.Objects;

/**
 * This class contains all tag values of a model.
 * <p>
 * Any request of tag value from any element of model will pass here.
 * <p>
 * This fix a major design in object model tag values management:
 * <p>
 * for a given attribute, there is only one tag-value possible, we can't override it for a implementation since attributes
 * are not copied to implementation classifier so always points to attribute in parent classifier :(.
 * <p>
 * Using this delegate cache solve this problem and improve design of this part of object model.
 * <p>
 * Created on 14/05/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0-alpha-39
 */
public class ObjectModelTagValuesStore {

    private final TagValuesStore store;
    private final ObjectModel model;
    private final TagValueQueryBuilder queryBuilder;

    public ObjectModelTagValuesStore(ObjectModel model) {
        this.model = Objects.requireNonNull(model);
        this.store = new TagValuesStore();
        this.queryBuilder = new TagValueQueryBuilder(store);
    }

    public String findClassifierTagValue(TagValueMetadata tagName, ObjectModelClass clazz) {
        String result = onClassifier(clazz.getQualifiedName()).onTagValue(tagName.getName()).single();
        if (result == null) {
            for (ObjectModelClass superclass : clazz.getSuperclasses()) {
                result = findClassifierTagValue(tagName, superclass);
                if (result != null) {
                    break;
                }
            }
        }
        return result;
    }

    public String findAttributeTagValue(TagValueMetadata tagName, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        String result = onClassifier(clazz.getQualifiedName()).onAttribute(attribute.getName()).onTagValue(tagName.getName()).single();
        if (result == null) {
            for (ObjectModelClass superclass : clazz.getSuperclasses()) {
                result = findAttributeTagValue(tagName, superclass, attribute);
                if (result != null) {
                    break;
                }
            }
        }
        return result;
    }

    public boolean findClassifierBooleanTagValue(TagValueMetadata tagName, ObjectModelClass clazz) {
        String result = findClassifierTagValue(tagName, clazz);
        return TagValueUtil.findBooleanTagValue(result);
    }

    public Integer findClassifierIntegerTagValue(TagValueMetadata tagName, ObjectModelClass clazz) {
        String result = findClassifierTagValue(tagName, clazz);
        return TagValueUtil.findIntegerTagValue(result);
    }

    public boolean findAttributeBooleanTagValue(TagValueMetadata tagName, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        String result = findAttributeTagValue(tagName, clazz, attribute);
        return TagValueUtil.findBooleanTagValue(result);
    }

    public Integer findAttributeIntegerTagValue(TagValueMetadata tagName, ObjectModelClass clazz, ObjectModelAttribute attribute) {
        String result = findAttributeTagValue(tagName, clazz, attribute);
        return TagValueUtil.findIntegerTagValue(result);
    }

    public TagValueQueryBuilder.OnModel onModel(String modelName) {
        return queryBuilder.onModel(modelName);
    }

    public TagValueQueryBuilder.OnPackage onPackage(String packageName) {
        return queryBuilder.onPackage(packageName);
    }

    public TagValueQueryBuilder.OnClassifier onClassifier(String classifierName) {
        return queryBuilder.onClassifier(classifierName);
    }

    public TagValuesStore getStore() {
        return store;
    }

    public String getModelTagValue(String name) {
        return store.getModelTagValue(model.getName(), name);
    }

    public String getPackageTagValue(String packageName, String name) {
        return store.getPackageTagValue(packageName, name);
    }

    public String getClassifierTagValue(String classifierName, String name) {
        return store.getClassifierTagValue(classifierName, name);
    }

    public String getAttributeTagValue(String classifierName, String attributeName, String name) {
        return store.getAttributeTagValue(classifierName, attributeName, name);
    }
}
