package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelPackage;
import org.nuiton.eugene.models.object.xml.ObjectModelClassImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelDependencyImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelEnumerationImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelInterfaceImpl;
import org.nuiton.eugene.models.object.xml.ObjectModelPackageImpl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ModelDef extends ElementDef {
    /** Logger. */
    private static final Logger log = LogManager.getLogger(ModelDef.class);

    private static final String DEFAULT_PACKAGE = "defaultPackage";
    private static final String PREFIX = "model ";
    private static final String VERSION = "version";
    private static final String CONSTANTS = "constants";
    private static final String SKIP = "skip";

    private final List<PackageDef> packages = new LinkedList<>();
    private final List<InterfaceDef> interfaces = new LinkedList<>();
    private final List<ClassDef> classes = new LinkedList<>();
    private final List<EnumDef> enumerations = new LinkedList<>();

    private ModelDef(String name) {
        super(name);
    }

    public static ModelDef of(ObjectModel objectModel) {
        ModelDef model = new ModelDef(objectModel.getName());
        model.loadStereotypesAndTagValues(objectModel);
        if (objectModel.getVersion() != null) {
            model.addTagValue(VERSION, objectModel.getVersion());
        }
        String defaultPackage = objectModel.getTagValue(DEFAULT_PACKAGE);
        if (defaultPackage == null) {
            defaultPackage = "";
        } else {
            defaultPackage += ".";
        }
        for (ObjectModelPackage aPackage : objectModel.getPackages()) {
            model.add(PackageDef.of(aPackage));
        }
        for (ObjectModelEnumeration anEnumeration : objectModel.getEnumerations()) {
            model.add(PackageDef.of(anEnumeration.getPackageName()));
            model.add(EnumDef.of(anEnumeration, defaultPackage));
        }
        for (ObjectModelInterface anInterface : objectModel.getInterfaces()) {
            model.add(PackageDef.of(anInterface.getPackageName()));
            model.add(InterfaceDef.of(anInterface, defaultPackage));
        }
        for (ObjectModelClass aClass : objectModel.getClasses()) {
            model.add(PackageDef.of(aClass.getPackageName()));
            model.add(ClassDef.of(aClass, defaultPackage));
        }

        model.computeFullyQualifiedName(new LinkedHashMap<>());

        return model;
    }

    public static ModelDef of(ObjectModel objectModel, BufferedReader reader) throws IOException {
        ModelDef model = null;
        List<String> packages = new LinkedList<>();
        Map<String, String> classesMapping = new LinkedHashMap<>();
        if (objectModel != null && objectModel.getName() != null) {
            ModelDef incomingModel = of(objectModel);
            model = ModelDef.of("model " + incomingModel.getName());
            packages.addAll(incomingModel.getPackages().stream().map(PackageDef::getFullyQualifiedName).collect(Collectors.toSet()));
            incomingModel.getStereotypes().forEach(model::addStereotype);
            incomingModel.getTagValues().forEach(model::addTagValue);
            classesMapping = incomingModel.generateClassesMapping(classesMapping);
        }

        List<List<String>> linesGrouped = new LinkedList<>();
        List<String> currentGroup = null;
        String line;
        int linesCount = 0;
        while ((line = reader.readLine()) != null) {
            linesCount++;
            line = line.trim();
            if (line.startsWith("#")) {
                // comment
                continue;
            }
            if (line.isEmpty()) {

                // end of group
                if (currentGroup != null && !currentGroup.isEmpty()) {
                    linesGrouped.add(currentGroup);
                    currentGroup = null;
                }
                continue;
            }

            if (currentGroup == null) {
                currentGroup = new LinkedList<>();
            }
            if (line.startsWith(PREFIX)) {
                if (model == null) {
                    model = of(line);
                } else {

                    int tagValuesIndexOf = line.indexOf('|');
                    if (tagValuesIndexOf != -1) {
                        model.loadStereotypesAndTagValues(line.substring(tagValuesIndexOf + 1).trim());
                    }
                }
                continue;
            }

            if (line.startsWith(PackageDef.PREFIX)) {
                packages.add(line);
                continue;
            }
            currentGroup.add(line);
        }
        if (currentGroup != null && !currentGroup.isEmpty()) {
            linesGrouped.add(currentGroup);
        }
        log.debug(String.format("Loading %d line(s)", linesCount));

        log.debug(String.format("Loading %d group(s)", linesGrouped.size()));

        Objects.requireNonNull(model);

        String defaultPackage = model.getTagValues().get(DEFAULT_PACKAGE);
        if (defaultPackage == null) {
            defaultPackage = "";
        } else {
            defaultPackage += ".";
        }
        for (String aPackage : packages) {
            model.add(PackageDef.of(aPackage));
        }

        for (List<String> groupLines : linesGrouped) {
            model.loadGroup(groupLines, defaultPackage);
        }
        model.computeFullyQualifiedName(classesMapping);

        return model;
    }

    public static ModelDef of(String line) {
        String firstLine = StringUtils.removeStart(line, PREFIX).trim();
        int tagValuesIndexOf = firstLine.indexOf('|');
        if (tagValuesIndexOf != -1) {
            String tagValues = firstLine.substring(tagValuesIndexOf + 1).trim();
            firstLine = firstLine.substring(0, tagValuesIndexOf - 1);
            ModelDef modelDef = new ModelDef(firstLine.trim());
            modelDef.loadStereotypesAndTagValues(tagValues);
            return modelDef;
        } else {
            return new ModelDef(firstLine.trim());
        }
    }

    private void computeFullyQualifiedName(Map<String, String> incomingClassesMapping) {

        Map<String, String> classesMapping = generateClassesMapping(incomingClassesMapping);

        for (InterfaceDef anInterface : interfaces) {
            anInterface.resolveTypes(classesMapping);
        }
        for (EnumDef anEnum : enumerations) {
            anEnum.resolveTypes(classesMapping);
        }
        for (ClassDef aClass : classes) {
            aClass.resolveTypes(classesMapping);
        }

    }

    private Map<String, String> generateClassesMapping(Map<String, String> incomingClassesMapping) {
        Map<String, String> classesMapping = new TreeMap<>(incomingClassesMapping);
        for (InterfaceDef anInterface : interfaces) {
            classesMapping.put(anInterface.getName(), anInterface.getFullyQualifiedName());
        }
        for (EnumDef anEnum : enumerations) {
            classesMapping.put(anEnum.getName(), anEnum.getFullyQualifiedName());
        }
        for (ClassDef aClass : classes) {
            classesMapping.put(aClass.getName(), aClass.getFullyQualifiedName());
        }
        return classesMapping;
    }

    private void loadGroup(List<String> groupLines, String defaultPackage) {

        String firstLine = groupLines.remove(0);

        if (firstLine.startsWith(InterfaceDef.PREFIX)) {
            add(InterfaceDef.of(firstLine, groupLines, defaultPackage));
            return;
        }
        if (firstLine.startsWith(EnumDef.PREFIX)) {
            add(EnumDef.of(firstLine, groupLines, defaultPackage));
            return;
        }

        add(ClassDef.of(firstLine, groupLines, defaultPackage));

    }

    @Override
    public void write(BufferedWriter writer) throws IOException {

        Collections.sort(packages);
        Collections.sort(enumerations);
        Collections.sort(interfaces);
        Collections.sort(classes);

        writer.append(PREFIX).append(getName());
        writeStereotypesAndTagValues(writer);
        writer.newLine();

        for (PackageDef aPackage : packages) {
            aPackage.write(writer);
        }
        writer.newLine();
        for (EnumDef enumeration : enumerations) {
            enumeration.write(writer);
        }
        for (InterfaceDef anInterface : interfaces) {
            anInterface.write(writer);
        }
        for (ClassDef aClass : classes) {
            aClass.write(writer);
        }
    }

    private void add(PackageDef packageDef) {
        for (PackageDef aPackage : packages) {
            if (packageDef.equals(aPackage)) {
                return;
            }
        }
        log.debug(String.format("Add package: %s", packageDef));
        packages.add(packageDef);
    }

    private void add(EnumDef enumDef) {
        log.debug(String.format("Add enum: %s", enumDef));
        enumerations.add(enumDef);
    }

    private void add(InterfaceDef interfaceDef) {
        log.debug(String.format("Add interface: %s", interfaceDef));
        interfaces.add(interfaceDef);
    }

    public void add(ClassDef classDef) {
        log.debug(String.format("Add class: %s", classDef));
        classes.add(classDef);
    }

    public List<PackageDef> getPackages() {
        return packages;
    }

    public List<InterfaceDef> getInterfaces() {
        return interfaces;
    }

    public List<ClassDef> getClasses() {
        return classes;
    }

    public List<EnumDef> getEnumerations() {
        return enumerations;
    }

    public void load(String line) {
        String firstLine = StringUtils.removeStart(line, PREFIX).trim();
        int tagValuesIndexOf = firstLine.indexOf('|');
        if (tagValuesIndexOf != -1) {
            String tagValues = firstLine.substring(tagValuesIndexOf + 1).trim();
            loadStereotypesAndTagValues(tagValues);
        }
    }

    public void toObjectModel(ObjectModelImpl result) {
        result.setName(getName());
        if (getTagValues().containsKey(VERSION)) {
            result.setVersion(getTagValues().get(VERSION));
        }
        flushStereotypesAndTagValues(result);
        for (PackageDef aPackage : packages) {
            result.addPackage(aPackage.toObjectModel());
        }
        for (EnumDef anEnumeration : enumerations) {

            ObjectModelEnumerationImpl enumeration = anEnumeration.toObjectModel();
            result.addEnumeration(enumeration);
            ObjectModelPackageImpl aPackage = new ObjectModelPackageImpl();
            aPackage.setName(enumeration.getPackageName());
            result.addPackage(aPackage);
        }
        for (InterfaceDef anInterface : interfaces) {
            ObjectModelInterfaceImpl interfacez = anInterface.toObjectModel();
            result.addInterface(interfacez);
            ObjectModelPackageImpl aPackage = new ObjectModelPackageImpl();
            aPackage.setName(interfacez.getPackageName());
            result.addPackage(aPackage);
        }
        for (ClassDef aClass : classes) {
            ObjectModelClassImpl clazz = aClass.toObjectModel();
            ObjectModelPackageImpl aPackage = new ObjectModelPackageImpl();
            aPackage.setName(clazz.getPackageName());
            result.addPackage(aPackage);
            result.addClass(clazz);
        }
        for (ClassDef aClass : classes) {
            aClass.addReverseAttributes(result);
            ObjectModelEnumeration enumeration = result.getEnumeration(aClass.getFullyQualifiedName() + "Constants");
            if (enumeration != null) {
                //TODO Replace this with tagValues constants and add a warning
                ObjectModelClassImpl parent = (ObjectModelClassImpl) result.getClass(aClass.getFullyQualifiedName());
                ObjectModelDependencyImpl dependency = new ObjectModelDependencyImpl();
                dependency.setName(CONSTANTS);
                dependency.setClient(parent);
                dependency.setSupplierName(enumeration.getQualifiedName());
                parent.addDependency(dependency);
                enumeration.addStereotype(SKIP);
            }
        }
        for (InterfaceDef anInterface : interfaces) {
            ObjectModelEnumeration enumeration = result.getEnumeration(anInterface.getFullyQualifiedName() + "Constants");
            if (enumeration != null) {
                //TODO Replace this with tagValues constants and add a warning
                ObjectModelInterfaceImpl parent = (ObjectModelInterfaceImpl) result.getInterface(anInterface.getFullyQualifiedName());
                ObjectModelDependencyImpl dependency = new ObjectModelDependencyImpl();
                dependency.setName(CONSTANTS);
                dependency.setClient(parent);
                dependency.setSupplierName(enumeration.getQualifiedName());
                parent.addDependency(dependency);
                enumeration.addStereotype(SKIP);
            }
        }

    }
}
