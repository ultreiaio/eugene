package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.lang3.tuple.Pair;
import org.nuiton.eugene.models.object.ObjectModelParameter;
import org.nuiton.eugene.models.object.xml.ObjectModelParameterImpl;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ParameterDef extends ElementDef {

    public static ParameterDef of(ObjectModelParameter aParameter, String defaultPackage) {
        ParameterDef result = new ParameterDef(aParameter.getName(), getRelativeType(aParameter.getType(), defaultPackage));
        result.loadStereotypesAndTagValues(aParameter);
        return result;
    }

    public static ParameterDef of(String lineDef) {
        Pair<String, String> pair = splitTagValues(lineDef);
        lineDef = pair.getLeft();
        String tagValues = pair.getRight();

        int index = lineDef.indexOf(' ');
        String name = lineDef.substring(0, index - 1).trim();
        String type = lineDef.substring(index + 1).trim();

        ParameterDef result = new ParameterDef(name, type);
        if (tagValues != null) {
            result.loadStereotypesAndTagValues(tagValues);
        }
        return result;
    }

    private final String type;
    private String fullyQualifiedType;

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return getName() + ": " + type;
    }

    @Override
    public void write(BufferedWriter writer) throws IOException {
        writer.append(getName()).append(" ");
        writer.append(type);
        writeStereotypesAndTagValues(writer);
    }

    void resolveType(Map<String, String> classesMapping) {
        fullyQualifiedType = resolveType(type, classesMapping);
    }

    ObjectModelParameterImpl toObjectModel() {
        ObjectModelParameterImpl result = new ObjectModelParameterImpl();
        result.setName(getName());
        result.setType(getFullyQualifiedType());
        flushStereotypesAndTagValues(result);
        return result;
    }

    private String getFullyQualifiedType() {
        return fullyQualifiedType;
    }

    private ParameterDef(String name, String type) {
        super(name);
        this.type = type;
    }
}
