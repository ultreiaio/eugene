package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.base.Joiner;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.xml.ObjectModelImplRef;
import org.nuiton.eugene.models.object.xml.ObjectModelInterfaceImpl;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class InterfaceDef extends ClassifierDef<InterfaceDef> {

    public static final String PREFIX = "interface ";

    public static InterfaceDef of(ObjectModelInterface anInterface, String defaultPackage) {
        InterfaceDef result = new InterfaceDef(getRelativeType(anInterface.getQualifiedName(), defaultPackage), defaultPackage);
        result.loadStereotypesAndTagValues(anInterface);
        for (ObjectModelInterface objectModelInterface : anInterface.getInterfaces()) {
            result.addExtend(objectModelInterface.getQualifiedName());
        }
        for (ObjectModelOperation objectModelOperation : anInterface.getOperations()) {
            result.add(OperationDef.of(objectModelOperation, defaultPackage));
        }
        return result;
    }

    public static InterfaceDef of(String line, List<String> groupLines, String defaultPackage) {

        String firstLine = StringUtils.removeStart(line, PREFIX).trim();
        Pair<String, String> pair = splitTagValues(firstLine);
        firstLine = pair.getLeft();
        String tagValues = pair.getRight();

        int extendIndexOf = firstLine.indexOf('>');
        String extend = null;
        if (extendIndexOf != -1) {
            extend = firstLine.substring(extendIndexOf + 1).trim();
            firstLine = firstLine.substring(0, extendIndexOf - 1);
        }
        InterfaceDef result = new InterfaceDef(firstLine.trim(), defaultPackage);
        if (tagValues != null) {
            result.loadStereotypesAndTagValues(tagValues);
        }
        if (extend != null) {
            for (String oneExtend : extend.split(",")) {
                String trim = oneExtend.trim();
                if (!trim.isEmpty()) {
                    result.addExtend(trim);
                }
            }
        }

        for (String groupLine : groupLines) {
            result.add(OperationDef.of(groupLine));
        }

        return result;
    }

    /** Logger. */
    private static final Logger log = LogManager.getLogger(InterfaceDef.class);

    private final List<String> extend = new LinkedList<>();

    private final List<String> fullyQualifiedExtend = new LinkedList<>();

    @Override
    public void write(BufferedWriter writer) throws IOException {
        writer.newLine();
        writer.append(PREFIX).append(getName());
        writeSimpleList(getExtend(), " > ", writer);
        writeStereotypesAndTagValues(writer);
        for (OperationDef operationDef : getOperations()) {
            operationDef.write(writer);
        }
        writer.newLine();
    }

    @Override
    public String toString() {
        return getName()
                + (getExtend().isEmpty() ? "" : " extends " + Joiner.on(",").join(extend))
                + (getOperations().isEmpty() ? "" : " ops: " + Joiner.on(",").join(getOperations()));
    }

    List<String> getExtend() {
        return extend;
    }

    List<String> getFullyQualifiedExtend() {
        return fullyQualifiedExtend;
    }

    @Override
    void resolveTypes(Map<String, String> classesMapping) {
        super.resolveTypes(classesMapping);

        fullyQualifiedExtend.clear();
        for (String extend : extend) {
            fullyQualifiedExtend.add(resolveType(extend, classesMapping));
        }
    }

    ObjectModelInterfaceImpl toObjectModel() {
        ObjectModelInterfaceImpl result = new ObjectModelInterfaceImpl();
        toObjectModel(result);
        for (String fullyQualifiedExtend : fullyQualifiedExtend) {
            ObjectModelImplRef ref = new ObjectModelImplRef();
            ref.setName(fullyQualifiedExtend);
            result.addInterface(ref);
        }
        return result;
    }

    private void addExtend(String extend) {
        log.debug(String.format("Add extend: %s on %s", extend, getName()));
        this.extend.add(extend);
    }

    private InterfaceDef(String name, String defaultPackage) {
        super(name, defaultPackage);
    }
}
