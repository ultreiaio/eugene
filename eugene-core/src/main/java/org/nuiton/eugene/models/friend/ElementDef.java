package org.nuiton.eugene.models.friend;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableSet;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.nuiton.eugene.models.extension.tagvalue.WithTagValuesOrStereotypes;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class ElementDef {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(ElementDef.class);

    private final String name;
    private final Set<String> stereotypes = new LinkedHashSet<>();
    private final Map<String, String> tagValues = new TreeMap<>();

    private static final Set<String> IGNORED_TAG_VALUES = ImmutableSet.of("documentation", "since", "author");

    ElementDef(String name) {
        this.name = name;
    }

    public abstract void write(BufferedWriter writer) throws IOException;

    public void addStereotype(String stereotype) {
        log.debug(String.format("Add stereotype: %s on %s", stereotype, name));
        stereotypes.add(stereotype);
    }

    public void addTagValue(String key, String value) {
        if (IGNORED_TAG_VALUES.contains(key)) {
            // don't import this
            return;
        }
        if ("true".equals(value)) {
            addStereotype(key);
        } else {
            log.debug(String.format("Add tag value: %s=%s on %s", key, value, name));
            tagValues.put(key, value);
        }
    }

    void flushStereotypesAndTagValues(WithTagValuesOrStereotypes model) {
        for (String stereotype : stereotypes) {
            model.addTagValue(stereotype, "true");
        }
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            model.addTagValue(entry.getKey(), entry.getValue());
        }
    }

    void loadStereotypesAndTagValues(String tagValues) {
        String[] parts = tagValues.split("\\s+");
        for (String part : parts) {
            part = part.trim();
            String[] tagValueParts = part.split("=");
            String firstPart = tagValueParts[0].trim();
            if (tagValueParts.length == 1) {
                addStereotype(firstPart);
            } else {
                String secondPart = tagValueParts[1].trim();
                addTagValue(firstPart, secondPart);
            }
        }
    }

    public String getName() {
        return name;
    }

    public Set<String> getStereotypes() {
        return stereotypes;
    }

    public Map<String, String> getTagValues() {
        return tagValues;
    }

    void writeSimpleList(List<String> list, String prefix, BufferedWriter writer) throws IOException {
        if (!list.isEmpty()) {
            writer.append(prefix);
            boolean first = true;
            for (String one : list) {
                if (first) {
                    first = false;
                } else {
                    writer.append(",");
                }
                writer.append(one);
            }
        }
    }

    void writeStereotypesAndTagValues(BufferedWriter writer) throws IOException {
        if (getStereotypes().isEmpty() && getTagValues().isEmpty()) {
            return;
        }
        writer.append(" |");
        for (String stereotype : stereotypes) {
            writer.append(" ").append(stereotype);
        }
        for (Map.Entry<String, String> entry : tagValues.entrySet()) {
            writer.append(" ").append(entry.getKey()).append("=").append(entry.getValue());
        }

    }

    private static ImmutableSet<String> PRIMITIVES = ImmutableSet.of("boolean", "byte", "char", "int", "float", "double", "long");
    private static ImmutableSet<String> PRIMITIVES_OBJECTS = ImmutableSet.of("String", "Boolean", "Byte", "Character", "Integer", "Float", "Double", "Long");
    private static ImmutableSet<String> PRIMITIVES_OBJECTS2 = ImmutableSet.of("java.lang.String", "java.lang.Boolean", "java.lang.Byte", "java.lang.Character", "java.lang.Integer", "java.lang.Float", "java.lang.Double", "java.lang.Long");
    private static ImmutableSet<String> JAVA_UTIL = ImmutableSet.of("Collection", "Set", "List", "Map", "Date");

    String resolveType(String value, Map<String, String> classesMapping) {
        if (value.isEmpty()) {
            return "void";
        }
        if (value.startsWith("!")) {
            return value.substring(1);
        }
        String result = classesMapping.get(value);
        if (result == null) {
            if (PRIMITIVES_OBJECTS.contains(value)) {
                result = "java.lang." + value;
            } else if (JAVA_UTIL.contains(value)) {
                result = "java.util." + value;
            } else {
                for (String s : JAVA_UTIL) {
                    if (value.startsWith(s + "<")) {
                        return "java.util." + value;
                    }
                }
                result = value;
            }
        }
        return result;
    }

    void loadStereotypesAndTagValues(WithTagValuesOrStereotypes model) {
        for (String stereotype : model.getStereotypes()) {
            addStereotype(stereotype);
        }
        for (Map.Entry<String, String> entry : model.getTagValues().entrySet()) {
            addTagValue(entry.getKey(), entry.getValue());
        }
    }


    void write(BufferedWriter writer, Collection<? extends ElementDef> defs, String separator) throws IOException {
        boolean first = true;
        for (ElementDef def : defs) {
            if (first) {
                first = false;
            } else {
                writer.append(separator);
            }
            def.write(writer);
        }
    }

    static String getRelativeType(String name, String defaultPackage) {
        if (PRIMITIVES.contains(name) || PRIMITIVES_OBJECTS.contains(name) || JAVA_UTIL.contains(name)){
            return name;
        }
        if (PRIMITIVES_OBJECTS2.contains(name)) {
            return StringUtils.removeStart(name, "java.lang.");
        }
        for (String s : JAVA_UTIL) {
            if (name.startsWith("java.util." + s)) {
                return s + StringUtils.removeStart(name, "java.util." + s);
            }
        }
        if (Objects.equals(name, "") || "void".equals(name)) {
            return "";
        }
        int indexOf = name.indexOf(defaultPackage);
        try {
            return indexOf == 0 ? name.substring(indexOf + defaultPackage.length()) : "!" + name;
        } catch (Exception e) {
            throw e;
        }
    }

    static Pair<String, String> splitTagValues(String line) {
        return splitTagValues(line, 0);
    }

    static Pair<String, String> splitTagValues(String line, int pos) {
        int tagValuesIndexOf = line.indexOf('|', pos);
        if (tagValuesIndexOf != -1) {
            String tagValues = line.substring(tagValuesIndexOf + 1).trim();
            line = line.substring(0, tagValuesIndexOf - 1);
            return Pair.of(line, tagValues);
        }
        return Pair.of(line, null);
    }
}
