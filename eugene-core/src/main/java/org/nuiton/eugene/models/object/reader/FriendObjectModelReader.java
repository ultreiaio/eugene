/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

package org.nuiton.eugene.models.object.reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.nuiton.eugene.ModelHelper;
import org.nuiton.eugene.models.friend.ModelDef;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.xml.ObjectModelImpl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * To read object model from yaml files into an memory object model.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */
public class FriendObjectModelReader extends AbstractObjectModelReader {

    private static final Logger log = LogManager.getLogger(FriendObjectModelReader.class);

    @Override
    public String getInputType() {
        return ModelHelper.ModelInputType.FRIEND.getAlias();
    }

    @Override
    protected void beforeReadFile(File... files) {
        super.beforeReadFile(files);
    }

    @Override
    protected void readFileToModel(File file, ObjectModel model) throws IOException {
        log.info("Read file: " + file);
        try {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
                ModelDef modelDef = ModelDef.of(model, reader);
                modelDef.toObjectModel((ObjectModelImpl) model);
            }
        } catch (Exception e) {
            throw new IOException("Unable to parse input file : " + file, e);
        }
    }

}
