package org.nuiton.eugene;

/*
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Some useful methods to locate resources.
 * <p>
 * Created on 08/04/16.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class ResourcesHelper {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(ResourcesHelper.class);

    protected final ClassLoader classLoader;
    protected final File outputDirectory;
    protected final boolean verbose;

    //FIXME Add the generated source root to be able to search in generated java files...
    public ResourcesHelper(ClassLoader classLoader, File outputDirectory, boolean verbose) {
        this.classLoader = classLoader;
        this.outputDirectory = outputDirectory;
        this.verbose = verbose;
    }

    /**
     * Checks if the given fully qualified name java file is in output directory.
     * <p>
     * Send a log a message if found.
     *
     * @param fqn fully qualified name to test
     * @return {@code true} if java file was found in output directory, {@code false} otherwise.
     */
    public boolean isJavaFileInOutputDirectory(String fqn) {
        URL fileLocation = getFileInOutputDirectory(fqn, "\\.", ".java");

        return fileLocation != null;
    }

    /**
     * Checks if the given fully qualified name java file (or class) is in class-path.
     * <p>
     * Send a log a message if found.
     *
     * @param fqn fully qualified name to test
     * @return {@code true} if java file was found in class-path, {@code false} otherwise.
     */
    public boolean isJavaFileInClassPath(String fqn) {
        return isFullyQualifiedNameInClassPath(fqn, ".java");
    }

    /**
     * Checks if the given fully qualified name (path separated by {@code pathSeparator} and optionally suffixed by
     * {@code extension}) (and class if extension is .java) is in class-path.
     * <p>
     * Send a log a message if found.
     *
     * @param extension file extension
     * @param fqn       fully qualified name to test
     * @return {@code true} if resource was found in class-path, {@code false} otherwise.
     */
    public boolean isFullyQualifiedNameInClassPath(String fqn, String extension) {
        return isInClassPath(fqn, "\\.", extension);
    }

    /**
     * Checks if the given fully qualified name (path separated by {@code pathSeparator} and optionally suffixed by
     * {@code extension}) (and class if extension is {@code .java}) is in class-path.
     * <p>
     * Send a log a message if found.
     *
     * @param extension file extension
     * @param fqn       fully qualified name to test
     * @return {@code true} if resource was found in class-path, {@code false} otherwise.
     */
    public boolean isInClassPath(String fqn, String pathSeparator, String extension) {

        URL fileLocation = getFileInClassPath(fqn, pathSeparator, extension);

        if (fileLocation != null) {

            // there is already an existing file in class-path, skip

            if (log.isDebugEnabled()) {
                log.debug("Will not generate [" + fqn + "], already found in class-path at location : " + fileLocation);
            } else if (verbose) {
                log.info("Will not generate [" + fqn + "], already found in class-path.");
            }

            return true;
        }

        if (".java".equals(extension)) {

            // try now the class (we prefer not to use classloader to avoid exceptions...

            URL classLocation = getFileInClassPath(fqn, pathSeparator, ".class");

            if (classLocation != null) {

                // there is already an existing class in class-path, skip

                if (log.isDebugEnabled()) {
                    log.debug("Will not generate [" + fqn + "], already found in class-path at location : " + classLocation);
                } else if (verbose) {
                    log.info("Will not generate [" + fqn + "], already found in class-path.");
                }

                return true;
            }

        }

        // is not found
        return false;
    }

    protected URL getFileInClassPath(String fqn, String pathSeparator, String extension) {
        String resourceName = fqn.replaceAll(pathSeparator, "/") + extension;
        URL fileLocation = classLoader.getResource(resourceName);
        if (log.isDebugEnabled()) {
            log.debug("Look for resource : " + resourceName + " = " + fileLocation);
        }
        return fileLocation;
    }

    protected URL getFileInOutputDirectory(String fqn, String pathSeparator, String extension) {
        String resourceName = fqn.replaceAll(pathSeparator, File.separator) + extension;
        File fileLocation = new File(outputDirectory, resourceName);
        if (!fileLocation.exists()) {
            return null;
        }
        try {
            return fileLocation.toURI().toURL();
        } catch (MalformedURLException ignored) {
            return null;
        }
    }
}
