package org.nuiton.eugene.models.object.reader.friend.model;

/*-
 * #%L
 * EUGene :: EUGene Core
 * %%
 * Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.junit.Test;
import org.nuiton.eugene.models.friend.ModelDef;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by tchemit on 22/06/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ModelDefTest {

    /** Logger. */
    private static final Logger log = LogManager.getLogger(ModelDefTest.class);

    private static final String CONTENT = "model yo |version=1.0 defaultPackage=fr.ird.observe.entities\n" +
            "package referential.common |entity schema=common\n" +
            "package referential.seine |entity schema=seine\n" +
            "package referential.longline |entity schema=longline\n" +
            "\n" +
            "enum ObserveType\n" +
            "A\n" +
            "B\n" +
            "C\n" +
            "\n" +
            "interface referential.Referential\n" +
            "\n" +
            "interface referential.ObserveReferentialEntity > Yo,Ya,Yi |extern schemaName=12\n" +
            "isDisabled(param1 boolean, param2 String) boolean |external yo=34\n" +
            "isEnabled()  boolean\n" +
            "\n" +
            "class referential.ObserveReferentialEntity\n" +
            "isEnabled()  boolean\n" +
            "isDisabled() boolean\n" +
            "\n" +
            "I18nReferentialEntity > ObserveReferentialEntity\n" +
            "label1 String |externic ya=567\n" +
            "label2 String\n" +
            "label3 String\n" +
            "label4 String\n" +
            "label5 String\n" +
            "label6 String\n" +
            "label7 String\n" +
            "label8 String\n" +
            "\n" +
            "Ocean > Referential\n" +
            "\n" +
            "Person > Referential\n" +
            "firstName String\n" +
            "lastName String\n" +
            "\n" +
            "data.seine.Trip\n" +
            "route + {1:*} data.seine.Route\n" +
            "\n" +
            "data.seine.Route\n" +
            "\n" +
            "referential.Species > referential.Referential >> Yo,Ya,Yu | entity tableName=ya\n" +
            "firstName String |unique\n" +
            "lastName String\n" +
            "ocean:mySpecies     {1:*} Ocean |ordered dbName=yo\n" +
            "ocean2    {1:+} Ocean |ordered dbName=yo\n" +
            "ocean3 {0..1} Ocean |ordered dbName=yo\n" +
            "ocean3 {1} Ocean |ordered dbName=yo\n" +
            "ocean4 +  {*} Ocean |ordered dbName=yo\n" +
            "ocean5 -  {*} Ocean |ordered dbName=yo\n" +
            "ocean6 +- {*} Ocean |ordered dbName=yo\n" +
            "ocean7 {*:*} Ocean |ordered dbName=yo\n" +
            "test() void\n" +
            "test(param1 String | unique paramName=yl, param2 String) String\n";

    @Test
    public void of() throws Exception {

        ModelDef modelDef;
        try (BufferedReader reader = new BufferedReader(new StringReader(CONTENT))) {
            modelDef = ModelDef.of(null, reader);
        }

        try (StringWriter out = new StringWriter()) {
            try (BufferedWriter writer = new BufferedWriter(out)) {
                modelDef.write(writer);
                writer.flush();
            }

            log.info("New model:\n" + out.toString());
        }

    }

}
