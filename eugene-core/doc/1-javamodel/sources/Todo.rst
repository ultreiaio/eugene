====
TODO
====


:Revision: $Revision$
:Date: $Date$


Idées ou choses à faire
=======================

- support des InnerClasses (en xmi argo: la classe est declarer dans une classe
  dans un element UML:Namespace.ownedElement)

- support des static (tout element declarer dans une class/interface: methode,
  attribut, enumeration, class, ...)
  (en xmi argo: ownerScope='classifier' ou ownerScope='instance')

- peut-etre faire des tests unitaires avec http://juxy.tigris.org/ pour le xls

- Permettre de faire du 'model to model' avant la generation

Model to Model
==============

Pour tout ce qui est génération de classe Java, le principe de base d'Eugene
est de dire que le développeur écrit directement ce qu'il souhaite voir dans
le fichier final.

Cela pose quelques difficultés:

- difficulté de prévoir les imports
- difficulté d'éviter la génération de deux méthodes ayant le même nom

L'idée serait alors de constuire la classe Java souhaitée au final en appelant
des méthodes (addImport, addMethod, addAttribute, ...). Ceci nous donnerait
alors un nouveau modèle mémoire dont on demanderait la génération à un
générateur sans inteligence qui se contenterait d'écrire le code Java modélisé.

L'avantage est que pour chaque méthode à ajouter on peut demander avant si elle
n'a pas déjà été ajoutée et ainsi potentiellement la renomé avant de l'inclure.
D'ajouter de façon automatique tous les imports des arguments des méthodes, ...

Pour pouvoir faire cela, nous pourrions nous baser sur ObjectModel, mais des
ajouter doivent être fait:

- pouvoir indiquer sur une Class une série d'import.
- pouvoir ajouter un message de licence
- pouvoir ajouter sur une méthode sont code source

Ce dernier élément est le plus problèmatique, car il deviendrait pénible de
devoir instancier tout le code de la méthode en objet et on perdrait l'idée
de départ d'Eugene.

Dans eugène il est déjà possible de modifier la transformation des tags de
génération '/\*{' et '}\*/' qui par défaut génère un 'ouput.write("...");'
Dans notre cas, il faudrait quelque chose comme 'method.addCode("...");' ou
encore plus simplement '"..." +' ce qui pourrait donner::

  addImport(MonObject.class);
  method.addCode(
  /*{ int i = 0;
      i = 1+2;
      MonObject result = new MonObject(result);
      return result;
  }*/
  );

qui deviendrait::

  addImport(MonObject.class);
  method.addCode(
    "int i = 0;\n" +
    "i = 1+2;\n" +
    "MonObject result = new MonObject(result);\n" +
    "return result;\n"
  );

On garde la facilte d'écriture du code à générer et on permet d'ajouter
facilement des imports dont on aurait besoin dans le coprs de la méthode. Aucun
traitement complexe du code de la méthode n'est donc nécessaire.

Ce nouveau mode de génération ne serait qu'une moyen complémentaire de la
génération actuelle.

Travail a faire:

- ajout d'objet dans ObjectModel
- ajout de méthode de modification dans ObjectModel
- création d'un template de génération d'un POJO (génération direct d'un modèle)

Il serait toute fois dommage d'ajouter tout un ensemnble de setter sur les
interfaces d'ObjectModel qui le rendrait moins clair pour les générateurs.
Il faut donc trouver une solution pour permettre l'instanciation et la
modification de classe implantant l'ObjectModel sans pour autant les ajouter
au interface.

Une idée pourrait-être l'utilisation de Helper qui permette de créer et modifier
le modèle::

  ObjectModelClass clazz = ObjectModelHelper.createClass();
  ObjectModelOperation method = ObjectModelHelper.addMethod(
                         clazz, "name", "typeRetour", "paramName", "typeParam");
  ObjectModelHelper.addImport("java.util.List");
  ObjectModelHelper.addCode(method,
    /*{ blablabla
    }*/
