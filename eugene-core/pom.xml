<?xml version="1.0" encoding="UTF-8"?>
<!--
  #%L
  EUGene :: EUGene Core
  %%
  Copyright (C) 2004 - 2025 Code Lutin, Ultreia.io
  %%
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Lesser General Public License as
  published by the Free Software Foundation, either version 3 of the
  License, or (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Lesser Public License for more details.
  You should have received a copy of the GNU General Lesser Public
  License along with this program.  If not, see
  <http://www.gnu.org/licenses/lgpl-3.0.html>.
  #L%
  -->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>io.ultreia.java4all</groupId>
    <artifactId>eugene</artifactId>
    <version>3.0-beta-3-SNAPSHOT</version>
  </parent>
  <groupId>io.ultreia.java4all.eugene</groupId>
  <artifactId>eugene-core</artifactId>
  <name>EUGene :: EUGene Core</name>
  <description>Efficient Universal Generator.</description>
  <dependencies>
    <dependency>
      <groupId>com.google.guava</groupId>
      <artifactId>guava</artifactId>
    </dependency>
    <dependency>
      <groupId>commons-io</groupId>
      <artifactId>commons-io</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-lang</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all</groupId>
      <artifactId>java-util</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.i18n</groupId>
      <artifactId>i18n-runtime</artifactId>
    </dependency>
    <dependency>
      <groupId>io.ultreia.java4all.i18n</groupId>
      <artifactId>i18n-spi-builder</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.annotation</groupId>
      <artifactId>jsr250-api</artifactId>
    </dependency>
    <dependency>
      <groupId>javax.inject</groupId>
      <artifactId>javax.inject</artifactId>
      <scope>provided</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-collections4</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-configuration2</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-lang3</artifactId>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
    </dependency>
    <dependency>
      <groupId>org.codehaus.plexus</groupId>
      <artifactId>plexus-utils</artifactId>
    </dependency>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <scope>test</scope>
    </dependency>
  </dependencies>
  <build>
    <plugins>
      <plugin>
        <groupId>org.nuiton.processor</groupId>
        <artifactId>processor-maven-plugin</artifactId>
        <configuration>
          <includes>**/*.java</includes>
          <filters>org.nuiton.processor.filters.GeneratorTemplatesFilter</filters>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>process</goal>
            </goals>
            <phase>generate-sources</phase>
          </execution>
        </executions>
      </plugin>
      <!-- expose sisu components -->
      <plugin>
        <groupId>org.eclipse.sisu</groupId>
        <artifactId>sisu-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>generate-index</id>
            <goals>
              <goal>main-index</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!-- expose new plexus components -->
      <plugin>
        <groupId>org.codehaus.plexus</groupId>
        <artifactId>plexus-component-metadata</artifactId>
        <executions>
          <execution>
            <goals>
              <goal>generate-metadata</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
  <profiles>
    <!-- perform only on a release stage when using the maven-release-plugin -->
    <profile>
      <id>reporting</id>
      <activation>
        <property>
          <name>performRelease</name>
          <value>true</value>
        </property>
      </activation>
      <build>
        <pluginManagement>
          <plugins>
            <plugin>
              <groupId>io.ultreia.java4all.eugene</groupId>
              <artifactId>eugene-maven-plugin</artifactId>
              <version>${eugene.version}</version>
              <dependencies>
                <dependency>
                  <groupId>${project.groupId}</groupId>
                  <artifactId>${project.artifactId}</artifactId>
                  <version>${project.version}</version>
                </dependency>
              </dependencies>
            </plugin>
          </plugins>
        </pluginManagement>
      </build>
      <reporting>
        <plugins>
          <plugin>
            <groupId>org.codehaus.plexus</groupId>
            <artifactId>plexus-component-metadata</artifactId>
          </plugin>
          <plugin>
            <groupId>io.ultreia.java4all.eugene</groupId>
            <artifactId>eugene-maven-plugin</artifactId>
            <version>${eugene.version}</version>
            <reportSets>
              <reportSet>
                <reports>
                  <report>tag-values-report</report>
                </reports>
              </reportSet>
            </reportSets>
          </plugin>
        </plugins>
      </reporting>
    </profile>
  </profiles>
</project>
