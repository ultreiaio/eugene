# EuGene changelog

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2022-11-01 09:21.

## Version [3.0-beta-2](https://gitlab.com/ultreiaio/eugene/-/milestones/35)

**Closed at 2022-11-01.**


### Issues
  * [[bug 50]](https://gitlab.com/ultreiaio/eugene/-/issues/50) **ObjectModelTagValuesStore does not recurse to all super classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-beta-1](https://gitlab.com/ultreiaio/eugene/-/milestones/34)

**Closed at 2022-10-03.**


### Issues
  * [[enhancement 48]](https://gitlab.com/ultreiaio/eugene/-/issues/48) **Fix some saxon issues, now can use last log4j and slf4j... What a mess** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 49]](https://gitlab.com/ultreiaio/eugene/-/issues/49) **Remove yaml and plantuml stuff** (Thanks to ) (Reported by Tony CHEMIT)

## Version [3.0-alpha-42](https://gitlab.com/ultreiaio/eugene/-/milestones/32)

**Closed at 2021-09-27.**


### Issues
  * [[enhancement 46]](https://gitlab.com/ultreiaio/eugene/-/issues/46) **Fix some real mess in ObjectModel impl** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-41](https://gitlab.com/ultreiaio/eugene/-/milestones/31)

**Closed at 2021-07-19.**


### Issues
  * [[bug 43]](https://gitlab.com/ultreiaio/eugene/-/issues/43) **Fix annotation with classes parameter generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 44]](https://gitlab.com/ultreiaio/eugene/-/issues/44) **Add more methods to ObjectModelTransformerToJava around annotations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 45]](https://gitlab.com/ultreiaio/eugene/-/issues/45) **Open ObjectModelTransformerToJava API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-40](https://gitlab.com/ultreiaio/eugene/-/milestones/30)

**Closed at 2021-06-25.**


### Issues
  * [[enhancement 41]](https://gitlab.com/ultreiaio/eugene/-/issues/41) **Add more log when tag-values can&#39;t be loaded** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 42]](https://gitlab.com/ultreiaio/eugene/-/issues/42) **Be able to load tag-values on inherited classes attributes without changing the attribute tagvalue store** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-39](https://gitlab.com/ultreiaio/eugene/-/milestones/28)

**Closed at 2021-05-18.**


### Issues
  * [[enhancement 38]](https://gitlab.com/ultreiaio/eugene/-/issues/38) **Migrate to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 40]](https://gitlab.com/ultreiaio/eugene/-/issues/40) **Improve tag-values design to be able to override them** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-38](https://gitlab.com/ultreiaio/eugene/-/milestones/27)

**Closed at 2020-11-20.**


### Issues
  * [[enhancement 36]](https://gitlab.com/ultreiaio/eugene/-/issues/36) **Final is not generated on operations** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-37](https://gitlab.com/ultreiaio/eugene/-/milestones/26)

**Closed at 2020-11-02.**


### Issues
  * [[enhancement 35]](https://gitlab.com/ultreiaio/eugene/-/issues/35) **fix one for all problem while generating java class with same name as his parent (or interfaces)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-36](https://gitlab.com/ultreiaio/eugene/-/milestones/25)

**Closed at 2020-10-05.**


### Issues
No issue.

## Version [3.0-alpha-35](https://gitlab.com/ultreiaio/eugene/-/milestones/24)

**Closed at 2020-10-01.**


### Issues
  * [[bug 34]](https://gitlab.com/ultreiaio/eugene/-/issues/34) **Friend model does not get attribute default value** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-34](https://gitlab.com/ultreiaio/eugene/-/milestones/23)

**Closed at 2020-06-27.**


### Issues
  * [[enhancement 31]](https://gitlab.com/ultreiaio/eugene/-/issues/31) **Add excludedPattern in ImportManager** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 32]](https://gitlab.com/ultreiaio/eugene/-/issues/32) **Get a nice getImportManager on ObjectModelTransformerToJava** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 33]](https://gitlab.com/ultreiaio/eugene/-/issues/33) **Add importAndSimplify method in ImportManager** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-33](https://gitlab.com/ultreiaio/eugene/-/milestones/22)

**Closed at 2020-03-20.**


### Issues
  * [[enhancement 29]](https://gitlab.com/ultreiaio/eugene/-/issues/29) **Add GenerateJavaBeanDefinition annotation only on concrete classes** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[enhancement 30]](https://gitlab.com/ultreiaio/eugene/-/issues/30) **Migrates to java 10, but still be able to build on java 8** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-32](https://gitlab.com/ultreiaio/eugene/-/milestones/21)

**Closed at 2020-02-09.**


### Issues
  * [[bug 26]](https://gitlab.com/ultreiaio/eugene/-/issues/26) **Can&#39;t generate static methods in interface** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 27]](https://gitlab.com/ultreiaio/eugene/-/issues/27) **Improve JavaBuilder usage in Transformer** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[bug 28]](https://gitlab.com/ultreiaio/eugene/-/issues/28) **Improve usage of computed method in ObjectModel impl** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-31](https://gitlab.com/ultreiaio/eugene/-/milestones/20)

**Closed at 2019-07-25.**


### Issues
  * [[enhancement 25]](https://gitlab.com/ultreiaio/eugene/-/issues/25) **Make synonyms possible in extends or implements in JavaGenerator** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-30](https://gitlab.com/ultreiaio/eugene/-/milestones/19)

**Closed at 2019-06-26.**


### Issues
  * [[enhancement 24]](https://gitlab.com/ultreiaio/eugene/-/issues/24) **Permits to use synonyms in a Bean** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-28](https://gitlab.com/ultreiaio/eugene/-/milestones/18)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-27](https://gitlab.com/ultreiaio/eugene/-/milestones/17)

**Closed at 2018-12-16.**


### Issues
  * [[enhancement 23]](https://gitlab.com/ultreiaio/eugene/-/issues/23) **Be able to use homonym bean** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-26](https://gitlab.com/ultreiaio/eugene/-/milestones/16)

**Closed at *In progress*.**


### Issues
  * [[enhancement 22]](https://gitlab.com/ultreiaio/eugene/-/issues/22) **Use new I18n api to detect keys** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-25](https://gitlab.com/ultreiaio/eugene/-/milestones/15)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-24](https://gitlab.com/ultreiaio/eugene/-/milestones/14)

**Closed at 2018-08-02.**


### Issues
  * [[enhancement 21]](https://gitlab.com/ultreiaio/eugene/-/issues/21) **Migrates to Log4J2** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-23](https://gitlab.com/ultreiaio/eugene/-/milestones/13)

**Closed at 2018-07-11.**


### Issues
  * [[enhancement 20]](https://gitlab.com/ultreiaio/eugene/-/issues/20) **Can add GenerateJavaBeanDefinition annotation on bean** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [3.0-alpha-22](https://gitlab.com/ultreiaio/eugene/-/milestones/12)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-21](https://gitlab.com/ultreiaio/eugene/-/milestones/11)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-20](https://gitlab.com/ultreiaio/eugene/-/milestones/10)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-19](https://gitlab.com/ultreiaio/eugene/-/milestones/9)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-18](https://gitlab.com/ultreiaio/eugene/-/milestones/8)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-17](https://gitlab.com/ultreiaio/eugene/-/milestones/7)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-16](https://gitlab.com/ultreiaio/eugene/-/milestones/6)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-15](https://gitlab.com/ultreiaio/eugene/-/milestones/5)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-14](https://gitlab.com/ultreiaio/eugene/-/milestones/4)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-13](https://gitlab.com/ultreiaio/eugene/-/milestones/3)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-12](https://gitlab.com/ultreiaio/eugene/-/milestones/2)

**Closed at *In progress*.**


### Issues
No issue.

## Version [3.0-alpha-11](https://gitlab.com/ultreiaio/eugene/-/milestones/1)

**Closed at *In progress*.**


### Issues
No issue.

