.. -
.. * #%L
.. * EUGene
.. * 
.. * $Id$
.. * $HeadURL$
.. * %%
.. * Copyright (C) 2004 - 2010 CodeLutin
.. * %%
.. * This program is free software: you can redistribute it and/or modify
.. * it under the terms of the GNU Lesser General Public License as 
.. * published by the Free Software Foundation, either version 3 of the 
.. * License, or (at your option) any later version.
.. * 
.. * This program is distributed in the hope that it will be useful,
.. * but WITHOUT ANY WARRANTY; without even the implied warranty of
.. * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.. * GNU General Lesser Public License for more details.
.. * 
.. * You should have received a copy of the GNU General Lesser Public 
.. * License along with this program.  If not, see
.. * <http://www.gnu.org/licenses/lgpl-3.0.html>.
.. * #L%
.. -

=========
Execution
=========

:Author: Florian Desbois
:Contact: eugene-devel@list.nuiton.org ou eugene-users@list.nuiton.org
:Revision: $Revision$
:Date: $Date$

Pour mettre en oeuvre la génération en utilisant EUGene, il est possible
d'utiliser les principaux utilitaires de build : `Ant`_ et `Maven`_

Ant
---

Ce module est désactivité depuis la version 2.5.
Consultez la documentation dans le `module ant`_.

Maven
-----

Tout d'abord il vous faut la dépendance d'EUGene pour pouvoir utiliser son
API ::

    <dependency>
        <groupId>io.ultreia.java4all.eugene</groupId>
        <artifactId>eugene</artifactId>
        <version>${project.version}</version>
        <scope>provided</scope>
    </dependency>

A noter que le scope provided suffit, étant donné qu'EUGene n'est utilisé
qu'à la compilation/génération.

Vous pouvez désormais étendre les Transformer et Generator désirés pour
pouvoir générer des fichiers en utilisant le plugin maven. Voir `les exemples
et la documentation du plugin`_ .

.. _module ant: ../ant-eugene-task/fr/

.. _les exemples et la documentation du plugin: ../eugene-maven-plugin/fr/example.html


