package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.auto.service.AutoService;
import com.google.common.collect.ImmutableSet;
import org.nuiton.eugene.models.extension.tagvalue.TagValueMetadata;
import org.nuiton.eugene.models.extension.tagvalue.TagValueUtil;
import org.nuiton.eugene.models.extension.tagvalue.matcher.EqualsTagValueNameMatcher;
import org.nuiton.eugene.models.extension.tagvalue.provider.DefaultTagValueMetadatasProvider;
import org.nuiton.eugene.models.extension.tagvalue.provider.TagValueMetadatasProvider;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Set;

import static io.ultreia.java4all.i18n.I18n.t;

/**
 * Defines all tag values managed by Java templates.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5.6
 */
@AutoService(TagValueMetadatasProvider.class)
public class BeanTransformerTagValues extends DefaultTagValueMetadatasProvider {

    @Override
    public String getDescription() {
        return "EUGeNe Bean transformer tag values";
    }

    public enum Store implements TagValueMetadata {
        /**
         * Tag value to generate property change support on generated beans.
         *
         * You can globally use it on the complete model, on packages, or to a specific classifier.
         *
         * @see #isGeneratePropertyChangeSupport(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generatePropertyChangeSupport("Generate PropertyChangedSupport?", boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to generate lazy instantiation of any collection to avoid NPEs.
         *
         * You can globally use it on the complete model or a package, or to a specific classifier.
         *
         * @see #isGenerateNotEmptyCollections(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        generateNotEmptyCollections("Generate none empty collections?", boolean.class, "true", ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to use a super class for generated bean.
         *
         * If the bean needs Property change support (says you use the tag-value {@link Store#generatePropertyChangeSupport},
         * then your class must provide everything for it.
         *
         * More over, if you use some collections in your bean you must also define
         * two method named {@code getChild(Collection list, int index)} and
         * {@code getChild(List list, int index)}
         *
         * See new code to know minimum stuff to add in your class for this purpose.
         * <pre>
         * public abstract class BeanSupport implements Serializable {
         *
         *     private static final long serialVersionUID = 1L;
         *
         *     protected final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
         *
         *     public void addPropertyChangeListener(PropertyChangeListener listener) {
         *         pcs.addPropertyChangeListener(listener);
         *     }
         *
         *     public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
         *         pcs.addPropertyChangeListener(propertyName, listener);
         *     }
         *
         *     public void removePropertyChangeListener(PropertyChangeListener listener) {
         *         pcs.removePropertyChangeListener(listener);
         *     }
         *
         *     public void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
         *         pcs.removePropertyChangeListener(propertyName, listener);
         *     }
         *
         *     protected void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
         *         pcs.firePropertyChange(propertyName, oldValue, newValue);
         *     }
         *
         *     protected void firePropertyChange(String propertyName, Object newValue) {
         *         firePropertyChange(propertyName, null, newValue);
         *     }
         *
         *     protected &lt;T&gt; T getChild(Collection&lt;T&gt; list, int index) {
         *         return CollectionUtil.getOrNull(list, index);
         *     }
         *
         *     protected &lt;T&gt; T getChild(List&lt;T&gt; list, int index) {
         *         return CollectionUtil.getOrNull(list, index);
         *     }
         * }
         * </pre>
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getSuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        superClass("Bean super-class", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * Tag value to use a super super-class for generated defaults class of a simple bean.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getHelperSuperClassTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        helperSuperClass("Helper super-class", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a prefix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getClassNamePrefixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        classNamePrefix("Class name's prefix", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),

        /**
         * To add a prefix on the name of each generated bean class.
         *
         * You can globally use it on the complete model or to a specific classifier.
         *
         * @see #getClassNameSuffixTagValue(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        classNameSuffix("Class name's suffix", String.class, null, ObjectModel.class, ObjectModelPackage.class, ObjectModelClassifier.class),
        /**
         * To add GenerateJavaBean annotation on generated bean.
         *
         * You can globally use it on the complete model.
         *
         * @see #isAddJavaBeanAnnotation(ObjectModelClassifier, ObjectModelPackage, ObjectModel)
         * @since 3.0
         */
        addJavaBeanAnnotation("Java 8 support", boolean.class, "true", ObjectModel.class,ObjectModelClassifier.class, ObjectModelPackage.class);

        private final Set<Class<?>> targets;
        private final Class<?> type;
        private final String i18nDescriptionKey;
        private final String defaultValue;

        Store(String i18nDescriptionKey, Class<?> type, String defaultValue, Class<?>... targets) {
            this.targets = ImmutableSet.copyOf(targets);
            this.type = type;
            this.i18nDescriptionKey = i18nDescriptionKey;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getName() {
            return name();
        }

        @Override
        public Set<Class<?>> getTargets() {
            return targets;
        }

        @Override
        public Class<?> getType() {
            return type;
        }

        @Override
        public Class<EqualsTagValueNameMatcher> getMatcherClass() {
            return EqualsTagValueNameMatcher.class;
        }

        @Override
        public String getDescription() {
            return t(i18nDescriptionKey);
        }

        @Override
        public String getDefaultValue() {
            return defaultValue;
        }

        @Override
        public boolean isDeprecated() {
            return false;
        }

    }

    public BeanTransformerTagValues() {
        super((TagValueMetadata[]) Store.values());
    }

    /**
     * Obtain the value of the {@link Store#superClass} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#superClass
     * @since 3.0
     */
    public String getSuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.superClass, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#helperSuperClass} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#helperSuperClass
     * @since 3.0
     */
    public String getHelperSuperClassTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.helperSuperClass, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#classNamePrefix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#classNamePrefix
     * @since 3.0
     */
    public String getClassNamePrefixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.classNamePrefix, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#classNameSuffix} tag value on the given model or classifier.
     *
     * It will first look on the model, and then in the given classifier.
     *
     * @param classifier classifier to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#classNameSuffix
     * @since 3.0
     */
    public String getClassNameSuffixTagValue(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findTagValue(Store.classNameSuffix, classifier, aPackage, model);
    }
    /**
     * Obtain the value of the {@link Store#generatePropertyChangeSupport} tag value on the given model, package or classifier.
     *
     * It will first look on the model, then and package and then in the given classifier.
     *
     * If no value found, then will use the default value of the tag value.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generatePropertyChangeSupport
     * @since 3.0
     */
    public boolean isGeneratePropertyChangeSupport(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generatePropertyChangeSupport, classifier, aPackage, model);
    }

    /**
     * Obtain the value of the {@link Store#generateNotEmptyCollections} tag value on the given model, package or classifier.
     *
     * It will first look on the model, then and package and then in the given classifier.
     *
     * If no value found, then will use the default value of the tag value.
     *
     * @param classifier classifier to seek
     * @param aPackage   package to seek
     * @param model      model to seek
     * @return the none empty value of the found tag value or {@code null} if not found nor empty.
     * @see Store#generateNotEmptyCollections
     * @since 3.0
     */
    public boolean isGenerateNotEmptyCollections(ObjectModelClassifier classifier, ObjectModelPackage aPackage, ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.generateNotEmptyCollections, classifier, aPackage, model);
    }

    /**
     * Check if the given model, package or classifier has the {@link Store#addJavaBeanAnnotation} stereotype.
     *
     * @param classifier classifier to test
     * @param aPackage package to test
     * @param model model to test
     * @return {@code true} if tag value was found, {@code false otherwise}
     * @see Store#addJavaBeanAnnotation
     */
    public boolean isAddJavaBeanAnnotation(ObjectModelClassifier classifier, ObjectModelPackage aPackage,ObjectModel model) {
        return TagValueUtil.findBooleanTagValue(Store.addJavaBeanAnnotation, classifier, aPackage);
    }

}
