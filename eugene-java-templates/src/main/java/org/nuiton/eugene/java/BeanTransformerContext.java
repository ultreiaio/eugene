package org.nuiton.eugene.java;

/*-
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.LogProxy;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelPackage;

/**
 * Created by tchemit on 31/08/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class BeanTransformerContext {

    // all beans classes with their fully qualified name
    public final ImmutableMap<ObjectModelClass, String> classesNameTranslation;

    // all bean classes indexed by their fully qualified name
    public final ImmutableMap<String, ObjectModelClass> classesByFqn;

    // all bean classes fqn
    public final ImmutableList<String> classesFqn;

    // selected bean classes
    public final ImmutableSet<ObjectModelClass> selectedClasses;

    // excluded bean classes
    public final ImmutableSet<ObjectModelClass> excludedClasses;

    // select bean classes fqn
    public final ImmutableList<String> selectedClassesFqn;

    // selected bean classes indexed by their fully qualified name
    public final ImmutableMap<String, ObjectModelClass> selectedClassesByFqn;

    public final boolean useJava8;
    public final boolean useRelativeName;
    public final String[] relativeNameExcludes;

    private final LogProxy log;

    public BeanTransformerContext(ObjectModel model, EugeneCoreTagValues coreTagValues, EugeneJavaTagValues javaTemplatesTagValues, BeanTransformerTagValues beanTagValues, boolean includeAbstract, boolean includeSkip, Predicate<ObjectModelClass> extraPredicate, LogProxy log) {
        this.log = log;

        useJava8 = javaTemplatesTagValues.isUseJava8(model);
        useRelativeName = coreTagValues.isUseRelativeName(model);
        Set<String> relativeNameExcludes = coreTagValues.getRelativeNameExcludes(model);
        this.relativeNameExcludes = relativeNameExcludes==null?new String[0]: relativeNameExcludes.toArray(new String[0]);

        ImmutableMap.Builder<ObjectModelClass, String> classesNameTranslationBuilder = new ImmutableMap.Builder<>();
        ImmutableSet.Builder<ObjectModelClass> classesBuilder = new ImmutableSet.Builder<>();
        ImmutableSet.Builder<ObjectModelClass> excludedClassesBuilder = new ImmutableSet.Builder<>();

        for (ObjectModelClass aClass : model.getClasses()) {

            ObjectModelPackage aPackage = model.getPackage(aClass.getPackageName());
            if (javaTemplatesTagValues.isBean(aClass, aPackage)) {

                String classNamePrefix = beanTagValues.getClassNamePrefixTagValue(aClass, aPackage, model);
                String classNameSuffix = beanTagValues.getClassNameSuffixTagValue(aClass, aPackage, model);

                String generateName = JavaGeneratorUtil.generateName(classNamePrefix, aClass.getName(), classNameSuffix);
                classesNameTranslationBuilder.put(aClass, generateName);

                if (!includeSkip && EugeneCoreTagValues.isSkip(aClass, aPackage)) {
                    this.log.debug("Exclude by skip: " + aClass.getQualifiedName());
                    excludedClassesBuilder.add(aClass);
                    continue;
                }
                if (!includeAbstract && aClass.isAbstract()) {
                    this.log.debug("Exclude by abstract: " + aClass.getQualifiedName());
                    excludedClassesBuilder.add(aClass);
                    continue;
                }

                if (extraPredicate != null && !extraPredicate.test(aClass)) {

                    this.log.debug("Exclude by predicate: " + aClass.getQualifiedName());
                    excludedClassesBuilder.add(aClass);
                    continue;
                }
                this.log.debug("Include: " + aClass.getQualifiedName());
                classesBuilder.add(aClass);

            }

        }

        classesNameTranslation = classesNameTranslationBuilder.build();
        selectedClasses = classesBuilder.build();
        excludedClasses = excludedClassesBuilder.build();

        classesByFqn = Maps.uniqueIndex(classesNameTranslation.keySet(), ObjectModelClassifier::getQualifiedName);
        List<String> classesFqn = new ArrayList<>(classesByFqn.keySet());
        Collections.sort(classesFqn);
        this.classesFqn = ImmutableList.copyOf(classesFqn);

        selectedClassesByFqn = Maps.uniqueIndex(selectedClasses, ObjectModelClassifier::getQualifiedName);
        List<String> beanClassesFqn = new ArrayList<>(selectedClassesByFqn.keySet());
        Collections.sort(beanClassesFqn);
        this.selectedClassesFqn = ImmutableList.copyOf(beanClassesFqn);
    }

    public void report() {
        log.info(String.format("Found %d classes with « Bean » stereotype.", classesNameTranslation.size()));
        if (!excludedClasses.isEmpty()) {
            log.info(String.format("Skip excluded %d classes.", excludedClasses.size()));
        }
        log.info(String.format("Will apply on %d classes.", selectedClasses.size()));
    }
}
