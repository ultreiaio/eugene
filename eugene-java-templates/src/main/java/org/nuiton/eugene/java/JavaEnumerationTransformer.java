/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */
package org.nuiton.eugene.java;


/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelEnumeration;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.util.Collection;

/**
 * JavaEnumerationTransformer generates a enumeration for enuration with
 * stereotype enumeration.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 2.5
 */
public class JavaEnumerationTransformer extends ObjectModelTransformerToJava {

    @Override
    public void transformFromModel(ObjectModel model) {
        if (getLog()==null) {
            setLog(new SystemOutLogProxy());
        }
        super.transformFromModel(model);
    }

    @Override
    public void transformFromEnumeration(ObjectModelEnumeration input) {
        if (!canGenerate(input)) {

            getLog().debug("Skip generation for " + input.getQualifiedName());
            return;
        }

        ObjectModelEnumeration output =
                createEnumeration(input.getName(), input.getPackageName());

        getLog().debug("will generate " + output.getQualifiedName());

        Collection<String> literals = input.getLiterals();

        for (String literal : literals) {
            addLiteral(output, literal);
        }
    }

    protected boolean canGenerate(ObjectModelEnumeration input) {
        ObjectModelPackage aPackage = getPackage(input);
        boolean b = !EugeneCoreTagValues.isSkip(input, aPackage);
        if (b) {

            // check if not found in class-path
            b = !getResourcesHelper().isJavaFileInClassPath(input.getQualifiedName());
        }
        return b;
    }
}
