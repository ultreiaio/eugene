package org.nuiton.eugene.java;

/*
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/*{generator option: parentheses = false}*/
/*{generator option: writeString = +}*/

import io.ultreia.java4all.bean.spi.GenerateJavaBeanDefinition;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.nuiton.eugene.EugeneCoreTagValues;
import org.nuiton.eugene.GeneratorUtil;
import org.nuiton.eugene.models.object.ObjectModel;
import org.nuiton.eugene.models.object.ObjectModelAttribute;
import org.nuiton.eugene.models.object.ObjectModelClass;
import org.nuiton.eugene.models.object.ObjectModelClassifier;
import org.nuiton.eugene.models.object.ObjectModelInterface;
import org.nuiton.eugene.models.object.ObjectModelJavaModifier;
import org.nuiton.eugene.models.object.ObjectModelOperation;
import org.nuiton.eugene.models.object.ObjectModelPackage;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Generates a bean and a helper class around it.
 * <p>
 * Generates also a model initializer contract which permits you to interact with all selectedClasses of your model.
 * <p>
 * For example:
 * <pre>
 *     GeneratedBoat
 *     Boat (extends GeneratedBoat)
 *     GeneratedBoatHelper
 *     BoatHelper (extends AbstractBoats)
 * </pre>
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 3.0
 */
public class BeanTransformer extends ObjectModelTransformerToJava {

    protected final EugeneJavaTagValues javaTemplatesTagValues;
    protected final BeanTransformerTagValues beanTagValues;
    protected final EugeneCoreTagValues coreTagValues;
    protected BeanTransformerContext context;

    public BeanTransformer() {
        javaTemplatesTagValues = new EugeneJavaTagValues();
        beanTagValues = new BeanTransformerTagValues();
        coreTagValues = new EugeneCoreTagValues();
    }

    public static boolean skipForInitializer(ObjectModelPackage aPackage, ObjectModelClass beanClass) {
        return beanClass.isAbstract() || EugeneCoreTagValues.isSkip(beanClass, aPackage);
    }

    @Override
    public void transformFromModel(ObjectModel model) {
        if (getLog() == null) {
            setLog(new SystemOutLogProxy());
        }
        super.transformFromModel(model);


        context = new BeanTransformerContext(model, coreTagValues, javaTemplatesTagValues, beanTagValues, true, true, aClass -> true, getLog());

        context.report();

        generateInitializers();
    }

    protected void generateInitializers() {

        String defaultPackageName = getDefaultPackageName();

        boolean useRelativeName = context.useRelativeName;
        String[] relativeNameExcludes = context.relativeNameExcludes;

        String modelBeanInitializeClassName = model.getName() + "ModelInitializer";
        boolean generateModelInitializer = !getResourcesHelper().isJavaFileInClassPath(defaultPackageName + "." + modelBeanInitializeClassName);
        if (generateModelInitializer) {

            ObjectModelInterface anInterface = createInterface(modelBeanInitializeClassName, defaultPackageName);

            addOperation(anInterface, "start", "void");
            addOperation(anInterface, "end", "void");

            for (String fqn : context.selectedClassesFqn) {
                ObjectModelClass beanClass = context.selectedClassesByFqn.get(fqn);
                ObjectModelPackage aPackage = getModel().getPackage(beanClass);
                if (skipForInitializer(aPackage, beanClass)) {
                    continue;
                }
                String beanName = context.classesNameTranslation.get(beanClass);
                String methodName = getMethodName(useRelativeName, relativeNameExcludes, "init", aPackage.getName(), beanName);
                addOperation(anInterface, methodName, "void");

            }
        }

        String modelInitializerRunnerClassName = model.getName() + "ModelInitializerRunner";
        boolean generateInitializerRunnerClassName = !getResourcesHelper().isJavaFileInClassPath(defaultPackageName + "." + modelInitializerRunnerClassName);
        if (generateInitializerRunnerClassName) {

            ObjectModelClass aClass = createClass(modelInitializerRunnerClassName, defaultPackageName);

            StringBuilder bodyBuilder = new StringBuilder();
            bodyBuilder.append(""
/*{
        initializer.start();}*/
            );
            for (String fqn : context.selectedClassesFqn) {
                ObjectModelClass beanClass = context.selectedClassesByFqn.get(fqn);
                ObjectModelPackage aPackage = getModel().getPackage(beanClass);
                if (skipForInitializer(aPackage, beanClass)) {
                    continue;
                }
                String beanName = context.classesNameTranslation.get(beanClass);
                String methodName = getMethodName(useRelativeName, relativeNameExcludes, "init", aPackage.getName(), beanName);
                bodyBuilder.append(""
/*{
        initializer.<%=methodName%>();}*/
                );

            }

            bodyBuilder.append(""
/*{
        initializer.end();
}*/
            );
            ObjectModelOperation operation = addOperation(aClass, "init", "void", ObjectModelJavaModifier.STATIC);
            addParameter(operation, modelBeanInitializeClassName, "initializer");
            setOperationBody(operation, bodyBuilder.toString());
        }
    }
    @Override
    public void transformFromClass(ObjectModelClass input) {

        ObjectModelPackage aPackage = getPackage(input);

        if (context.selectedClasses.contains(input)) {

            if (EugeneCoreTagValues.isSkip(input, aPackage)) {
                return;
            }

            String prefix = getConstantPrefix(input);
            setConstantPrefix(prefix);

            String className = context.classesNameTranslation.get(input);
            String generatedClassName = "Generated" + className;

            boolean generateClass = notFoundInClassPath(input, className);
            if (generateClass) {
                generateClass(input, className, generatedClassName);
            }

            boolean generateGeneratedClass = canGenerateAbstractClass(input, generatedClassName);
            if (generateGeneratedClass) {
                generateGeneratedClass(aPackage, input, generatedClassName, className);
            }
        }

    }

    protected ObjectModelClass generateClass(ObjectModelClass input,
                                             String className,
                                             String abstractClassName) {

        ObjectModelClass output;

        if (input.isAbstract()) {
            output = createAbstractClass(className, input.getPackageName());
        } else {
            output = createClass(className, input.getPackageName());
        }
        if (beanTagValues.isAddJavaBeanAnnotation(input, model.getPackage(input), model)) {
            addAnnotation(output, output, GenerateJavaBeanDefinition.class);
        }

        setSuperClass(output, abstractClassName);

        getLog().debug("will generate " + output.getQualifiedName());

        addSerializable(input, output, true);

        return output;
    }

    protected ObjectModelClass generateGeneratedClass(ObjectModelPackage aPackage,
                                                      ObjectModelClass input,
                                                      String className, String mainClassName) {

        String superClass = null;

        // test if a super class has bean stereotype
        boolean superClassIsBean = false;
        Collection<ObjectModelClass> superclasses = input.getSuperclasses();
        if (CollectionUtils.isNotEmpty(superclasses)) {
            for (ObjectModelClass superclass : superclasses) {
                superClassIsBean = context.selectedClasses.contains(superclass);
                if (superClassIsBean) {
                    superClass = superclass.getPackageName() + "." + context.classesNameTranslation.get(superclass);
                    break;
                }
                superClass = superclass.getQualifiedName();
            }
        }

        if (!superClassIsBean) {

            // try to find a super class by tag-value
            superClass = beanTagValues.getSuperClassTagValue(input, aPackage, model);
            if (superClass != null) {

                // will act as if super class is a bean
                superClassIsBean = true;
            }
        }

        ObjectModelClass output;

        output = createAbstractClass(className, input.getPackageName());
        if (superClass != null) {
            setSuperClass(output, superClass);
        }
        getLog().debug("will generate " + output.getQualifiedName());

        boolean serializableFound;

        serializableFound = addInterfaces(input, output, null);

        generateI18nBlockAndConstants(aPackage, input, output);

        addSerializable(input, output, serializableFound || superClassIsBean);

        // Get available properties
        List<ObjectModelAttribute> properties = getProperties(input);

        boolean usePCS = beanTagValues.isGeneratePropertyChangeSupport(input, aPackage, model);
        boolean generateBooleanGetMethods = eugeneTagValues.isGenerateBooleanGetMethods(input, aPackage, model);
        boolean generateNotEmptyCollections = beanTagValues.isGenerateNotEmptyCollections(input, aPackage, model);

        // Add properties field + javabean methods
        for (ObjectModelAttribute attr : properties) {

            createProperty(output,
                    attr,
                    usePCS,
                    generateBooleanGetMethods,
                    generateNotEmptyCollections);
        }

        if (!superClassIsBean) {
            addDefaultMethodForNoneBeanSuperClass(output, usePCS, properties);
        }
        return output;
    }

    protected String getAttributeType(ObjectModelAttribute attr) {
        String attrType = attr.getType();
        if (attr.hasAssociationClass()) {
            attrType = attr.getAssociationClass().getName();
        }
        return getAttributeType(attrType);
    }

    protected String getAttributeType(String attrType) {
        if (!JavaGeneratorUtil.isPrimitiveType(attrType)) {
            boolean hasClass = model.hasClass(attrType);
            if (hasClass) {
                ObjectModelClass attributeClass = model.getClass(attrType);
                String attributeType = context.classesNameTranslation.get(attributeClass);
                if (attributeType != null) {
                    attrType = attributeClass.getPackageName() + "." + attributeType;
                }
            }
        }
        return attrType;
    }

    protected boolean notFoundInClassPath(ObjectModelClass input, String className) {
        String fqn = input.getPackageName() + "." + className;
        boolean inClassPath = getResourcesHelper().isJavaFileInClassPath(fqn);
        return !inClassPath;
    }

    protected void createProperty(ObjectModelClass output,
                                  ObjectModelAttribute attr,
                                  boolean usePCS,
                                  boolean generateBooleanGetMethods,
                                  boolean generateNotEmptyCollections) {

        String attrName = getAttributeName(attr);
        String attrType = getAttributeTypeWithGeneric(attr);

        boolean multiple = JavaGeneratorUtil.isNMultiplicity(attr);

        String constantName = getConstantName(attrName);
        String simpleType = JavaGeneratorUtil.getSimpleName(attrType);

        if (multiple) {

            createGetChildMethod(output,
                    attrName,
                    attrType,
                    simpleType
            );

            createIsEmptyMethod(output, attrName);

            createSizeMethod(output, attrName);

            createAddChildMethod(output,
                    attrName,
                    attrType,
                    constantName,
                    usePCS
            );

            createAddAllChildrenMethod(output,
                    attrName,
                    attrType,
                    constantName,
                    usePCS
            );

            createRemoveChildMethod(output,
                    attrName,
                    attrType,
                    constantName,
                    usePCS
            );

            createRemoveAllChildrenMethod(output,
                    attrName,
                    attrType,
                    constantName,
                    usePCS
            );

            createContainsChildMethod(output,
                    attrName,
                    attrType,
                    constantName,
                    usePCS
            );

            createContainsAllChildrenMethod(output,
                    attrName,
                    attrType,
                    constantName
            );

            // Change type for Multiple attribute
            attrType = JavaGeneratorUtil.getAttributeInterfaceType(attr, getAttributeTypeWithGeneric(attr), true);
            simpleType = JavaGeneratorUtil.getSimpleName(attrType);
        }

        boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attr);

        if (multiple) {

            String collectionImplementationType = JavaGeneratorUtil.getAttributeImplementationType(attr, getAttributeTypeWithGeneric(attr), true);

            // creates a getXXX (multiple) method
            createGetMethod(output,
                    attrName,
                    attrType,
                    JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX,
                    generateNotEmptyCollections,
                    collectionImplementationType
            );

        } else {

            if (booleanProperty) {

                // creates a isXXX method
                createGetMethod(output,
                        attrName,
                        attrType,
                        JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX
                );
            }

            if (!booleanProperty || generateBooleanGetMethods) {

                // creates a getXXX method
                createGetMethod(output,
                        attrName,
                        attrType,
                        JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX
                );

            }


        }

        createSetMethod(output,
                attrName,
                attrType,
                simpleType,
                constantName,
                usePCS
        );

        // Add attribute to the class
        addAttribute(output,
                attrName,
                attrType,
                "",
                ObjectModelJavaModifier.PROTECTED
        );

    }

    protected List<ObjectModelAttribute> getProperties(ObjectModelClass input) {
        Collection<ObjectModelAttribute> attributes = input.getAttributes();

        List<ObjectModelAttribute> attrs = new ArrayList<>();
        for (ObjectModelAttribute attr : attributes) {
            if (attr.isNavigable()) {
                // only keep navigable attributes
                attrs.add(attr);
            }
        }
        return attrs;
    }

    protected void createGetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String methodPrefix,
                                   boolean generateLayzCode,
                                   String collectionImplementationType) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        if (generateLayzCode) {
            addImport(output, collectionImplementationType);
            setOperationBody(operation, ""
/*{
    if (<%=attrName%> == null) {
        <%=attrName%> = new <%=collectionImplementationType%>();
    }
    return <%=attrName%>;
}*/
            );
        } else {
            setOperationBody(operation, ""
/*{
    return <%=attrName%>;
}*/
            );
        }

    }

    protected void createGetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String methodPrefix) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName(methodPrefix, attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%>;
    }*/
        );
    }

    protected void createGetChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String simpleType) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("get", attrName),
                attrType,
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "int", "index");
        setOperationBody(operation, ""
    /*{
        <%=attrType%> o = getChild(<%=attrName%>, index);
        return o;
    }*/
        );
    }

    protected void createIsEmptyMethod(ObjectModelClass output,
                                       String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("is", attrName) + "Empty",
                boolean.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null || <%=attrName%>.isEmpty();
    }*/
        );
    }

    protected void createSizeMethod(ObjectModelClass output,
                                    String attrName) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("size", attrName),
                int.class,
                ObjectModelJavaModifier.PUBLIC
        );
        setOperationBody(operation, ""
    /*{
        return <%=attrName%> == null ? 0 : <%=attrName%>.size();
    }*/
        );
    }

    protected void createAddChildMethod(ObjectModelClass output,
                                        String attrName,
                                        String attrType,
                                        String constantName,
                                        boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("add", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);

        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder(""
    /*{
        <%=methodName%>().add(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createAddAllChildrenMethod(ObjectModelClass output,
                                              String attrName,
                                              String attrType,
                                              String constantName,
                                              boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("addAll", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);

        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder(""
    /*{
        <%=methodName%>().addAll(<%=attrName%>);
    }*/
        );
        if (usePCS) {
            buffer.append(""
    /*{    firePropertyChange(<%=constantName%>, null, <%=attrName%>);
    }*/
            );
        }
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveChildMethod(ObjectModelClass output,
                                           String attrName,
                                           String attrType,
                                           String constantName,
                                           boolean usePCS) {
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("remove", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        String methodName = getJavaBeanMethodName("get", attrName);
        StringBuilder buffer = new StringBuilder();
        buffer.append(""
    /*{
        boolean removed = <%=methodName%>().remove(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createRemoveAllChildrenMethod(ObjectModelClass output,
                                                 String attrName,
                                                 String attrType,
                                                 String constantName,
                                                 boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("removeAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  removed = <%=methodName%>().removeAll(<%=attrName%>);}*/
        );

        if (usePCS) {
            buffer.append(""
    /*{
        if (removed) {
            firePropertyChange(<%=constantName%>, <%=attrName%>, null);
        }}*/
            );
        }
        buffer.append(""
    /*{
        return removed;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsChildMethod(ObjectModelClass output,
                                             String attrName,
                                             String attrType,
                                             String constantName,
                                             boolean usePCS) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("contains", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean contains = <%=methodName%>().contains(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createContainsAllChildrenMethod(ObjectModelClass output,
                                                   String attrName,
                                                   String attrType,
                                                   String constantName) {

        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("containsAll", attrName),
                "boolean",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, "java.util.Collection<" + attrType + ">", attrName);
        StringBuilder buffer = new StringBuilder();
        String methodName = getJavaBeanMethodName("get", attrName);
        buffer.append(""
    /*{
        boolean  contains = <%=methodName%>().containsAll(<%=attrName%>);
        return contains;
    }*/
        );
        setOperationBody(operation, buffer.toString());
    }

    protected void createSetMethod(ObjectModelClass output,
                                   String attrName,
                                   String attrType,
                                   String simpleType,
                                   String constantName,
                                   boolean usePCS) {
        boolean booleanProperty = GeneratorUtil.isBooleanPrimitive(simpleType);
        ObjectModelOperation operation = addOperation(
                output,
                getJavaBeanMethodName("set", attrName),
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, attrType, attrName);

        if (usePCS) {
            String methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
            if (booleanProperty) {
                methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
            }
            String methodName = getJavaBeanMethodName(methodPrefix, attrName);
            setOperationBody(operation, ""
    /*{
        <%=attrType%> oldValue = <%=methodName%>();
        this.<%=attrName%> = <%=attrName%>;
        firePropertyChange(<%=constantName%>, oldValue, <%=attrName%>);
    }*/
            );
        } else {
            setOperationBody(operation, ""
    /*{
        this.<%=attrName%> = <%=attrName%>;
    }*/
            );
        }
    }

    protected void addSerializable(ObjectModelClass input,
                                   ObjectModelClass output,
                                   boolean interfaceFound) {
        if (!interfaceFound) {
            addInterface(output, Serializable.class);
        }

        // Generate the serialVersionUID
        long serialVersionUID = JavaGeneratorUtil.generateSerialVersionUID(input);

        addConstant(output,
                JavaGeneratorUtil.SERIAL_VERSION_UID,
                "long",
                serialVersionUID + "L",
                ObjectModelJavaModifier.PRIVATE
        );
    }

    /**
     * Add all interfaces defines in input class and returns if
     * {@link Serializable} interface was found.
     *
     * @param input  the input model class to process
     * @param output the output generated class
     * @return {@code true} if {@link Serializable} was found from input,
     * {@code false} otherwise
     */
    protected boolean addInterfaces(ObjectModelClass input,
                                    ObjectModelClassifier output,
                                    String extraInterfaceName) {
        boolean foundSerializable = false;
        Set<String> added = new HashSet<>();
        for (ObjectModelInterface parentInterface : input.getInterfaces()) {
            String fqn = parentInterface.getQualifiedName();
            added.add(fqn);
            addInterface(output, fqn);
            if (Serializable.class.getName().equals(fqn)) {
                foundSerializable = true;
            }
        }
        if (extraInterfaceName != null && !added.contains(extraInterfaceName)) {
            addInterface(output, extraInterfaceName);
        }
        return foundSerializable;
    }

    protected void createPropertyChangeSupport(ObjectModelClass output) {

        addAttribute(output,
                "pcs",
                PropertyChangeSupport.class,
                "new PropertyChangeSupport(this)",
                ObjectModelJavaModifier.PROTECTED,
                ObjectModelJavaModifier.FINAL,
                ObjectModelJavaModifier.TRANSIENT
        );

        // Add PropertyListener

        ObjectModelOperation operation;

        operation = addOperation(output,
                "addPropertyChangeListener",
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                "addPropertyChangeListener",
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.addPropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                "removePropertyChangeListener",
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(listener);
    }*/
        );

        operation = addOperation(output,
                "removePropertyChangeListener",
                "void",
                ObjectModelJavaModifier.PUBLIC
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, PropertyChangeListener.class, "listener");
        setOperationBody(operation, ""
    /*{
        pcs.removePropertyChangeListener(propertyName, listener);
    }*/
        );

        operation = addOperation(output,
                "firePropertyChange",
                "void",
                ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "oldValue");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }*/
        );

        operation = addOperation(output,
                "firePropertyChange",
                "void",
                ObjectModelJavaModifier.PROTECTED
        );
        addParameter(operation, String.class, "propertyName");
        addParameter(operation, Object.class, "newValue");
        setOperationBody(operation, ""
    /*{
        firePropertyChange(propertyName, null, newValue);
    }*/
        );
    }

    protected void createGetChildMethod(ObjectModelClass output) {
        ObjectModelOperation getChild = addOperation(
                output,
                "getChild", "<T> T",
                ObjectModelJavaModifier.PROTECTED
        );
        addImport(output, List.class);

        addParameter(getChild, "java.util.Collection<T>", "childs");
        addParameter(getChild, "int", "index");
        setOperationBody(getChild, ""
/*{
        T result = null;
        if (childs != null) {
            if (childs instanceof List) {
                if (index < childs.size()) {
                    result = ((List<T>) childs).get(index);
                }
            } else {
                int i = 0;
                for (T o : childs) {
                    if (index == i) {
                        result = o;
                        break;
                    }
                    i++;
                }
            }
        }
        return result;
}*/
        );
    }

    protected void generateI18nBlockAndConstants(ObjectModelPackage aPackage,
                                                 ObjectModelClass input,
                                                 ObjectModelClassifier output) {

        String i18nPrefix = eugeneTagValues.getI18nPrefixTagValue(input,
                aPackage,
                model);
        if (!StringUtils.isEmpty(i18nPrefix)) {
            generateI18nBlock(input, output, i18nPrefix);
        }

        String prefix = getConstantPrefix(input);

        setConstantPrefix(prefix);

        Set<String> constantNames = addConstantsFromDependency(input, output);

        // Add properties constant
        for (ObjectModelAttribute attr : getProperties(input)) {

            createPropertyConstant(output, attr, prefix, constantNames);
        }
    }

    protected void addDefaultMethodForNoneBeanSuperClass(ObjectModelClass output,
                                                         boolean usePCS,
                                                         List<ObjectModelAttribute> properties) {


        if (usePCS) {

            // Add property change support
            createPropertyChangeSupport(output);
        }

        boolean hasAMultipleProperty = containsMultiple(properties);

        // Add helper operations
        if (hasAMultipleProperty) {

            // add getChild methods
            createGetChildMethod(output);
        }
    }

    protected String wrapPrimitiveType(String attrType) {
        if (JavaGeneratorUtil.isPrimitiveType(attrType)) {
            attrType = JavaGeneratorUtil.getPrimitiveWrapType(attrType);
        }
        return attrType;
    }

    protected String getGetterName(ObjectModelAttribute attribute, String attrName) {
        boolean booleanProperty = JavaGeneratorUtil.isBooleanPrimitive(attribute);
        String methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_DEFAULT_PREFIX;
        if (booleanProperty) {
            methodPrefix = JavaGeneratorUtil.OPERATION_GETTER_BOOLEAN_PREFIX;
        }
        return getJavaBeanMethodName(methodPrefix, attrName);
    }


    protected boolean canGenerateAbstractClass(ObjectModelClass aClass, String abstractClassName) {

        boolean inClassPath = !notFoundInClassPath(aClass, abstractClassName);

        if (inClassPath) {
            throw new IllegalStateException(String.format("Can't generate %s, already found in class-path, this is a generated class, you should not ovveride it.\n\nPlease remove it from class path and use the %s class instead.", aClass.getPackageName() + "." + abstractClassName, aClass));
        }

        return true;

    }

    protected void createPropertyConstant(ObjectModelClassifier output,
                                          ObjectModelAttribute attr,
                                          String prefix,
                                          Set<String> constantNames) {

        String attrName = getAttributeName(attr);

        String constantName = prefix + builder.getConstantName(attrName);

        if (!constantNames.contains(constantName)) {

            addConstant(output,
                    constantName,
                    String.class,
                    "\"" + attrName + "\"",
                    ObjectModelJavaModifier.PUBLIC
            );
        }
    }

    protected String getAttributeName(ObjectModelAttribute attr) {
        String attrName = attr.getName();
        if (attr.hasAssociationClass()) {
            String assocAttrName = JavaGeneratorUtil.getAssocAttrName(attr);
            attrName = JavaGeneratorUtil.toLowerCaseFirstLetter(assocAttrName);
        }
        return attrName;
    }

    protected String getAttributeTypeWithGeneric(ObjectModelAttribute attr) {
        String attrType = getAttributeType(attr);
        String generic = eugeneTagValues.getAttributeGenericTagValue(attr);
        if (generic != null) {
            attrType += "<" + getAttributeType(generic) + ">";
        }
        return attrType;
    }

    protected boolean containsMultiple(List<ObjectModelAttribute> attributes) {

        boolean result = false;

        for (ObjectModelAttribute attr : attributes) {

            if (JavaGeneratorUtil.isNMultiplicity(attr)) {
                result = true;

                break;
            }

        }
        return result;
    }


}
