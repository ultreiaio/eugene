package org.nuiton.eugene.java;

/*-
 * #%L
 * EUGene :: Java templates
 * %%
 * Copyright (C) 2012 - 2025 Code Lutin, Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.nuiton.eugene.LogProxy;

/**
 * Created by tchemit on 02/09/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
class SystemOutLogProxy implements LogProxy {
    @Override
    public void info(String message) {
        System.out.println("[INFO] " + message);
    }

    @Override
    public void debug(String message) {
        System.out.println("[DEBUG] " + message);
    }

    @Override
    public void warn(String message) {
        System.out.println("[WARNING] " + message);
    }

    @Override
    public void error(String message) {
        System.out.println("[ERROR] " + message);
    }
}
